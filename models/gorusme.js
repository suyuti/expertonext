const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

const musteri = require('./musteri')
const sf = require('./satisFirsati2')
const kisi = require('./kisi')

const Schema = mongoose.Schema;
const gorusmeKanallari = ['telefon', 'mail', 'toplanti']

var SchemaModel = new Schema(
  {
    musteri         : { type: mongoose.Schema.Types.ObjectId, ref: 'Musteri' },
    satisFirsati    : { type: mongoose.Schema.Types.ObjectId, ref: 'SatisFirsati2'},
    kisi            : { type: mongoose.Schema.Types.ObjectId, ref: 'Kisi'},
    personel        : { type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    gorusmeZamani   : { type: Date },
    kanal           : { type: String, gorusmeKanallari},
    konu            : { type: String },
    aciklama        : { type: String },
    createdAt       : { type: Date, default: Date.now },
    createdBy       : { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    updatedAt       : { type: Date },
    updatedBy       : { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
  },
  {
    versionKey: false,
    timestamps: true,
  }
);

SchemaModel.pre("save", function (next) {
    return next();
});

module.exports = mongoose.models.Gorusme || mongoose.model("Gorusme", SchemaModel);
