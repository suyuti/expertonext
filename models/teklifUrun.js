const mongoose = require("mongoose");
import Urun from './urun'

const Schema = mongoose.Schema;

var SchemaModel = new Schema(
  {
        urun                    : { type: mongoose.Schema.Types.ObjectId, ref: 'Urun'},
        urunAdi                 : { type: String},                                      // populate etmemek icin
        onOdemeTutari           : { type: Number, default: 0 },
        basariTutari            : { type: Number, default: 0 },
        sabitOdemeTutari        : { type: Number, default: 0 },
        sabitOdemeAy            : { type: Number, default: 0 },
        raporBasiOdemeTutari    : { type: Number, default: 0 },
        araToplam               : { type: Number, default: 0 },
        indirim                 : { type: Number, default: 0 },
        kdv                     : { type: Number, default: 0 },
        toplam                  : { type: Number, default: 0 },
  },
  {
    versionKey: false,
    timestamps: true,
  }
);

SchemaModel.pre("save", function (next) {
    return next();
});

module.exports = mongoose.models.Teklif || mongoose.model("Teklif", SchemaModel);
