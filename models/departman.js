const mongoose = require("mongoose");
import MpathPlugin from 'mongoose-mpath';

import './musteri'
import './toplanti'

const Schema = mongoose.Schema;

var SchemaModel = new Schema(
  {
    adi             : { type: String },
    yoneticisi      : { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    createdAt       : { type: Date, default: Date.now },
    createdBy       : { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    updatedAt       : { type: Date },
    updatedBy       : { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
  },
  {
    versionKey: false,
    timestamps: true,
  }
);

export default mongoose.models.Departman || mongoose.model("Departman", SchemaModel.plugin(MpathPlugin));
