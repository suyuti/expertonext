const mongoose = require("mongoose");
import './musteri'
import './kisi'
import './user'
import './gorev'

const Schema = mongoose.Schema;

const kararOncelikleri = ['KRITIK', 'YUKSEK', 'ORTA', 'DUSUK']

var SchemaModel = new Schema({
    randevu             : {}, // Varsa Randevu ile iliskendirilir
    musteri             : { type: mongoose.Schema.Types.ObjectId, ref: 'Musteri' },
    firmaKatilimcilar   : [{ type: mongoose.Schema.Types.ObjectId, ref: 'Kisi' }],
    expertoKatilimcilar : [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
    konum               : { type: String},
    zaman               : { type: Date},
    kararlar: [
      {
        proje         : { type: String}, // TODO
        konu          : { type: String},
        ilgiliPersonel: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
        terminTarihi  : { type: Date},
        oncelik       : { type: String, enum: kararOncelikleri},
        gorev         : { type: mongoose.Schema.Types.ObjectId, ref: 'Gorev' } 
      }
    ],
    createdAt: { type: Date, default: Date.now },
    createdBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    updatedAt: { type: Date },
    updatedBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
  },
  {
    versionKey: false,
    timestamps: true,
  }
);

module.exports = mongoose.models.Toplanti || mongoose.model("Toplanti", SchemaModel);
