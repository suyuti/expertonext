const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

const Schema = mongoose.Schema;


var SchemaModel = new Schema({
    satisFirsati        : { type: mongoose.Schema.Types.ObjectId, ref: 'SatisFirsati2'},
    satisYapanPersonel  : { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    satisToplami        : { type: Number, default: 0},
    ilkFaturaToplami    : { type: Number, default: 0},
    createdAt           : { type: Date, default: Date.now },
    createdBy           : { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  },
    {
        versionKey: false,
        timestamps: true,
    }
);

SchemaModel.pre("save", function (next) {
    return next();
});

module.exports = mongoose.models.Satis || mongoose.model("Satis", SchemaModel);
