const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

const Schema = mongoose.Schema;

const satisFirsatiDurumlari = ['olusturuldu', 'randevuOlusturuldu', 'toplantiYapildi', 'teklifVerildi', 'kapandi']
const satisFirsatiSonulari = ['basarili', 'basarisiz', 'iptal']

var SchemaModel = new Schema({
    musteri         : { type: mongoose.Schema.Types.ObjectId, ref: 'Musteri'},
    seciliUrun      : { type: mongoose.Schema.Types.ObjectId, ref: 'Urun'},
    teklifFiyatlari: {
        onOdemeTutari       : { type: Number },
        basariTutari        : { type: Number },
        sabitTutar          : { type: Number },
        yuzdeTutari         : { type: Number },
        raporBasiOdemeTutari: { type: Number },
        sabitTutarAy        : { type: Number },
        fiyatDegisikligiVar : { type: Boolean },
        araToplam           : { type: Number },
        toplam              : { type: Number },
        indirim             : { type: Number },
        kdvOran             : { type: Number },
        kdv                 : { type: Number },
    },
    aciklama    : { type: String },
    onay        : {
        durum       : { type : Boolean, default: false},
        onaylayan   : { type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        onayTarihi  : { type: Date}
    },
    durum : { type: String, enum: satisFirsatiDurumlari, default: 'olusturuldu'},
    sonuc: {
        sonuclandiran   : { type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        sonucAciklama   : { type: String},
        sonucTarihi     : { type: Date}
    },
    createdAt: { type: Date, default: Date.now },
    createdBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    updatedAt: { type: Date },
    updatedBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
  },
    {
        versionKey: false,
        timestamps: true,
    }
);

SchemaModel.pre("save", function (next) {
    return next();
});

module.exports = mongoose.models.SatisFirsati || mongoose.model("SatisFirsati", SchemaModel);
