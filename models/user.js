import mongoose from "mongoose";
import MpathPlugin from 'mongoose-mpath';

const { String } = mongoose.Schema.Types;

var SchemaModel = new mongoose.Schema(
  {
    adi                       : { type: String, required: false },
    soyadi                    : { type: String, required: false },
    name                      : { type: String, required: false },
    displayName               : { type: String, required: false },
    email                     : { type: String, required: true, unique: true},
    password                  : { type: String, required: true, select: false},
    //role                      : { type: String,required: true,default: "new-user",enum: ["new-user", "admin", "satis-yonetici", 'satis-personel']},
    role                      : { type: String,required: true, default: "new-user"},
    aktif                     : { type: Boolean, default: false},
    //departman                 : {},
    createdAt                 : { type: Date, default: Date.now },
    //createdBy                 : { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    updatedAt                 : { type: Date },
    updatedBy                 : { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
/*    satisPersonelData: {
      satisHedefi                   : {type: Number},
      satisHedefiGuncellemeTarihi   : {type: Date},
      gorusmeHedefi                 : {type: Number},
      gorusmeHedefiGuncellemeTarihi : {type: Date},
      //oncekiHedefler                : []
    }
*/
  },
  {
    versionKey: false
    //timestamps: true,
    //toObject:{gettters: true}
  }
);


export default mongoose.models.User || mongoose.model("User", SchemaModel.plugin(MpathPlugin));
