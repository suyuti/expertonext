const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

const musteri = require('./musteri')

const Schema = mongoose.Schema;
const kisiYetki = ['yonetim', 'teknik', 'mali']

var SchemaModel = new Schema(
  {
    musteri   : { type: mongoose.Schema.Types.ObjectId, ref: 'Musteri', required: true  },
    adi       : { type: String, required: true  },
    soyadi    : { type: String, required: true  },
    unvani    : { type: String, required: false },
    cinsiyet  : { type: String, enum: ['Erkek', 'Kadın']},
    mail      : { type: String, required: false },
    cepTelefon: { type: String, required: false },
    isTelefon : { type: String, required: false },
    dahili    : { type: String, required: false },
    tckn      : { type: String, required: false },
    dogumTarihi: { type: Date},
    yetki     : { type: String, enum: kisiYetki },
    resim     : { type: String, required: false },
    notlar: [{
        tarih : { type: Date, default: Date.now },
        not   : { type: String },
        yazar : { type: mongoose.Schema.Types.ObjectId, ref: 'User'}
    }],
    createdAt: { type: Date, default: Date.now },
    createdBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    updatedAt: { type: Date },
    updatedBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
  },
  {
    versionKey: false,
    timestamps: true,
  }
);

SchemaModel.pre("save", function (next) {
    return next();
});

module.exports = mongoose.models.Kisi || mongoose.model("Kisi", SchemaModel);
