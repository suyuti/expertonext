import mongoose from "mongoose";

const { String } = mongoose.Schema.Types;
const Cinsiyet = ['Erkek', 'Kadın']

var SchemaModel = new mongoose.Schema(
  {
    user            : { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    adi             : { type: String, required: false},
    soyadi          : { type: String, required: false},
    cinsiyet        : { type: String, enum: Cinsiyet},
    dogumYeri       : { type: String },
    dogumTarihi     : { type: Date },
    evAdresi        : { type: String },
    telefonu        : { type: String },
    tckn            : { type: String }, // todo kvkk

    //satis           : {
        indirimOrani    : { type: Number, default: 0},
        satisHedefi     : { type: Number, default: 0},  // todo deprecated
        gorusmeHedefi   : { type: Number, default: 0},
    //},

    satisHedefleri : [
      {
        ay        : { type: String },
        hedefciro : { type: Number },
        hedeftoplanti:{ type: Number },
      }
    ],

    createdAt       : { type: Date, default: Date.now },
    createdBy       : { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    updatedAt       : { type: Date },
    updatedBy       : { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  },
  {
    versionKey: false
  }
);


export default mongoose.models.UserDetails || mongoose.model("UserDetails", SchemaModel);
