const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

import './musteri'
import './toplanti'

const Schema = mongoose.Schema;
const GorevDurumlari = ['ACIK', 'KAPALI', 'CALISIYOR']
const PriorityLevels = ['KRITIK', 'YUKSEK', 'ORTA', 'DUSUK']

var SchemaModel = new Schema(
  {
    baslik        : { type: String, required: true },
    musteri       : { type: mongoose.Schema.Types.ObjectId, ref: 'Musteri' },
    //proje         : { type: mongoose.Schema.Types.ObjectId, ref: 'User' },  // TODO  Gorevler musterinin projesine/sozlesmesine baglanacak.
    toplanti      : { type: mongoose.Schema.Types.ObjectId, ref: 'Toplanti' },
    personel      : { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    durum         : { type: String, enum: GorevDurumlari},
    icGorev       : { type: Boolean, default: false },
    ilerleme      : { type: Number },
    termin        : { type: Date },
    aciklama      : { type: String },
    priority      : { type: String, enum: PriorityLevels},
    sonucAciklama : { type: String },
    notlar        : [ {
      not         : { type: String, required: true },
      yazar       : { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
      tarih       : { type: Date}
    }],
    viewed        : { type: Boolean, default: false },
    viewedAt      : { type: Date},
    createdAt     : { type: Date, default: Date.now },
    createdBy     : { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    updatedAt     : { type: Date },
    updatedBy     : { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
  },
  {
    versionKey: false,
    timestamps: true,
  }
);

SchemaModel.pre("save", function (next) {
    return next();
});

module.exports = mongoose.models.Gorev || mongoose.model("Gorev", SchemaModel);
