const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

const sektor = require('./sektor')

const Schema = mongoose.Schema;
const musteriDurumlari = ['aktif', 'pasif', 'potansiyel', 'sorunlu']

var SchemaModel = new Schema(
  {
      marka             : {type: String, required: true},
      unvan             : {type: String, required: true},
      musteriTemsilcisi : {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
      musteriKaynagi    : {},
      sektor            : {type: mongoose.Schema.Types.ObjectId, ref: 'Sektor'},
      iletisim: {
          adres         : {type: String, required: false},
          faturaAdresi  : {type: String, required: false},
          telefon       : {type: String},
          il            : {type: String},
          ilce          : {type: String},
          osb           : {type: String},
          konum         : {type: String}, // TODO
          mail          : {type: String},
          webSitesi     : {type: String},
      },
      firmaBilgi: {
          vergiDairesi  : {type: String},
          vergiNo       : {type: String},
          eFaturaMi     : {type: Boolean, default: false},
          ciro          : {type: Number},
          kobiMi        : {type: Boolean, default: true}
      },
      kontaklar: {
          yonetimYetkili: { type: mongoose.Schema.Types.ObjectId, ref: 'Kisi' },
          teknikYetkili : { type: mongoose.Schema.Types.ObjectId, ref: 'Kisi' },
          maliYetkili   : { type: mongoose.Schema.Types.ObjectId, ref: 'Kisi' }
      },
      destekler: {
          oncekiDestekler   : [],
          danisman          : {type: String}
      },
      durum : { type: String, enum: musteriDurumlari, default: 'potansiyel'},
      notlar: [{
          tarih : { type: Date, default: Date.now },
          not   : { type: String },
          yazar : { type: mongoose.Schema.Types.ObjectId, ref: 'User'}
      }],
      createdAt: { type: Date, default: Date.now },
      createdBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
      updatedAt: { type: Date },
      updatedBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
  },
  {
    versionKey: false,
    timestamps: true,
  }
);

SchemaModel.pre("save", function (next) {
    return next();
});

module.exports = mongoose.models.Musteri ||  mongoose.model("Musteri", SchemaModel);
