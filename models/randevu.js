const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

import Musteri from './musteri'
import Kisi from './kisi'
import User from './user'
import SatisFirsati2 from './satisFirsati2'

const Schema = mongoose.Schema;
const randevuDurumlari = ["ACIK", "KAPALI", "IPTAL"];

var SchemaModel = new Schema(
  {
    musteri             : { type: mongoose.Schema.Types.ObjectId, ref: "Musteri" },
    firmaKatilimcilar   : [{ type: mongoose.Schema.Types.ObjectId, ref: "Kisi" }],
    personelKatilimcilar: [{ type: mongoose.Schema.Types.ObjectId, ref: "User" }],
    konum               : { type: String },
    zaman               : { type: Date },
    konu                : { type: String },
    aciklama            : { type: String },
    satisFirsati        : { type: mongoose.Schema.Types.ObjectId, ref: "SatisFirsati2"},
    durum               : { type: String, enum: randevuDurumlari, default: "ACIK" },

    createdAt           : { type: Date, default: Date.now },
    createdBy           : { type: mongoose.Schema.Types.ObjectId, ref: "User" },
    updatedAt           : { type: Date },
    updatedBy           : { type: mongoose.Schema.Types.ObjectId, ref: "User" },
  },
  {
    versionKey: false,
    timestamps: true,
  }
);

SchemaModel.pre("save", function (next) {
  return next();
});

module.exports = mongoose.models.Randevu || mongoose.model("Randevu", SchemaModel);
