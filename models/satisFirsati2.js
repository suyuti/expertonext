const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

import Musteri from './musteri'
import Randevu from './randevu'

const Schema = mongoose.Schema;

const satisFirsatiDurumlari = ['YENI', 'RANDEVU', 'TEKLIF', 'KAPALI']
const satisFirsatiSonulari = ['BASARILI', 'BASARISIZ']

var SchemaModel = new Schema({
    musteri         : { type: mongoose.Schema.Types.ObjectId, ref: 'Musteri'},
    atananPersonel  : { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    notlar          : [{ type: String }],
    durum           : { type: String, enum: satisFirsatiDurumlari, default:'YENI'},
    sonuc: {
        sonuc           : { type: String, enum: satisFirsatiSonulari},
        sonuclandiran   : { type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        sonucNotu       : { type: String},
        sonucTarihi     : { type: Date}
    },
    not: {type: String},
    onay        : {
        durum       : { type : Boolean, default: false},
        onyaNotu    : { type: String },
        onaylayan   : { type: mongoose.Schema.Types.ObjectId, ref: 'User'},
        onayTarihi  : { type: Date}
    },

    randevu: {type: mongoose.Schema.Types.ObjectId, ref: 'Randevu'},

    teklif: {
        teklifUrunleri: [{type: mongoose.Schema.Types.ObjectId, ref: 'Teklif'}],
        araToplam   : {type: Number},
        indirim     : {type: Number},
        toplam      : {type: Number},
    },

    history: [{
        action: {type: String},
        date: {type: Date},
        by: {type: mongoose.Schema.Types.ObjectId, ref: 'User'}
    }],

    createdAt: { type: Date, default: Date.now },
    createdBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    updatedAt: { type: Date },
    updatedBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
  },
    {
        versionKey: false,
        timestamps: true,
    }
);

SchemaModel.pre("save", function (next) {
    return next();
});

module.exports = mongoose.models.SatisFirsati2 || mongoose.model("SatisFirsati2", SchemaModel);
