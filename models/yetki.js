const mongoose = require("mongoose");
const Schema = mongoose.Schema;

var SchemaModel = new Schema(
  {
    //yetki       : { type: String, required: true  },
    rol         : { type: String, required: true  },
    yetkiler    : Object
    //deger       : { type: Boolean, default: false },
  },
  {
    versionKey: false,
    timestamps: true,
  }
);

module.exports = mongoose.models.Yetki || mongoose.model("Yetki", SchemaModel);
