import React, {useEffect} from 'react';
import { Card, CardHeader, Divider, CardContent, Typography, TextField, IconButton } from '@material-ui/core';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import AddIcon from '@material-ui/icons/Add';
import MaterialTable from 'material-table';
import useSWR from 'swr'
import baseUrl from '../../utils/baseUrl';
import {useRouter}  from 'next/router'

const FirmaTeklifler = props => {
	const {musteri} = props
    const router = useRouter()
	const fetcher = (...args) => fetch(...args).then(res => res.json());
	const {data, error} = useSWR(`${baseUrl}/api/musteriler/${musteri._id}/satisFirsatlari`, fetcher)


	return (
		<Card className="w-full border-t-4 border-red-500">
			<CardHeader
				title={
					<Typography className="text-xl" variant="caption">
						Teklifler
					</Typography>
				}
				subheader="Müşteriye yapilan teklifler"
				avatar={<AddShoppingCartIcon />}
				action={
					<IconButton 
						//component={Link} 
						//to={`/apps/satisfirsati/new?musteriId=${musteri._id}`}
						>
						<AddIcon />
					</IconButton>
				}
			/>
			<Divider />
			<CardContent>
				<MaterialTable 
					options={{
						showTitle: false,
						search: false
					}}
					columns={[
						{title: 'Urun', field:'durum'},
						{title: 'Sonuc', field:'durum'},
						{title: 'Durum', field:'durum'}
					]}
					data={data && data.data  || [] }
					onRowClick={(e, rowData) => {
						e.preventDefault()
						router.push(
                            '/satisfirsati/[id]',
                            `/satisfirsati/${rowData._id}`
                        )
					}}
				/>
			</CardContent>
		</Card>
	);
};
export default FirmaTeklifler;
