import React, { useEffect, useState } from "react";
import {
  Card,
  CardHeader,
  Divider,
  CardContent,
  Typography,
  TextField,
  IconButton,
  Chip,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Tooltip,
} from "@material-ui/core";
import PhoneIcon from "@material-ui/icons/Phone";
import AddIcon from "@material-ui/icons/Add";
import MaterialTable from "material-table";
//import GorusmelerTable from 'app/main/apps/components/gorusme/GorusmelerTable'
import useSWR from "swr";
import baseUrl from "../../utils/baseUrl";
import axios from "axios";
import moment from "moment";
import { useRouter } from "next/router";
import Link from '../../components/Link'

import EventIcon from '@material-ui/icons/Event';
import MailIcon from '@material-ui/icons/Mail';

const FirmaGorusmeler = (props) => {
  const { token, user } = props;
  const router = useRouter();

  moment.locale('tr')

  const { id } = router.query.id;
  //const [yeniUrl, setYeniUrl] = useState(`/satis/musteriler/${router.query.id}/gorusme`)

  const {
    data: gorusmeler,
    error: errorGorusmeler,
  } = useSWR(`${baseUrl}/api/musteriler/${router.query.id}/gorusmeler`, (url) =>
    axios
      .get(url, { headers: { Authentication: token } })
      .then((res) => res.data.data)
  );

  if (!gorusmeler) {
    return <></>
  }

  const kanalIcon = (kanal) => {
    if (kanal === 'telefon') {
      return (
        <Tooltip title='Telefon'>
          <PhoneIcon />
        </Tooltip>) 
    }
    else if (kanal === 'mail') {
      return (
        <Tooltip title='Mail'>
          <MailIcon />
        </Tooltip>) 
    }
    else if (kanal === 'toplanti') {
      return (
        <Tooltip title='Toplanti'>
          <EventIcon />
        </Tooltip>) 
    }
    return <></>
  }

  return (
    <Card className="w-full border-t-4 border-red-500">
      <CardHeader
        title={
          <Typography className="text-xl" variant="h6">
            Görüşmeler
          </Typography>
        }
        subheader="Müşteri ile yapilan gorusmeler"
        avatar={<PhoneIcon />}
        action={
          user.yetkiler.create_gorusme && (
              <IconButton
                onClick={(e) => {
                  e.preventDefault()
                  if (user.yetkiler.create_gorusme) {
                    router.push(
                      '/satis/gorusmeler/[id]',
                      `/satis/gorusmeler/new?musteri=${router.query.id}`
                    )
                  }
                }}
              >
                <AddIcon />
              </IconButton>
          )
        }
      />
      <Divider />
      <CardContent>
        <Table size='small'>
          <TableHead>
            <TableRow>
              <TableCell>Konu</TableCell>
              <TableCell>Kisi</TableCell>
              <TableCell>Kanal</TableCell>
              <TableCell>Zaman</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {gorusmeler && gorusmeler.map((g, i) => (
              <TableRow key={g._id}>
                <TableCell>{g.konu}</TableCell>
                <TableCell>{g.kisi.adi}</TableCell>
                <TableCell>{kanalIcon(g.kanal)}</TableCell>
                <TableCell>{moment(g.gorusmeZamani).format('Do MMMM YYYY, hh:mm')}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
        {/*
        <MaterialTable
          columns={[
            { title: "Konu", field: "konu" },
            {
              title: "Zaman",
              render: (rowData) => (
                <div className="flex-col">
                  <Typography className="text-sm">
                    {moment(rowData.gorusmeZamani).format("HH:mm DD.MM.YYYY")}
                  </Typography>
                  <Typography className="text-sm">
                    {moment(rowData.gorusmeZamani).endOf("day").fromNow()}
                  </Typography>
                </div>
              ),
            },
            { title: "Kanal", field: "konu" },
            {
              title: "Kişi",
              render: (rowData) => (
                <Chip label={`${rowData.kisi.adi} ${rowData.kisi.soyadi}`} />
              ),
            },
          ]}
          data={gorusmeler || []}
        />
        */}
      </CardContent>
    </Card>
  );
};
export default FirmaGorusmeler;
