import React, { useEffect, useState } from "react";
import {
  Card,
  CardHeader,
  Divider,
  CardContent,
  Typography,
  TextField,
  MenuItem,
  Button,
  CardActions,
  ButtonBase,
  Paper,
} from "@material-ui/core";
import AccountBalanceIcon from "@material-ui/icons/AccountBalance";
import * as Yetki from "../../utils/permissions";
import baseUrl from "../../utils/baseUrl";
import axios from "axios";
import useSWR from "swr";
import { useRouter } from "next/router";

import PriorityHighIcon from "@material-ui/icons/PriorityHigh"; // sorunlu musterı
import SentimentVerySatisfiedIcon from "@material-ui/icons/SentimentVerySatisfied"; // aktif
import SentimentVeryDissatisfiedIcon from "@material-ui/icons/SentimentVeryDissatisfied"; // pasif
import SentimentSatisfiedIcon from "@material-ui/icons/SentimentSatisfied"; // potansiyel

const FirmaBilgiler = (props) => {
  const { user, token } = props;
  const router = useRouter();

  const [musteri, setMusteri] = useState(null);
  const [editable, setEditable] = useState(user.yetkiler.edit_musteri);
  const [degisiklikVar, setDegisiklikVar] = useState(false);
  const [yeni, setYeni] = useState(router.query.id === "new");
  const { id } = router.query;

  const [yonetici, setYonetici] = useState(user.role==='satis-yonetici')

  const {
    data: musteriData,
    error: musteriError,
  } = useSWR(`${baseUrl}/api/musteriler/${id}`, (url) =>
    axios
      .get(url, { headers: { authorization: token } })
      .then((res) => res.data.data)
  );
  const {
    data: personeller,
    error: personellerError,
  } = useSWR(`${baseUrl}/api/users`, (url) =>
    axios
      .get(url, { headers: { authorization: token } })
      .then((res) => res.data.data)
  );
  const {
    data: sektorler,
    error: sektorlerError,
  } = useSWR(`${baseUrl}/api/sektorler`, (url) =>
    axios
      .get(url, { headers: { authorization: token } })
      .then((res) => res.data.data)
  );

  if (!musteri && musteriData) {
    setMusteri(musteriData);
  }

  if (!musteri) {
    return <></>;
  }

  const handleMusteriDurum = (durum) => {
    setDegisiklikVar(true);
    var m = { ...musteri };
    m.durum = durum;
    setMusteri(m);
  }

  const handleChange = (e) => {
    setDegisiklikVar(true);
    var m = { ...musteri };
    m[e.target.name] = e.target.value;
    setMusteri(m);
  };

  const save = async () => {
    if (yeni) {
      axios.post(`${baseUrl}/api/musteriler`, musteri, {
        headers: { authorization: token },
      });
    } else {
      axios.put(`${baseUrl}/api/musteriler/${id}`, musteri, {
        headers: { authorization: token },
      });
    }
  };

  return (
    <Card className="w-full border-t-4 border-red-200">
      <CardHeader
        title={
          <Typography className="text-xl" variant="h6">
            Müşteri Bilgileri
          </Typography>
        }
        subheader="Müşteri detayları"
        avatar={<AccountBalanceIcon fontSize="large" />}
      />
      <Divider />
      <CardContent>
        <div className="pb-4">
          <TextField
            //error={orjForm.Markasi !== form.Markasi}
            className="m-4"
            label="Marka"
            variant="outlined"
            fullWidth
            name="marka"
            value={musteri.marka}
            onChange={handleChange}
            InputProps={{
              readOnly: !editable,
            }}
          />
        </div>
        <div className="pb-4">
          <TextField
            //error={orjForm.Markasi !== form.Markasi}
            className="m-4"
            label="Unvan"
            variant="outlined"
            fullWidth
            name="unvan"
            value={musteri.unvan}
            onChange={handleChange}
            InputProps={{
              readOnly: !editable,
            }}
          />
        </div>
        <div className="pb-4">
          <TextField
            //error={orjForm.Markasi !== form.Markasi}
            className="m-4"
            label="Temsilci"
            variant="outlined"
            fullWidth
            name="musteriTemsilcisi"
            InputProps={{
              readOnly: !editable,
            }}
            //value={musteri.musteriTemsilcisi}
            //onChange={handleChange}
            select
          >
            {personeller &&
              personeller.map((p) => (
                <MenuItem key={p._id} value={p._id}>
                  {p.displayName}
                </MenuItem>
              ))}
          </TextField>
        </div>
        <div className="pb-4">
          <TextField
            //error={orjForm.Markasi !== form.Markasi}
            className="m-4"
            label="Sektor"
            variant="outlined"
            fullWidth
            name="sektor"
            InputProps={{
              readOnly: !editable,
            }}
            value={musteri.sektor}
            onChange={handleChange}
            select
          >
            {sektorler &&
              sektorler.map((s) => (
                <MenuItem key={s._id} value={s._id}>
                  {s.adi}
                </MenuItem>
              ))}
          </TextField>
        </div>
        <div className="pb-4">
          <TextField
            //error={orjForm.Markasi !== form.Markasi}
            className="m-4"
            label="Musteri Kaynagi"
            variant="outlined"
            fullWidth
            name="MusteriKaynagi"
            InputProps={{
              readOnly: !editable,
            }}
            //value={form.Marka}
            //onChange={handleChange}
          />
        </div>
        <div className="flex pb-4 justify-around m-8">
          <Paper className="rounded border " component={ButtonBase} disabled={!user.yetkiler.edit_musteri} onClick={() => handleMusteriDurum('potansiyel')}>
            <div className="flex-col text-center m-8">
              <SentimentSatisfiedIcon
                fontSize="large"
                color={
                  musteri.durum === "potansiyel" ? "secondary" : "disabled"
                }
              />
              <Typography
                color={
                  musteri.durum === "potansiyel" ? "secondary" : "disabled"
                }
              >
                Potansiyel
              </Typography>
            </div>
          </Paper>
          <Paper className="border" component={ButtonBase}  disabled={!user.yetkiler.edit_musteri} onClick={() => handleMusteriDurum('aktif')}>
            <div className="flex-col text-center m-8">
              <SentimentVerySatisfiedIcon
                fontSize="large"
                color={musteri.durum === "aktif" ? "secondary" : "disabled"}
              />
              <Typography
                color={musteri.durum === "aktif" ? "secondary" : "disabled"}
              >
                Aktif
              </Typography>
            </div>
          </Paper>
          <Paper className="rounded border " component={ButtonBase}  disabled={!user.yetkiler.edit_musteri} onClick={() => handleMusteriDurum('pasif')}>
            <div className="flex-col text-center m-8">
              <SentimentVeryDissatisfiedIcon
                fontSize="large"
                color={musteri.durum === "pasif" ? "secondary" : "disabled"}
              />
              <Typography
                color={musteri.durum === "pasif" ? "secondary" : "disabled"}
              >
                Pasif
              </Typography>
            </div>
          </Paper>
          <Paper className="rounded border " component={ButtonBase}  disabled={!user.yetkiler.edit_musteri} onClick={() => handleMusteriDurum('sorunlu')}>
            <div className="flex-col text-center m-8">
              <PriorityHighIcon
                fontSize="large"
                color={musteri.durum === "sorunlu" ? "secondary" : "disabled"}
              />
              <Typography
                color={musteri.durum === "sorunlu" ? "secondary" : "disabled"}
              >
                Sorunlu
              </Typography>
            </div>
          </Paper>
        </div>
      </CardContent>
      <Divider />
      <CardActions className="flex justify-end">
        <Button
          variant="contained"
          color="primary"
          onClick={save}
          disabled={
            !(
              (user.yetkiler.edit_musteri || user.yetkiler.create_musteri) &&
              degisiklikVar
            )
          }
        >
          Kaydet
        </Button>
      </CardActions>
    </Card>
  );
};

export default FirmaBilgiler;
