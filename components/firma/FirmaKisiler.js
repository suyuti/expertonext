import React, {useEffect, useState} from 'react';
import {
	Card,
	CardHeader,
	Divider,
	CardContent,
	Typography,
	TextField,
	IconButton,
	List,
	ListItem,
	ListItemAvatar,
	ListItemText,
	Avatar
} from '@material-ui/core';
import PersonIcon from '@material-ui/icons/Person';
import PeopleIcon from '@material-ui/icons/People';
import AddIcon from '@material-ui/icons/Add';

const FirmaKisiler = props => {
	const {musteri} = props

    const kisiler = [] // useSelector(({ satisApp }) => satisApp.kisiler.data)
    const [yonetim, setYonetim] = useState(null)
    const [teknik, setTeknik] = useState(null)
    const [mali, setMali] = useState(null)

    useEffect(() => {
        const init = () => {
            //dispatch(Actions.getKisiler({filter: {musteri: musteri._id}}))
        }
        init()
	}, [])
	
	useEffect(() => {
        if (kisiler) {
//            setYonetim(kisiler.filter(k => k.yetki === 'yonetim'))
//            setTeknik(kisiler.filter(k => k.yetki === 'teknik'))
//            setMali(kisiler.filter(k => k.yetki === 'mali'))
        }
    }, [kisiler])


	return (
		<Card className="w-full border-t-4 border-blue-200">
			<CardHeader
				title={
					<Typography className="text-xl" variant="h6">
						Kontak Kisiler
					</Typography>
				}
				subheader="Müşteri kontaklari"
				avatar={<PeopleIcon fontSize='large' />}
				action={
					<IconButton 
						//component={Link} 
						//to={`/apps/kisi/new?musteriId=${musteri._id}`}
						>
						<AddIcon/>
					</IconButton>
				}
			/>
			<Divider />
			<CardContent>
				<div className="flex justify-center">
					<div className="flex-col w-full w-1/3">
						<Typography className="text-600 text-center border-r-1 font-bold ">Yönetim</Typography>
						<Divider />
						<List className="w-full">
							{yonetim &&
								yonetim.map(y => (
									<ListItem 
										//component={Link} 
										//to={`/apps/kisi/${y._id}`}
										>
										<ListItemAvatar>
											<Avatar>
												<PersonIcon />
											</Avatar>
										</ListItemAvatar>
										<ListItemText primary={`${y.adi} ${y.soyadi}`} secondary={y.unvani} />
									</ListItem>
								))}
						</List>
					</div>
					<div className="flex-col w-full w-1/3">
						<Typography className="text-600 text-center font-bold ">Teknik</Typography>
						<Divider />
						<List className="w-full border-l-1 border-r-1 ">
							{teknik &&
								teknik.map(y => (
									<ListItem 
										//component={Link} 
										//to={`/apps/kisi/${y._id}`}
										>
										<ListItemAvatar>
											<Avatar>
												<PersonIcon />
											</Avatar>
										</ListItemAvatar>
										<ListItemText primary={`${y.adi} ${y.soyadi}`} secondary={y.unvani} />
									</ListItem>
								))}
						</List>
					</div>
					<div className="flex-col w-full w-1/3">
						<Typography className="text-600 text-center font-bold ">Mali</Typography>
						<Divider />
						<List className="w-full border-l-1">
							{mali &&
								mali.map(y => (
									<ListItem 
										//component={Link} 
										//to={`/apps/kisi/${y._id}`}
										>
										<ListItemAvatar>
											<Avatar>
												<PersonIcon />
											</Avatar>
										</ListItemAvatar>
										<ListItemText primary={`${y.adi} ${y.soyadi}`} secondary={y.unvani} />
									</ListItem>
								))}
						</List>
					</div>
				</div>
			</CardContent>
		</Card>
	);
};
export default FirmaKisiler;
