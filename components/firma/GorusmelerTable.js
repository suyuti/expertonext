import React, { useEffect, useState } from 'react';
import ReactPaginate from 'react-paginate';
//import {useDispatch, useSelector} from 'react-redux'
//import * as Actions from 'app/main/apps/satis/store/actions'

const GorusmeList = props => {
    const {data} = props
    return (
        <div>
            {data && data.map((d, i) => 
                <div key={i} className='flex'>
                    {d.konu}
                    {d.musteri}
                </div>
            )}
        </div>
    )
}

const GorusmelerTable = props => {
    const dispatch = useDispatch()
    const {musteri, kisi} = props
	const [filter, setFilter] = useState(null)

	const gorusmeler 	= [] //useSelector(({ satisApp }) => satisApp.gorusmeler.data);

    useEffect(() => {
        if (musteri) {
            setFilter({filter: {musteri:musteri._id}})
        }
        if (kisi) {
            setFilter({filter: {kisi:kisi._id}})
        }
    }, [])

    useEffect(() => {
        //dispatch(Actions.getGorusmeler(filter))
    }, [filter])
    
    const handlePageClick = data => {
		let selected = data.selected;
		let offset = Math.ceil(selected * this.props.perPage);

		this.setState({ offset: offset }, () => {
            setFilter({filter: {musteri: musteri._id}})
		});
	};

	return (
		<div>
            <GorusmeList data={gorusmeler}/>
			<ReactPaginate
				previousLabel={'previous'}
				nextLabel={'next'}
				breakLabel={'...'}
				breakClassName={'break-me'}
				//pageCount={this.state.pageCount}
				marginPagesDisplayed={2}
				pageRangeDisplayed={5}
				onPageChange={handlePageClick}
				containerClassName={'pagination'}
				subContainerClassName={'pages pagination'}
				activeClassName={'active'}
			/>
		</div>
	);
};

export default GorusmelerTable;
