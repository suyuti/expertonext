import React, { useEffect, useState } from "react";
import {
  Card,
  Checkbox,
  InputAdornment,
  CardHeader,
  Divider,
  CardContent,
  Typography,
  TextField,
  MenuItem,
  CardActions,
  Button
} from "@material-ui/core";
import AccountBalanceIcon from "@material-ui/icons/AccountBalance";
import * as Yetki from "../../utils/permissions";
import baseUrl from "../../utils/baseUrl";
import axios from "axios";
import useSWR from "swr";
import { useRouter } from "next/router";
import Snackbar from '../shared/SnackBar'


const UrunBilgiler = (props) => {
  const router = useRouter()
  const {user, token} = props
  const [editable, setEditable] = useState(user.yetkiler.edit_urun);
  const [degisiklikVar, setDegisiklikVar] = useState(false);
  const [yeni, setYeni] = useState(router.query.id === 'new')
  const {id} = router.query
  const [urun, setUrun] = useState(null)
  const [snackbarInfo, setSnackbarInfo] = useState({severity:'', message: ''})
  const [open, setOpen] = React.useState(false);

  const  {data: urunData,  error: urunError     } = useSWR(`${baseUrl}/api/urun/${id}`,  url => axios.get(url, {headers: {authorization: token}}).then(res => res.data.data))

  if (!urun && urunData) {
    setUrun(urunData)
  }

  if (!urun) {
    return <></>
  }
  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  const canSubmit = () => {
    return urun.adi && editable
  }

  const save = async () => {
    if (yeni) {
      axios.post(`${baseUrl}/api/urun`, urun, {headers: {authorization: token}})
      .then(resp => {
        if (resp.status === 200) {
          setSnackbarInfo({severity:'success', message:'Urun kaydedildi'})
        }
        else {
          setSnackbarInfo({severity:'error', message:'Urun kaydedilemedi'})
        }
        setOpen(true)
      })
    }
    else {
      axios.put(`${baseUrl}/api/urun/${id}`, urun, {headers: {authorization: token}})
      .then(resp => {
        if (resp.status === 200) {
          setSnackbarInfo({severity:'success', message:'Urun kaydedildi'})
        }
        else {
          setSnackbarInfo({severity:'error', message:'Urun kaydedilemedi'})
        }
        setOpen(true)
      })
    }
  }

  const handleChange = (e) => {
    setDegisiklikVar(true);
    var u = {...urun}
    u[e.target.name] = e.target.type === 'checkbox' ? e.target.checked : e.target.value
    setUrun(u)
  }

  return (
    <Card className="w-full border-t-4 border-red-200">
      <CardHeader
        title={
          <Typography className="text-xl" variant="caption">
            Ürün Bilgileri
          </Typography>
        }
        subheader="Ürün detayları"
        avatar={<AccountBalanceIcon />}
      />
      <Divider />
      <CardContent>
        <div className="flex mb-8">
          <TextField
            className="mb-24"
            label="Ürün Adı"
            autoFocus
            id="Adi"
            name="adi"
            value={urun.adi}
            onChange={handleChange}
            variant="outlined"
            required
            fullWidth
            InputProps={{
              readOnly: !editable,
            }}
          />
        </div>
        <div className="flex mb-8">
          <Checkbox
            className="mb-24"
            checked={urun.onOdemeTutariVar}
            name="onOdemeTutariVar"
            onChange={handleChange}
            //tabIndex={-1}
            disableRipple
            size="medium"
            disabled={!editable}
          />
          <TextField
            className="mb-24"
            label="Ön Ödeme Tutarı"
            id="onOdemeTutari"
            name="onOdemeTutari"
            //value={urun.onOdemeTutari}
            value={urun.onOdemeTutariVar ? urun.onOdemeTutari : ''}
            onChange={e => {
                if (e.target.value.match(/^(\s*|\d+)$/)) {
                    handleChange(e);
                }
            }}
            //type="text"
            variant="outlined"
            fullWidth
            disabled={!urun.onOdemeTutariVar}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">TL</InputAdornment>
              ),
              readOnly: !editable,
            }}

          />
        </div>
        <div className="flex mb-8">
          <Checkbox
            className="mb-24"
            checked={urun.sabitTutarVar}
            name="sabitTutarVar"
            onChange={handleChange}
            disableRipple
            size="medium"
            disabled={!editable}
          />
          <TextField
            className="mb-24"
            label="Sabit Tutar"
            id="sabitTutar"
            name="sabitTutar"
            value={urun.sabitTutarVar ? urun.sabitTutar : ""}
            onChange={e => {
                if (e.target.value.match(/^(\s*|\d+)$/)) {
                    handleChange(e);
                }
            }}
            type="text"
            variant="outlined"
            fullWidth
            disabled={!urun.sabitTutarVar}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">TL</InputAdornment>
              ),
              readOnly: !editable,
            }}
          />
        </div>
        <div className="flex mb-8">
          <Checkbox
            className="mb-24"
            checked={urun.basariTutariVar}
            name="basariTutariVar"
            onChange={handleChange}
            //tabIndex={-1}
            disableRipple
            size="medium"
            disabled={!editable}
          />
          <TextField
            className="mb-24"
            label="Başarı Tutarı"
            id="basariTutari"
            name="basariTutari"
            value={urun.basariTutariVar ? urun.basariTutari : ""}
            onChange={e => {
                if (e.target.value.match(/^(\s*|\d+)$/)) {
                    handleChange(e);
                }
            }}
            type="text"
            variant="outlined"
            fullWidth
            disabled={!urun.basariTutariVar}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">TL</InputAdornment>
              ),
              readOnly: !editable,
            }}
          />
        </div>
        <div className="flex mb-8">
          <Checkbox
            className="mb-24"
            checked={urun.raporBasiOdemeTutariVar}
            name="raporBasiOdemeTutariVar"
            onChange={handleChange}
            //tabIndex={-1}
            disableRipple
            size="medium"
            disabled={!editable}
          />
          <TextField
            className="mb-24"
            label="Rapor başı Tutarı"
            id="raporBasiOdemeTutari"
            name="raporBasiOdemeTutari"
            value={
              urun.raporBasiOdemeTutariVar ? urun.raporBasiOdemeTutari : ""
            }
            onChange={e => {
                if (e.target.value.match(/^(\s*|\d+)$/)) {
                    handleChange(e);
                }
            }}
            type="text"
            variant="outlined"
            fullWidth
            disabled={!urun.raporBasiOdemeTutariVar}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">TL</InputAdornment>
              ),
              readOnly: !editable,
            }}
          />
        </div>
        
        <div className="flex mb-8">
          <Checkbox
            className="mb-24"
            checked={urun.yuzdeTutariVar}
            name="yuzdeTutariVar"
            onChange={handleChange}
            //tabIndex={-1}
            disableRipple
            size="medium"
          />
          <TextField
            className="mb-24"
            label="Yüzde Tutarı"
            id="yuzdeTutari"
            name="yuzdeTutari"
            value={urun.yuzdeTutariVar ? urun.yuzdeTutari : ""}
            onChange={e => {
                if (e.target.value.match(/^(\s*|\d+)$/)) {
                    handleChange(e);
                }
            }}
            type="text"
            variant="outlined"
            fullWidth
            disabled={!urun.yuzdeTutariVar}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">%</InputAdornment>
              ),
            }}
          />
        </div>
        
        <div className='flex mb-8'>
        <TextField
          className="mb-24"
          label="KDV Oranı"
          id="kdvOrani"
          name="kdvOrani"
          value={urun.kdvOrani}
          InputProps={{              readOnly: !editable,
          }}
          onChange={e => {
            if (e.target.value.match(/^(\s*|\d+)$/)) {
                handleChange(e);
            }
        }}
          type="text"
          variant="outlined"
          fullWidth
          InputProps={{
            startAdornment: <InputAdornment position="start">%</InputAdornment>,
            readOnly: !editable,
          }}
        />
        </div>
        <div className='flex mb-8'>
        <TextField
          className="mb-24"
          label="Ürün Kısa Kod"
          id="kisaKod"
          name="kisaKod"
          value={urun.kisaKod}
          onChange={handleChange}
          //onChange={TutarInputHandler}
          type="text"
          variant="outlined"
          fullWidth
          InputProps={{
            readOnly: !editable,

          }}
        />
        </div>
        <div className='flex mb-8'>
        <TextField
          className="mb-24"
          label="Renk"
          id="urunRenk"
          name="urunRenk"
          value={urun.urunRenk}
          onChange={handleChange}
          type="text"
          variant="outlined"
          fullWidth
          InputProps={{
            readOnly: !editable,

          }}
        />
        </div>
      </CardContent>
      <Divider />
      <CardActions>
        <div className='flex w-full justify-end'>
          {(user.yetkiler.edit_urun || user.yetkiler.create_urun)  && (
              <Button 
                variant="contained" 
                color="primary" 
                onClick={save}
                disabled={!canSubmit()}>
                Kaydet
              </Button>
            )}
          {open && 
            <Snackbar open={open} severity={snackbarInfo.severity} message={snackbarInfo.message} handleClose={handleClose}/>
          }
        </div>
      </CardActions>
    </Card>
  );
};

export default UrunBilgiler;
