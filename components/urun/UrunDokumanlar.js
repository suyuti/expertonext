import React, { useEffect, useState } from "react";
import {
  Card,
  Checkbox,
  InputAdornment,
  CardHeader,
  Divider,
  CardContent,
  Typography,
  TextField,
  MenuItem,
} from "@material-ui/core";
import AccountBalanceIcon from "@material-ui/icons/AccountBalance";

const UrunDokumanlar = (props) => {
  const { data } = props;

  return (
    <Card className="w-full border-t-4 border-red-200">
      <CardHeader
        title={
          <Typography className="text-xl" variant="caption">
            Ürün Dokumanlari
          </Typography>
        }
        subheader="Ürün dokumanlari"
        avatar={<AccountBalanceIcon />}
      />
      <Divider />
      <CardContent>
      </CardContent>
    </Card>
  );
};

export default UrunDokumanlar;
