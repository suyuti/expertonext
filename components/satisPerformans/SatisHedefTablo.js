import React, { useState } from "react";
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  makeStyles,
  Paper,
  TextField,
} from "@material-ui/core";
import MaterialTable from "material-table";
import { DatePicker, KeyboardDatePicker } from "@material-ui/pickers";
import axios from "axios";
import baseUrl from "../../utils/baseUrl";

const useStyles = makeStyles((theme) => ({
    table: {
      border: "none",
      boxShadow: "none",
    },
  }));

const aylar = ['Ocak', 'Şubat', 'Mart', 'Nisan', 'Mayıs', 'Haziran', 'Temmuz', 'Ağustos', 'Eylül', 'Ekim', 'Kasım', 'Aralık']
  

const SatisHedefTablo = (props) => {
  const { data, personel, token } = props;
  const [selectedDate, handleDateChange] = useState(new Date());
  const classes = useStyles();

  //console.log(data)

    const satisHedefiEkle = (hedef) => {
        axios.post(`${baseUrl}/api/users/${personel._id}/satishedefi`, hedef, {headers: {authorization: token}})//.then(resp => console.log(resp.data))
    }



  return (
    <div className=" p-8 w-full">
      <MaterialTable
        title="Satış hedefleri"
        data={data}
        columns={[
          {
            title: "Ay",
            field: "ay",
            render: rowData => {
              let yil = rowData.ay.substring(0, 4)
              let ay = parseInt(rowData.ay.substring(4, 6))
              return `${aylar[ay-1]} / ${yil}`
            },
            editComponent: (props) => (
              <DatePicker
                autoFocus
                views={["year", "month"]}
                label="Hedef Ay"
                value= {props.value}//{selectedDate}
                onChange={handleDateChange}
                animateYearScrolling
                fullWidth
                minDate={new Date("2020-03-01")}
                inputVariant="outlined"
                onChange={e => {
                  props.onChange(e)}}
                //InputAdornmentProps={{ position: "end" }}
                //variant="inline"
              />
            ),
          },
          { title: "Ciro Hedef", 
            field: "hedefciro",
            editComponent: props => (
                <TextField 
                    label='Satış hedefi'
                    variant='outlined'
                    fullWidth
                    value={props.value}
                    onChange={e => props.onChange(e.target.value)}
                />
            )
        },
        { title: "Gerçekleşen Ciro", 
          field: "gerceklesenciro", 
          editable: 'never', 
          render: rowData => {
            if (rowData) {
              return `${rowData.gerceklesenciro}   (% ${Math.floor((rowData.gerceklesenciro/rowData.hedefciro)*100)})` 
            }
            else {
              return ''
            }
          }
        },
        { title: "Toplantı Hedef", field: "hedeftoplanti", editable: 'never' },
        { title: "Gerçekleşen Toplantı", field: "gerceklesentoplanti", editable: 'never' },
      ]}
        localization={{
            body: {
              emptyDataSourceMessage: "Herhangi bir hedef tanımlanmadı",
            },
          }}

        options={{
          showTitle: false,
          search: false,
          padding: 'dense',
          paging: false
        }}
        editable={{
          onRowAdd: (newData) => {
            satisHedefiEkle(newData)
          },
          //onRowUpdate: (newData, oldData) => {},
          //onRowDelete: oldData => {}
        }}
        components={{
            Container: (props) => (
              <Paper className={classes.table} {...props} />
            ),
          }}

      />
    </div>
  );
};

export default SatisHedefTablo;
