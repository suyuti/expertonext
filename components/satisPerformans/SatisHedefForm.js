import React, { useState } from "react";
import { TextField, Button } from "@material-ui/core";
import { DatePicker, KeyboardDatePicker } from "@material-ui/pickers";

const SatisHedefForm = (props) => {
  const { personel } = props;
  const [selectedDate, handleDateChange] = useState(new Date());
  return (
    <div className="flex m-4">
      <div className="mr-4 w-full">
        <DatePicker
          autoFocus
          views={["year", "month"]}
          label="Hedef Ay"
          value={selectedDate}
          onChange={handleDateChange}
          animateYearScrolling
          fullWidth
          inputVariant="outlined"
          InputAdornmentProps={{ position: "end" }}
          variant="inline"
        />
      </div>
      <div className="mr-4 w-full">
        <TextField label="Satış hedefi?" variant="outlined" fullWidth />
      </div>
      <Button variant="contained" color="primary">
        Ekle
      </Button>
    </div>
  );
};

export default SatisHedefForm;
