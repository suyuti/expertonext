import React from "react";
import SatisHedefForm from "./SatisHedefForm";
import SatisPerformansGrafik from "./SatisPerformansGrafik";
import SatisHedefTablo from "./SatisHedefTablo";
import { Typography, Paper, Avatar } from "@material-ui/core";
import SatisPerformansToplamlar from "./SatisPerformansToplamlar";
import useSWR from "swr";
import baseUrl from "../../utils/baseUrl";
import axios from "axios";

const PersonelSatisPerformans = (props) => {
  const { personel, token } = props;

  //const {data:personelPerformansData, error} = useSWR(`${baseUrl}/api/users/${personel._id}/detail`, 
  const {data:personelPerformansData, error} = useSWR(`${baseUrl}/api/users/${personel._id}/satisperformans`, 
    (url) => axios.get(url, {headers: {authorization: token}})
              .then(res => { return res.data}))

  if (!personelPerformansData) {
    return <></>
  }


  return (
    <Paper className="m-4 p-4 border-t-4 border-red">
      <div className="flex-col m-4 p-4 w-full">
        <div className="flex">
          <Avatar title="MSD"></Avatar>
          <Typography variant="h4">{`${personel.adi} ${personel.soyadi}`}</Typography>
        </div>
        <div className="flex  w-full">
          <div className="flex-col w-full">
            <SatisPerformansGrafik />
            <div className="m-4 p-4">
              <SatisPerformansToplamlar />
            </div>
          </div>
          <div className="ml-8 w-full">
            <SatisHedefTablo data={personelPerformansData.hedefler} {...props}/>
          </div>
        </div>
      </div>
    </Paper>
  );
};

export default PersonelSatisPerformans;
