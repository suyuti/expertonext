import React from "react";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles(theme => (
    {
        baslik: {
            fontWeight: 100,
            fontSize: 18 
        },
        rakam: {
            fontSize: 38,
            fontWeight: 400
        }
    }
))


const SatisPerformansToplamlar = (props) => {
    const classes = useStyles()

  return (
    <div className="flex justify-around m-8">
      <div className="flex-col text-center">
        <div className={classes.baslik}>Toplam Satis</div>
        <div className={classes.rakam}>150000</div>
      </div>
      <div className="flex-col text-center">
      <div className={classes.baslik}>Görüşme</div>
        <div className={classes.rakam}>45</div>
      </div>
      <div className="flex-col text-center">
      <div className={classes.baslik}>Müşteri</div>
        <div className={classes.rakam}>12</div>
      </div>
    </div>
  );
};
export default SatisPerformansToplamlar;
