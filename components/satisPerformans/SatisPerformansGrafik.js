import React from "react";
import Chart from "react-google-charts";

const SatisPerformansGrafik = (props) => {
  const { personel } = props;
  return (
    <div className='w-full'>
      <Chart
        width={"800px"}
        height={"300px"}
        chartType="AreaChart"
        loader={<div>Grafik yükleniyor...</div>}
        data={[
          ["Ay", "Satış", "Hedef"],
          ["2020-01", 1000, 400],
          ["2020-02", 1170, 460],
          ["2020-03", 660, 1120],
          ["2020-03", 660, 1120],
          ["2020-03", 6160, 1120],
          ["2020-03", 660, 11240],
          ["2020-03", 6620, 1120],
          ["2020-03", 660, 1120],
          ["2020-03", 6360, 14120],
          ["2020-03", 660, 1120],
          ["2020-04", 1030, 540],
        ]}
        options={{
          title: "Satış Performansı",
          hAxis: { title: "Ay", titleTextStyle: { color: "#333" } },
          vAxis: { minValue: 0 },
          chartArea: { width: "80%", height: "70%" },
        }}
        // For tests
        rootProps={{ "data-testid": "1" }}
      />{" "}
    </div>
  );
};

export default SatisPerformansGrafik;
