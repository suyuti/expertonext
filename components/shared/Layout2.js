import React from "react";
import clsx from "clsx";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import {
  Drawer,
  AppBar,
  Collapse,
  Toolbar,
  List,
  CssBaseline,
  Typography,
  Divider,
  IconButton,
  MenuItem,
  ListItem,
  Menu,
  ListItemIcon,
  ListItemText,
} from "@material-ui/core";
import AccountBalanceIcon from "@material-ui/icons/AccountBalance"; // musterı
import ToysIcon from "@material-ui/icons/Toys"; // urunler
import PeopleIcon from "@material-ui/icons/People"; // kısıler
import PriorityHighIcon from "@material-ui/icons/PriorityHigh"; // sorunlu musterı
import ReportProblemIcon from "@material-ui/icons/ReportProblem";

import SentimentVerySatisfiedIcon from "@material-ui/icons/SentimentVerySatisfied"; // aktif
import SentimentVeryDissatisfiedIcon from "@material-ui/icons/SentimentVeryDissatisfied"; // pasif
import SentimentSatisfiedIcon from "@material-ui/icons/SentimentSatisfied"; // potansiyel

import PersonOutlineIcon from "@material-ui/icons/PersonOutline"; // personel

import VpnKeyIcon from "@material-ui/icons/VpnKey"; // yetkı

import TrendingUpIcon from "@material-ui/icons/TrendingUp"; // satıs performans

import EventIcon from '@material-ui/icons/Event'; // Toplantılar

import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";
import SendIcon from "@material-ui/icons/Send";
import Link from "../Link";
import HomeIcon from "@material-ui/icons/Home";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import AccountCircle from "@material-ui/icons/AccountCircle";
import Footer from "../Footer";
import { handleLogout } from "../../utils/auth";
import * as Yetki from "../../utils/permissions";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    backgroundColor: '#E54D03',
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
  },
  hide: {
    display: "none",
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: "nowrap",
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: "hidden",
    width: theme.spacing(7) + 1,
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(9) + 1,
    },
  },
  toolbar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    //padding: theme.spacing(3),
  },
}));

export default function MiniDrawer(props) {
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(true);
  const [musteriOpen, setMusteriOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const [anchorEl, setAnchorEl] = React.useState(null);
  const menuopen = Boolean(anchorEl);
  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  //const {pageProps} = props
  const { yetkiler, user } = props;

  ////console.log(props)
  return (
    <div className="flex">
      <CssBaseline />
      <AppBar
        //color="primary"
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar
          //className={clsx(classes.toolbar, 'flex justify-between')} 
        className="flex justify-between"
        >
          <div className="flex items-center">
            <IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={handleDrawerOpen}
              edge="start"
              className={clsx(classes.menuButton, {
                [classes.hide]: open,
              })}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h5" noWrap>
              Experto
            </Typography>
          </div>

          <div className="">
            <IconButton
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleMenu}
              color="inherit"
            >
              <AccountCircle />
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={anchorEl}
              anchorOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              keepMounted
              transformOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              open={menuopen}
              onClose={handleClose}
            >
              <MenuItem component={Link} href="/me">
                Profil
              </MenuItem>
              <Divider />
              <MenuItem onClick={handleLogout}>Çıkış</MenuItem>
            </Menu>
            <Typography>{props.user.displayName}</Typography>
          </div>
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}
      >
        <div className={classes.toolbar}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === "rtl" ? (
              <ChevronRightIcon />
            ) : (
              <ChevronLeftIcon />
            )}
          </IconButton>
        </div>
        <Divider />
        <List>
          <ListItem button component={Link} naked href="/">
            <ListItemIcon>
              <HomeIcon fontSize="large" />
            </ListItemIcon>
            <ListItemText primary="Ana Ekran" />
          </ListItem>
          <Divider />

          {user.yetkiler.list_satisperformans && (
            <>
              <ListItem button component={Link} naked href="/satis/performans">
                <ListItemIcon>
                  <TrendingUpIcon fontSize="large" />
                </ListItemIcon>
                <ListItemText primary="Satış Performans" />
              </ListItem>
              <Divider />
            </>
          )}

          <ListItem button component={Link} naked href="/satis/ekip">
                <ListItemIcon>
                  <TrendingUpIcon fontSize="large" />
                </ListItemIcon>
                <ListItemText primary="Ekip" />
              </ListItem>
              <Divider />


          {user.yetkiler.list_toplanti && (
            <>
              <ListItem button component={Link} naked href="/satis/toplantilar">
                <ListItemIcon>
                  <EventIcon fontSize="large" />
                </ListItemIcon>
                <ListItemText primary="Toplantılar" />
              </ListItem>
              <Divider />
            </>
          )}

          {user.yetkiler.list_musteri && (
            <>
              <ListItem
                button
                onClick={() => {
                  setMusteriOpen(!musteriOpen);
                }}
              >
                <ListItemIcon>
                  <AccountBalanceIcon fontSize="large" />
                </ListItemIcon>
                <ListItemText primary="Müşteriler" />
                {musteriOpen ? <ExpandLess /> : <ExpandMore />}
              </ListItem>
              <Collapse in={musteriOpen} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>
                  {user.yetkiler.list_musteri && (
                    <div className="pl-8">
                      <ListItem
                        button
                        component={Link}
                        naked
                        href="/satis/musteriler?durum=potansiyel"
                      >
                        <ListItemIcon>
                          <SentimentSatisfiedIcon fontSize="large" />
                        </ListItemIcon>
                        <ListItemText primary="Potansiyel Müşteriler" />
                      </ListItem>
                    </div>
                  )}
                  {user.yetkiler.list_musteri && (
                    <div className="pl-8">
                      <ListItem
                        button
                        component={Link}
                        naked
                        href="/satis/musteriler?durum=aktif"
                      >
                        <ListItemIcon>
                          <SentimentVerySatisfiedIcon fontSize="large" />
                        </ListItemIcon>
                        <ListItemText primary="Aktif Müşteriler" />
                      </ListItem>
                    </div>
                  )}
                  {user.yetkiler.list_musteri && (
                    <div className="pl-8">
                      <ListItem
                        button
                        component={Link}
                        naked
                        href="/satis/musteriler?durum=pasif"
                      >
                        <ListItemIcon>
                          <SentimentVeryDissatisfiedIcon fontSize="large" />
                        </ListItemIcon>
                        <ListItemText primary="Pasif Müşteriler" />
                      </ListItem>
                    </div>
                  )}
                  {user.yetkiler.list_musteri && (
                    <div className="pl-8">
                      <ListItem
                        button
                        component={Link}
                        naked
                        href="/satis/musteriler?durum=sorunlu"
                      >
                        <ListItemIcon>
                          <PriorityHighIcon fontSize="large" />
                        </ListItemIcon>
                        <ListItemText primary="Sorunlu Müşteriler" />
                      </ListItem>
                    </div>
                  )}
                </List>
              </Collapse>
            </>
          )}

          {user.yetkiler.list_urun && (
            <ListItem button component={Link} naked href="/satis/urunler">
              <ListItemIcon>
                <ToysIcon fontSize="large" />
              </ListItemIcon>
              <ListItemText primary="Ürünler" />
            </ListItem>
          )}

          {user.yetkiler.list_kisi && (
            <ListItem button component={Link} naked href="/satis/kisiler">
              <ListItemIcon>
                <PeopleIcon fontSize="large" />
              </ListItemIcon>
              <ListItemText primary="Kontak Kişiler" />
            </ListItem>
          )}
        </List>
        <Divider />
        {user.yetkiler.list_personel && (
          <ListItem button component={Link} naked href="/satis/personeller">
            <ListItemIcon>
              <PersonOutlineIcon fontSize="large" />
            </ListItemIcon>
            <ListItemText primary="Personeller" />
          </ListItem>
        )}
        {user.yetkiler.list_yetki && (
          <ListItem button component={Link} naked href="/admin/yetki">
            <ListItemIcon>
              <VpnKeyIcon fontSize="large" />
            </ListItemIcon>
            <ListItemText primary="Yetkiler" />
          </ListItem>
        )}
        {user.yetkiler.list_departman && (
          <ListItem button component={Link} naked href="/admin/departman">
            <ListItemIcon>
              <VpnKeyIcon fontSize="large" />
            </ListItemIcon>
            <ListItemText primary="Departmanlar" />
          </ListItem>
        )}
      </Drawer>
      <main className={classes.content}>
        <div className={classes.toolbar} />
        {React.cloneElement(props.children, { ...props })}
      </main>
    </div>
  );
}
