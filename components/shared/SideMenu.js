import React, { useState } from "react";
import {
  Collapse,
  Drawer,
  makeStyles,
  Typography,
  List,
  ListItem,
  ListItemText,
  ListItemIcon,
  ListSubheader,
  Divider,
} from "@material-ui/core";
import SendIcon from "@material-ui/icons/Send";
import DraftsIcon from "@material-ui/icons/Drafts";
import InboxIcon from "@material-ui/icons/Inbox";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import StarBorder from "@material-ui/icons/StarBorderOutlined";
import Link from "../Link";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  drawer: {
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  toolbar: theme.mixins.toolbar,
  root: {
    width: "100%",
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
}));

const SideMenu = (props) => {
  const { drawerOpen } = props;
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const [musteriOpen, setMusteriOpen] = useState(false);

  const handleClick = (e) => {
    setOpen(!open);
  };

  return (
    <Drawer
      variant="persistent"
      open={drawerOpen}
      className={classes.drawer}
      classes={{
        paper: classes.drawerPaper,
      }}
    >
      <div className={classes.toolbar}></div>
      <List
        component="nav"
        aria-labelledby="nested-list-subheader"
        subheader={
          <ListSubheader component="div" id="nested-list-subheader">
            Nested List Items
          </ListSubheader>
        }
        className={classes.root}
      >
        <ListItem button onClick={() => {setMusteriOpen(!musteriOpen)}}>
          <ListItemIcon>
            <InboxIcon />
          </ListItemIcon>
          <ListItemText primary="Müşteriler" />
          {musteriOpen ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
        <Collapse in={musteriOpen} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            <ListItem button component={Link} naked href="/satis/musteriler">
              <ListItemIcon>
                <SendIcon />
              </ListItemIcon>
              <ListItemText primary="Potansiyel Musteriler" />
            </ListItem>
            <ListItem button component={Link} naked href="/satis/musteriler">
              <ListItemIcon>
                <SendIcon />
              </ListItemIcon>
              <ListItemText primary="Aktif Musteriler" />
            </ListItem>
            <ListItem button component={Link} naked href="/satis/musteriler">
              <ListItemIcon>
                <SendIcon />
              </ListItemIcon>
              <ListItemText primary="Pasif Musteriler" />
            </ListItem>
            <ListItem button component={Link} naked href="/satis/musteriler">
              <ListItemIcon>
                <SendIcon />
              </ListItemIcon>
              <ListItemText primary="Sorunlu Musteriler" />
            </ListItem>
          </List>
        </Collapse>

        <ListItem button component={Link} naked href="/satis/urunler">
          <ListItemIcon>
            <SendIcon />
          </ListItemIcon>
          <ListItemText primary="Urunler" />
        </ListItem>
        <ListItem button component={Link} naked href="/satis/kisiler">
          <ListItemIcon>
            <SendIcon />
          </ListItemIcon>
          <ListItemText primary="Kontak Kisiler" />
        </ListItem>
        <ListItem button component={Link} naked href="/">
          <ListItemIcon>
            <DraftsIcon />
          </ListItemIcon>
          <ListItemText primary="Home" />
        </ListItem>
        <Divider />
        <ListItem button onClick={handleClick}>
          <ListItemIcon>
            <InboxIcon />
          </ListItemIcon>
          <ListItemText primary="Inbox" />
          {open ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
        <Collapse in={open} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            <ListItem button className={classes.nested}>
              <ListItemIcon>
                <StarBorder />
              </ListItemIcon>
              <ListItemText primary="Starred" />
            </ListItem>
          </List>
        </Collapse>
      </List>{" "}
    </Drawer>
  );
};
export default SideMenu;
