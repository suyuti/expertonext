import React, { useState } from "react";
import {
  Typography,
  AppBar,
  Toolbar,
  IconButton,
  Button,
  Drawer,
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";
import { withStyles } from "@material-ui/core/styles";
import SideMenu from "./SideMenu";
const drawerWidth = 240;

const styles = (theme) => ({
  root: {
    display: "flex",
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  drawer: {
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    //padding: theme.spacing.unit * 3,
  },
  toolbar: theme.mixins.toolbar,
  shiftTextLeft: {
    marginLeft: "0px",
  },
  shiftTextRight: {
    marginLeft: drawerWidth,
  },
});

const Layout = ({ children, classes }) => {
  const [drawerOpen, setDrawerOpen] = useState(false);

  return (
    <div className='flex w-full'>
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <IconButton
            edge="start"
            color="inherit"
            aria-label="menu"
            onClick={() => setDrawerOpen(!drawerOpen)}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6">Experto</Typography>
          <Button color="inherit">Login</Button>
        </Toolbar>
      </AppBar>

      <SideMenu drawerOpen={drawerOpen} />

      <main className='flex-grow p-3 bg-teal-300'>
        <div
          className={
            `w-full bg-yellow-400  ${drawerOpen ? classes.shiftTextRight : classes.shiftTextLeft}`
          }
        >
          <div className={classes.toolbar} />
          <div className='bg-indigo-500'>
          {children}

          </div>
        </div>
      </main>
    </div>
  );
};

export default withStyles(styles)(Layout);
