import React from "react";
import {
  Card,
  CardHeader,
  Divider,
  CardContent,
  Table,
  TableRow,
  TableCell,
  Chip,
  IconButton,
  Typography,
  TableHead,
} from "@material-ui/core";
import DoneIcon from "@material-ui/icons/Done";
import AddIcon from "@material-ui/icons/Add";
import ErrorOutlineIcon from "@material-ui/icons/ErrorOutline";
import MaterialTable from "material-table";
import useSWR from "swr";
import baseUrl from "../../utils/baseUrl";
import { useRouter } from "next/router";
import axios from "axios";
import moment from "moment";
import EventIcon from "@material-ui/icons/Event";

const Randevular = (props) => {
  const router = useRouter();
  const { token, user } = props;

  const { data: randevular, error } = useSWR(
    `${baseUrl}/api/randevular`,
    (url) =>
      axios
        .get(url, {
          params: { filter: { durum: "ACIK" } },
          headers: { authorization: token },
        })
        .then((resp) => resp.data)
  );

  return (
    <Card className="w-full border-t-4 border-yellow-600">
      <CardHeader
        title={<Typography variant="h6">Randevular</Typography>}
        avatar={<EventIcon fontSize="large" />}
        action={
          <IconButton>
            <AddIcon fontSize="large" />
          </IconButton>
        }
      />
      <Divider />
      <CardContent>
        <div className="flex p-8">
          <Table size="small">
            <TableHead>
              <TableRow>
                <TableCell>Müşteri</TableCell>
                <TableCell>Personel</TableCell>
                <TableCell>Zaman</TableCell>
                <TableCell>Konum</TableCell>
                <TableCell>Konu</TableCell>
              </TableRow>
            </TableHead>
            {randevular &&
              randevular.map((r, i) => (
                <TableRow
                  hover
                  key={r._id}
                  onClick={(e) => {
                    e.preventDefault();
                    if (user.yetkiler.view_randevu) {
                      router.push(
                        "/satis/randevular/[id]",
                        `/satis/randevular/${r._id}`
                      );
                    }
                  }}
                >
                  <TableCell>{r.musteri.marka}</TableCell>
                  <TableCell></TableCell>
                  <TableCell>
                    {moment(r.zaman).format("DD.MM.YYYY HH:mm")}
                  </TableCell>
                  <TableCell>{r.konum}</TableCell>
                  <TableCell>{r.konu}</TableCell>
                </TableRow>
              ))}
          </Table>
        </div>
      </CardContent>
    </Card>
  );
};

export default Randevular;
