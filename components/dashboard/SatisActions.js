import React from "react";
import { Button, Paper } from "@material-ui/core";
import Link from "../../components/Link";
import { useRouter } from "next/router";

const SatisActions = (props) => {
  const router = useRouter();
  const { user } = props;
  return (
    <Paper className="w-full p-8 flex justify-end border-t-4 border-green-500">
      {user.yetkiler.create_satisfirsati && (
        <Button
          onClick={(e) => {
            e.preventDefault();

            router.push("/satis/sfs/[id]", "/satis/sfs/new");
          }}
          className="ml-2"
          variant="outlined"
          color="secondary"
        >
          Satis Firsati
        </Button>
      )}
      {user.yetkiler.create_gorusme && (
        <div className="ml-4">
          <Button
            onClick={(e) => {
              e.preventDefault();

              router.push(
                "/satis/gorusmeler/[id]",
                "/satis/gorusmeler/new"
              );
            }}
            variant="outlined"
            color="primary"
          >
            Gorusme
          </Button>
        </div>
      )}
      {user.yetkiler.create_randevu && (
        <div className="ml-4">
          <Button
            onClick={(e) => {
              e.preventDefault();

              router.push(
                "/satis/randevular/[id]",
                "/satis/randevular/new"
              );
            }}
            variant="outlined"
            color="primary"
          >
            Randevu
          </Button>
        </div>
      )}
      {user.yetkiler.create_gorev && (
        <div className="ml-4">
          <Button
            onClick={(e) => {
              e.preventDefault();

              router.push(
                "/satis/gorevler/[id]",
                "/satis/gorevler/new"
              );
            }}
            variant="outlined"
            color="primary"
          >
            Görev
          </Button>
        </div>
      )}

    </Paper>
  );
};

export default SatisActions;
