import React, { useState } from "react";
import { Typography, Divider, Box, Tabs, Tab } from "@material-ui/core";
import SatisActions from "./SatisActions";
import SatisFirsatlari from "./SatisFirsatlari";
import SatisFirsatlariWidget from "./SatisFirsatlariWidget";
import GorusmeWidget from "./GorusmeWidget";
import Randevular from "./Randevular";
import SwipeableViews from "react-swipeable-views";
import EkipList from "../widgets/ekip/EkipListWidget";

import DashboardIcon from "@material-ui/icons/Dashboard";
import PeopleOutlineIcon from "@material-ui/icons/PeopleOutline";
import Gorevler from "./Gorevler";
import SatisHedefi from "../widgets/SatisHedefi";
import InfoBox from "../widgets/InfoBox";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

const SatisDashboard = (props) => {
  const { user } = props;
  const [activeTab, setActiveTab] = useState(0);
  const [yonetim, setYonetim] = useState(user.role === "Satış Yöneticisi");

  //console.log(user)

  return (
    <div className="flex-col">
      <Tabs value={activeTab} onChange={(e, n) => setActiveTab(n)}>
        <Tab index={0} label="Satış Masası" icon={<DashboardIcon />}></Tab>
        {(user.yetkiler.list_satisperformans && yonetim) && (
          <Tab index={1} label="Ekip" icon={<PeopleOutlineIcon />}></Tab>
        )}
      </Tabs>


      <TabPanel value={activeTab} index={0} className="border">
        <div className="flex-wrap w-full">
          <div className="m-4 sm:w-1/1">
            <SatisActions user={user} />
          </div>
          {user.role === 'Satış Personel' && 
            <div className="sm:w-1/4">
              <InfoBox 
                title='Satış Hedefi' 
                value={0} 
                {...props}
                />
            </div>
          }
          {user.yetkiler.list_satisfirsati && (
            <div className="m-4 sm:w-1/1">
              <SatisFirsatlari {...props} />
            </div>
          )}
          {user.yetkiler.list_randevu && (
            <div className="m-4 sm:w-1/2">
              <Randevular {...props} />
            </div>
          )}
          {user.yetkiler.list_gorev && (
            <div className="m-4 sm:w-1/2">
              <Gorevler {...props} />
            </div>
          )}
        </div>
      </TabPanel>

      <TabPanel value={activeTab} index={1} className="border">
        <div className="flex">
          <EkipList {...props} />
        </div>
      </TabPanel>
    </div>
  );
};
export default SatisDashboard;
