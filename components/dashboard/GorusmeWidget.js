import React from "react";
import { Paper, Typography, Divider, Link, Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  title: {
    fontSize: "1.2rem",
  },
  text: {
    fontSize: "7.2rem",
    verticalAlign: "baseline",
  },
  onayBekleyen: {
    fontSize: "3rem",
    verticalAlign: "baseline",
  },
}));

const GorusmeWidget = (props) => {
  const classes = useStyles();
  return (
    <Paper className="w-full p-8 rounded-8 shadow-none  border-t-4 border-green-500">
      <Typography className={classes.title}>Görüşmeler</Typography>
      <Divider />
      <div className="text-center ">
        <Typography className={classes.text}>12</Typography>
		<div className='flex justify-end'>
			<Button variant="outlined" size="small" color="secondary">
			Detay
			</Button>
		</div>
      </div>
    </Paper>
  );
};

export default GorusmeWidget;
