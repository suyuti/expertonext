import React from "react";
import {
  Card,
  CardHeader,
  Divider,
  CardContent,
  Table,
  TableRow,
  TableCell,
  Chip,
  IconButton,
  Typography,
  Icon,
  TableHead,
  LinearProgress,
  makeStyles,
} from "@material-ui/core";
import DoneIcon from "@material-ui/icons/Done";
import AddIcon from "@material-ui/icons/Add";
import ErrorOutlineIcon from "@material-ui/icons/ErrorOutline";
import MaterialTable from "material-table";
import useSWR from "swr";
import baseUrl from "../../utils/baseUrl";
import { useRouter } from "next/router";
import axios from "axios";
import moment from "moment";
import EventIcon from "@material-ui/icons/Event";
import PlaylistAddIcon from "@material-ui/icons/PlaylistAdd";

const useStyles = makeStyles(theme => ({
  kritik: {
    color: '#E76F51'
  },
  yuksek: {
    color: '#F4A261'
  },
  orta: {
    color: '#E9C46A'
  },
  dusuk: {
    color: '#2A9D8F'
  }
}))

const Gorevler = (props) => {
  const router = useRouter();
  const { token, user } = props;
  const classes = useStyles()

  moment.locale("tr");

  const { data: gorevler, error } = useSWR(`${baseUrl}/api/gorevler`, (url) =>
    axios
      .get(url, {
        //params: { filter: { durum: "ACIK" } },
        headers: { authorization: token },
      })
      .then((resp) => resp.data.data)
  );

  const onem = (onem) => {
    switch (onem) {
      case "KRITIK":
        return <Typography className={classes.kritik} >Kritik</Typography>;
      case "YUKSEK":
        return <Typography className={classes.yuksek} >Yüksek</Typography>;
      case "ORTA":
        return <Typography className={classes.orta} >Orta</Typography>;
      case "DUSUK":
        return <Typography className={classes.dusuk} >Düşük</Typography>;
    }
  };

  return (
    <Card className="w-full border-t-4 border-teal-600">
      <CardHeader
        title={<Typography variant="h6">Görevler</Typography>}
        avatar={<PlaylistAddIcon fontSize="large" />}
        action={
          <IconButton
            disabled={!user.yetkiler.create_gorev}
            onClick={(e) => {
              e.preventDefault();
              router.push("/satis/gorevler/[id]", `/satis/gorevler/new`);
            }}
          >
            <AddIcon fontSize="large" />
          </IconButton>
        }
      />
      <Divider />
      <CardContent>
        <div className="flex p-8">
          <Table size="small">
            <TableHead>
              <TableRow>
                <TableCell>Müşteri</TableCell>
                <TableCell>Proje</TableCell>
                <TableCell>Görev</TableCell>
                <TableCell>Personel</TableCell>
                <TableCell>Önem</TableCell>
                <TableCell>Süre</TableCell>
              </TableRow>
            </TableHead>
            {gorevler &&
              gorevler.map((g, i) => (
                <TableRow
                  hover
                  key={g._id}
                  onClick={(e) => {
                    e.preventDefault();
                    if (user.yetkiler.view_gorev) {
                      router.push(
                        "/satis/gorevler/[id]",
                        `/satis/gorevler/${g._id}`
                      );
                    }
                  }}
                >
                  <TableCell>{g.musteri && g.musteri.marka}</TableCell>
                  <TableCell>-</TableCell>
                  <TableCell>{g.baslik}</TableCell>
                  <TableCell>{`${g.personel.adi} ${g.personel.soyadi}`}</TableCell>
                  <TableCell>{onem(g.priority)}</TableCell>
                  <TableCell>
                    {
                      moment(g.termin).fromNow()
                      //.format('DD.MM.YYYY')
                    }
                  </TableCell>
                </TableRow>
              ))}
          </Table>
        </div>
      </CardContent>
    </Card>
  );
};

export default Gorevler;
