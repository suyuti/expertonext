import React from "react";
import {
  Card,
  CardHeader,
  Divider,
  CardContent,
  IconButton,
  Table,
  TableRow,
  TableCell,
  Chip,
  Typography,
  LinearProgress,
  TableHead,
} from "@material-ui/core";
import DoneIcon from "@material-ui/icons/Done";
import ErrorOutlineIcon from "@material-ui/icons/ErrorOutline";
import AddShoppingCartIcon from "@material-ui/icons/AddShoppingCart";
import AddIcon from "@material-ui/icons/Add";
import MaterialTable from "material-table";
import useSWR from "swr";
import baseUrl from "../../utils/baseUrl";
import { useRouter } from "next/router";
import axios from "axios";
import moment from "moment";

const SatisFirsatlari = (props) => {
  const router = useRouter();
  const { token, user } = props;

  const { data: satisFirsatlari, error } = useSWR(`${baseUrl}/api/sfs`, (url) =>
    axios
      .get(url, {
        headers: {
          authorization: token,
        },
      })
      .then((resp) => resp.data.data)
  );

  const renderDurum = (data) => {
    if (data.durum === "YENI") {
      return (
        <div className="flex-col">
          <Typography style={{ fontSize: 12, marginBottom: 8 }}>
            Randevu oluşturulacak
          </Typography>
          <LinearProgress
            variant="determinate"
            value={20}
            style={{ width: "100%", height: 10, borderRadius: 4 }}
          />
        </div>
      );
    } else if (data.durum === "RANDEVU") {
      return (
        <div className="flex-col">
          <Typography style={{ fontSize: 12, marginBottom: 8 }}>
            Teklif hazırlanacak
          </Typography>
          <LinearProgress
            variant="determinate"
            value={40}
            style={{ width: "100%", height: 10, borderRadius: 4 }}
          />
        </div>
      );
    } else if (data.durum === "TEKLIF") {
      return (
        <div className="flex-col">
          <Typography style={{ fontSize: 12, marginBottom: 8 }}>
            Teklif sonucu bekleniyor
          </Typography>
          <LinearProgress
            variant="determinate"
            value={80}
            style={{ width: "100%", height: 10, borderRadius: 4 }}
          />
        </div>
      );
    }
    return <Typography>-</Typography>;
  };

  return (
    <Card className="w-full border-t-4 border-red-200">
      <CardHeader
        title={<Typography variant="h6">Satış Fırsatları</Typography>}
        avatar={<AddShoppingCartIcon fontSize="large" />}
        action={
          <IconButton 
            disabled={!user.yetkiler.create_satisfirsati}
            onClick={(e) => {
            e.preventDefault();
              router.push(
                "/satis/sfs/[id]",
                "/satis/sfs/new"
              );
        }}>
            <AddIcon fontSize="large" />
          </IconButton>
        }
      />
      <Divider />
      <CardContent>
        <div className='flex'>
            <Table size='small'>
                <TableHead>
                    <TableRow>
                    <TableCell>Müşteri</TableCell>
                    <TableCell>Ürünler</TableCell>
                    <TableCell>Personel</TableCell>
                    <TableCell>İlk Fatura</TableCell>
                    <TableCell>Sözleşme Bedeli</TableCell>
                    <TableCell>Durum</TableCell>
                    <TableCell>Oluşturma Zamanı</TableCell>
                    <TableCell>Aksiyon</TableCell>
                    </TableRow>
                </TableHead>
                {satisFirsatlari && satisFirsatlari.map((s, i) => 
                    <TableRow hover key={s._id} onClick={(e) => {
                        e.preventDefault();
                        if (user.yetkiler.view_satisfirsati) {
                            router.push("/satis/sfs/[id]", `/satis/sfs/${s._id}`);
                        }
                        }}>
                        <TableCell>{s.musteri.marka}</TableCell>
                        <TableCell></TableCell>
                        <TableCell>{s.atananPersonel && `${s.atananPersonel.adi} ${s.atananPersonel.soyadi}`}</TableCell>
                        <TableCell></TableCell>
                        <TableCell></TableCell>
                        <TableCell>{renderDurum(s)}</TableCell>
                        <TableCell>{moment(s.createdAt).format('DD.MM.YYYY')}</TableCell>
                        <TableCell></TableCell>
                    </TableRow>
                )}
            </Table>
        </div>
      </CardContent>
    </Card>
  );
};

export default SatisFirsatlari;
