import React, { useState, Fragment } from "react";
import {
  Button,
  Typography,
  Box,
  TextField,
  formatMs,
  Card,
  CardHeader,
  CardContent,
  CardActions,
  LinearProgress,
  Divider,
  Slider,
  FormControlLabel,
  MenuItem,
  ListItemIcon,
  ListItemText,
} from "@material-ui/core";

import PlaylistAddIcon from "@material-ui/icons/PlaylistAdd";

import DoneIcon from "@material-ui/icons/Done";
import { DateTimePicker, KeyboardDateTimePicker } from "@material-ui/pickers";
import { useRouter } from "next/router";
import axios from "axios";
import baseUrl from "../../utils/baseUrl";
import { Autocomplete } from "@material-ui/lab";
import IOSSwitch from "../../components/IOSSwitch";
import PanoramaFishEyeIcon from "@material-ui/icons/PanoramaFishEye"; // departman icon
import PersonOutlineIcon from "@material-ui/icons/PersonOutline"; // user
import Snackbar from "../shared/SnackBar";

const GorevBilgiler = (props) => {
  const router = useRouter();
  const { gorev, personeller, departmanlar, musteriler, user, token } = props;
  const [editable, setEditable] = useState(
    true
    //user.yetkiler.edit_gorev && (gorev.createdBy == user._id || id !== "new")
  );
  const [form, setForm] = useState({ ...gorev });
  const [open, setOpen] = React.useState(false);
  const { id } = router.query;
  const [yeni, setYeni] = useState(id === "new");
  const [snackbarInfo, setSnackbarInfo] = useState({
    severity: "",
    message: "",
  });

  const [ilgiliKisiler, setIlgiliKisiler] = useState([
    ...personeller,
    ...departmanlar.map((d) => {
      return {
        ...d,
        adi: `${d.adi} / Departman`,
        _id: d.yoneticisi,
        departman: true,
      };
    }),
  ]);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  const handleChange = (e) => {
    const u = { ...form };
    u[e.target.name] =
      e.target.type === "checkbox" ? e.target.checked : e.target.value;
    setForm(u);
  };

  const handleTermin = (val) => {
    var _f = { ...form };
    _f.termin = val;
    setForm(_f);
  };

  const handleIlerleme = (e, val) => {
    var _f = { ...form };
    _f.ilerleme = val;
    setForm(_f);
  };

/*  const atamaYap = async () => {
    await axios
    .put(`${baseUrl}/api/gorevler/${gorev._id}`, form, {
      headers: { authorization: token },
    })
    .then((resp) => {
      if (resp.status === 200) {
        setSnackbarInfo({
          severity: "success",
          message: "Görev güncellendi",
        });
      } else {
        setSnackbarInfo({
          severity: "error",
          message: "Görev güncellenemedi",
        });
      }
    });
  setOpen(true);
}
*/
  const save = async () => {
    if (yeni) {
      await axios
        .post(`${baseUrl}/api/gorevler`, form, {
          headers: { authorization: token },
        })
        .then((resp) => {
          if (resp.status === 200) {
            setSnackbarInfo({
              severity: "success",
              message: "Görev oluşturuldu",
            });
          } else {
            setSnackbarInfo({
              severity: "error",
              message: "Görev oluşturulamadı",
            });
          }
          setOpen(true);
        });
    } else {
      await axios
        .put(`${baseUrl}/api/gorevler/${gorev._id}`, form, {
          headers: { authorization: token },
        })
        .then((resp) => {
          if (resp.status === 200) {
            setSnackbarInfo({
              severity: "success",
              message: "Görev güncellendi",
            });
          } else {
            setSnackbarInfo({
              severity: "error",
              message: "Görev güncellenemedi",
            });
          }
        });
      setOpen(true);
    }
  };

  const canSubmit = () => {
    if (form.icGorev) {
      return form.baslik && form.personel;
    } else {
      return form.musteri && form.baslik && form.personel;
    }
  };

  return (
    <Card className="w-full border-t-4 border-red-200">
      <CardHeader
        title={<Typography variant="h6">Görev</Typography>}
        avatar={<PlaylistAddIcon fontSize="large" />}
        action={<DoneIcon fontSize="large" />}
      />
      <Divider />
      <CardContent>
        <div className="flex-col ">
          <div className="">
            <FormControlLabel
              control={
                <IOSSwitch
                  checked={form.icGorev}
                  disabled={!editable}
                  onChange={(e) => {
                    let checked = e.target.checked;
                    var f = { ...form };
                    f.icGorev = checked;
                    setForm(f);
                  }}
                  name="checkedB"
                />
              }
              label="İç Görev"
            />
          </div>
          {!form.icGorev && (
            <Fragment>
              <div className="my-4">
                <Autocomplete
                  id="musteriler-combo"
                  options={musteriler}
                  getOptionLabel={(option) => option.marka}
                  fullWidth
                  autoHighlight={true}
                  autoSelect={true}
                  clearOnEscape={true}
                  groupBy={(o) => o.durum}
                  value={form.musteri}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      label="Müşteri"
                      autoFocus
                      variant="outlined"
                    />
                  )}
                  onChange={(e, v, r) => {
                    if (r === "select-option") {
                      setForm((prev) => ({ musteri: v._id }));
                    } else if (r === "clear") {
                      setForm((prev) => ({ musteri: null }));
                    }
                  }}
                />
              </div>

              <div className="my-4">
                <Autocomplete
                  id="projeler-combo"
                  disabled
                  options={[]}
                  //getOptionLabel={(option) => option.marka}
                  fullWidth
                  autoHighlight={true}
                  autoSelect={true}
                  clearOnEscape={true}
                  //groupBy={(o) => o.durum}
                  //value={form.musteri}
                  renderInput={(params) => (
                    <TextField {...params} label="Proje" variant="outlined" />
                  )}
                  //onChange={(e, v, r) => {
                  //  if (r === "select-option") {
                  //    setForm((prev) => ({ musteri: v }));
                  //  } else if (r === "clear") {
                  //    setForm((prev) => ({ musteri: null }));
                  //  }
                  //}}
                />
              </div>
            </Fragment>
          )}
          <div className="my-4">
            <TextField
              className="mb-24"
              label="Başlık"
              id="baslik"
              name="baslik"
              value={form.baslik}
              onChange={handleChange}
              variant="outlined"
              required
              fullWidth
              InputProps={{
                readOnly: !editable,
              }}
            />
          </div>
          <div className="my-4">
            <TextField
              className="mb-24"
              label="Açıklama"
              id="aciklama"
              name="aciklama"
              multiline
              rows={3}
              //value={urun.onOdemeTutari}
              value={form.aciklama}
              onChange={handleChange}
              //type="text"
              variant="outlined"
              fullWidth
              //InputProps={{
              //  readOnly: !editable,
              //}}
            />
          </div>
          <div className="my-4">
            <TextField
              className="mb-24"
              label="Önem"
              id="onem"
              name="priority"
              value={form.priority}
              onChange={handleChange}
              variant="outlined"
              fullWidth
              InputProps={{
                readOnly: !editable,
              }}
              select
            >
              <MenuItem key="KRITIK" value="KRITIK">
                Kritik
              </MenuItem>
              <MenuItem key="YUKSEK" value="YUKSEK">
                Yüksek
              </MenuItem>
              <MenuItem key="ORTA" value="ORTA">
                Orta
              </MenuItem>
              <MenuItem key="DUSUK" value="DUSUK">
                Düşük
              </MenuItem>
            </TextField>
          </div>
          <div className="flex my-4">
            <div className="mr-4 w-full">
              <TextField
                label="Personel"
                id="personel"
                name="personel"
                value={form.personel}
                onChange={handleChange}
                variant="outlined"
                fullWidth
                InputProps={{
                  readOnly: !editable,
                }}
                select
              >
                {ilgiliKisiler &&
                  ilgiliKisiler.map((p) => {
                    if (p.departman) {
                      return (
                        <MenuItem key={p._id} value={p._id}>
                          <ListItemIcon>
                            <PanoramaFishEyeIcon />
                          </ListItemIcon>
                          <ListItemText
                            //classes={{ primary: classes.primary }}
                            //inset
                            primary={`${p.adi}`}
                          />
                        </MenuItem>
                      );
                    } else {
                      return (
                        <MenuItem key={p._id} value={p._id}>
                          <ListItemIcon>
                            <PersonOutlineIcon />
                          </ListItemIcon>
                          <ListItemText
                            //classes={{ primary: classes.primary }}
                            //inset
                            primary={`${p.adi} ${p.soyadi}`}
                          />
                        </MenuItem>
                      );
                    }
                  })}
              </TextField>
            </div>
            <div className="w-full">
              <KeyboardDateTimePicker
                variant="inline"
                inputVariant="outlined"
                ampm={false}
                label="Termin Tarihi"
                value={form.randevuTarihi}
                onChange={handleTermin}
                onError={console.log}
                //disableFuture
                //disablePast
                format="dd/MM/yyyy HH:mm"
                name="randevuTarihi"
                fullWidth
                autoOk
                showTodayButton
              />
            </div>
          </div>

          {/*!yeni && (
            <>
              <div className="flex w-full">
                <div className="mr-8 w-full">
                  <TextField
                    label="Personeller"
                    variant="outlined"
                    select
                    fullWidth
                    onChange={(e) => {
                      debugger
                      var f = {...form}
                      f.personel = e.target.value
                      setForm(f)
                    }}
                  >
                    {personeller &&
                      personeller.map((p) => (
                        <MenuItem key={p._id} value={p._id}>
                          <ListItemIcon>
                            <PersonOutlineIcon />
                          </ListItemIcon>
                          <ListItemText primary={`${p.adi} ${p.soyadi}`} />
                        </MenuItem>
                      ))}
                  </TextField>
                </div>
                <Button 
                  variant="contained" 
                  color="primary"
                  onClick={atamaYap}
                  >
                  Atama yap
                </Button>
              </div>
            </>
                      )*/}

          {!yeni && (
            <Fragment>
              <div className="my-4">
                <TextField label="Sonuç" variant="outlined" fullWidth select>
                  <MenuItem key="Başarılı" value="Başarılı">
                    Başarılı
                  </MenuItem>
                  <MenuItem key="Başarısız" value="Başarsız">
                    Başarısız
                  </MenuItem>
                </TextField>
              </div>
              <div className="my-4">
                <TextField
                  label="Sonuç Notu"
                  variant="outlined"
                  fullWidth
                  multiline
                  rows={3}
                />
              </div>
            </Fragment>
          )}
        </div>
      </CardContent>
      <Divider />
      <CardActions>
        <div className="flex w-full justify-end">
          {(user.yetkiler.edit_urun || user.yetkiler.create_urun) && (
            <Button
              variant="contained"
              color="primary"
              onClick={save}
              disabled={!canSubmit()}
            >
              {yeni ? "Kaydet" : "Kaydet"}
            </Button>
          )}
          {open && (
            <Snackbar
              open={open}
              severity={snackbarInfo.severity}
              message={snackbarInfo.message}
              handleClose={handleClose}
            />
          )}
        </div>
      </CardActions>
    </Card>
  );
};

export default GorevBilgiler;
