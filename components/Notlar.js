import React, { useState } from 'react'
import {
    Card,
    CardHeader,
    CardContent,
    CardActions,
    Divider,
    Typography,
    TextField,
    Button,
    IconButton,
    Paper,
    makeStyles

} from '@material-ui/core'

import AddIcon from '@material-ui/icons/Add'
import CommentIcon from '@material-ui/icons/Comment';
import moment from 'moment'

const useStyles = makeStyles(theme => (
{
    not: {},
    notyazar: {},
    nottarih: {}
}
))


const NotItem = props => {
    const {not} = props
    return (
        <div className='flex-col m-8'>
            <Paper className='p-8'>
                <p>{not.not}</p>
                <div className='flex justify-between'>
                    <p>{`${not.yazar.adi} ${not.yazar.soyadi}`}</p>
                    <p>{moment(not.tarih).format('HH:mm DD.MM.YYYY')}</p>
                </div>
            </Paper>
        </div>
    )
}

const NotForm = props => {
    const {onSubmit, onClose} = props
    const [form, setForm] = useState({})
    
    const handleChange = (e) => {
        var f = {...form}
        f[e.target.name] = e.target.value
        setForm(f)
    }

    return (
        <div className='flex-col w-full'>
            <TextField 
                variant='outlined'
                autoFocus
                multiline
                fullWidth
                rows={3}
                name='not'
                value = {form.not}
                onChange={handleChange}
            />
            <div className='flex my-4'>
                <Button variant="contained" size='small' color='primary' onClick={() => onSubmit(form)}>Ekle</Button>
                <Button variant="contained" size='small' color='secondary' onClick={onClose}>Kapat</Button>
            </div>
        </div>
    )
}

const Notlar = props => {
    const {notlar, user, onAddNew} = props
    const [showForm, setShowForm] = useState(false)

    const onSubmit = (form) => {
        onAddNew(form)
    }

    return (
        <Card className="w-full border-t-4 border-red-500">
        <CardHeader
          title={
            <Typography variant="h6">
              {`Notlar`}
            </Typography>
          }
          //subheader="Yapılan görüşmeler"
          avatar={<CommentIcon fontSize='large' />}
          action={
              <IconButton
               // disabled={!user.yetkiler.create_not}
                onClick={(e) => {
                  e.preventDefault();
                  setShowForm(true)
                }}
              >
                <AddIcon fontSize='large' />
              </IconButton>
          }
        />
        <Divider />
        <CardContent>
            <div className='flex-col'>
                {showForm && <NotForm onSubmit={onSubmit} onClose={() => setShowForm(false)}/>}
                {notlar && notlar.map(n => (
                    <NotItem not={n} />
                ))}
            </div>
        </CardContent>
        </Card>
      )
}

export default Notlar