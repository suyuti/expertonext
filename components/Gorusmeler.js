import React, { useEffect, useState } from "react";
import {
  Card,
  CardHeader,
  Divider,
  CardContent,
  Typography,
  TextField,
  IconButton,
  Chip,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Tooltip,
} from "@material-ui/core";
import PhoneIcon from "@material-ui/icons/Phone";
import EventIcon from "@material-ui/icons/Event";
import MailIcon from "@material-ui/icons/Mail";
import AddIcon from "@material-ui/icons/Add";
import useSWR from "swr";
import baseUrl from "../utils/baseUrl";
import axios from "axios";
import moment from "moment";

const Gorusmeler = (props) => {
  const prepFilter = () => {
    if (relation === "kisi") {
        return { kisi: value._id };
      }
      else if (relation === "musteri") {
        return { musteri: value }; // id gelmiyor
      }
      };
  const { relation, value, token, user } = props;
  const [filter, setFilter] = useState(prepFilter());

  //alert(JSON.stringify(filter))
  const { data: gorusmeler, error: errorGorusmeler } = useSWR(
    `${baseUrl}/api/gorusmeler`,
    (url) =>
      axios
        .get(url, {
          params: { filter: filter },
          headers: { authorization: token },
        })
        .then((res) => res.data.data)
  );

  const kanalIcon = (kanal) => {
    if (kanal === "telefon") {
      return (
        <Tooltip title="Telefon">
          <PhoneIcon />
        </Tooltip>
      );
    } else if (kanal === "mail") {
      return (
        <Tooltip title="Mail">
          <MailIcon />
        </Tooltip>
      );
    } else if (kanal === "toplanti") {
      return (
        <Tooltip title="Toplanti">
          <EventIcon />
        </Tooltip>
      );
    }
    return <></>;
  };


  return (
    <Card className="w-full border-t-4 border-red-500">
      <CardHeader
        title={
          <Typography className="text-xl" variant="h6">
            {`${relation} Görüşmeleri`}
          </Typography>
        }
        subheader="Yapılan görüşmeler"
        avatar={<PhoneIcon />}
        action={
          user.yetkiler.create_gorusme && (
            <IconButton
              onClick={(e) => {
                e.preventDefault();
                if (user.yetkiler.create_gorusme) {
                  router.push(
                    "/satis/gorusmeler/[id]",
                    `/satis/gorusmeler/new?musteri=${router.query.id}`
                  );
                }
              }}
            >
              <AddIcon />
            </IconButton>
          )
        }
      />
      <Divider />
      <CardContent>
        <Table size="small">
          <TableHead>
            <TableRow>
              <TableCell>Konu</TableCell>
              <TableCell>Kisi</TableCell>
              <TableCell>Kanal</TableCell>
              <TableCell>Zaman</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {gorusmeler &&
              gorusmeler.map((g, i) => (
                <TableRow key={g._id}>
                  <TableCell>{g.konu}</TableCell>
                  <TableCell>{g.kisi.adi}</TableCell>
                  <TableCell>{kanalIcon(g.kanal)}</TableCell>
                  <TableCell>
                    {moment(g.gorusmeZamani).format("Do MMMM YYYY, hh:mm")}
                  </TableCell>
                </TableRow>
              ))}
          </TableBody>
        </Table>
      </CardContent>
    </Card>
  );
};

export default Gorusmeler;
