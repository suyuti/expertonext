import React, { useState } from "react";
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Fab,
  TextField,
  MenuItem,
  Chip,
} from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import MaterialTable from "material-table";
import { Autocomplete } from "@material-ui/lab";
import { DatePicker } from "@material-ui/pickers";
import axios from "axios";
import baseUrl from "../../utils/baseUrl";
import moment from 'moment'

const ToplantiKararList = (props) => {
  const { toplanti, personeller, departmanlar, token } = props;

  const [ilgiliKisiler, setIlgiliKisiler] = useState([
    ...personeller,
    ...departmanlar.map(d => { return {...d, adi: `${d.adi} / Departman`}})
    //...departmanlar
  ])
//  debugger

  return (
    <MaterialTable
      data={toplanti.kararlar}
      columns={[
        {
          title: "Karar",
          field: "konu",
          editComponent: (props) => (
            <TextField
              multiline
              label="Karar"
              rows={3}
              fullWidth
              variant="outlined"
              autoFocus
              value={props.value}
              onChange={(e) => props.onChange(e.target.value)}
            />
          ),
        },
        {
          title: "İlgili Kişi",
          field: "ilgiliPersonel",
          render: rowData => <Chip label={`${rowData.ilgiliPersonel.adi} ${rowData.ilgiliPersonel.soyadi}`} variant='outlined'/>,
          editComponent: (props) => (
            <Autocomplete
              id="musteriler-combo"
              options={ilgiliKisiler}
              getOptionLabel={(option) => option.adi || ""}
              fullWidth
              autoHighlight={true}
              autoSelect={true}
              clearOnEscape={true}
              value={props.value}
              renderInput={(params) => (
                <TextField {...params} label="Personel" variant="outlined" />
              )}
              value={props.value}
              onChange={(e, v, r) => {
                if (r === "select-option") {
                  props.onChange(v._id);
                  //onMusteriChanged(v);
                  //setForm((prev) => ({ musteri: v._id }));
                } else if (r === "clear") {
                  //setForm((prev) => ({ musteri: null }));
                }
              }}
            />
          ),
        },
        {
          title: "Son Tarih",
          field: "sonTarih",
          render: rowData => moment(rowData.terminTarihi).format('DD.MM.YYYY'),
          editComponent: (props) => (
            <DatePicker
              //autoFocus
              //views={["year", "month"]}
              label="Son Tarih"
              //value= {props.value}//{selectedDate}
              //onChange={handleDateChange}
              autoOk
              animateYearScrolling
              fullWidth
              minDate={new Date("2020-03-01")}
              inputVariant="outlined"
              onChange={(e) => {
                props.onChange(e);
              }}
              //InputAdornmentProps={{ position: "end" }}
              //variant="inline"
              value={props.value}
              onChange={(e) => props.onChange(e)}
            />
          ),
        },
        {
          title: "Önem",
          field: "oncelik",
          editComponent: (props) => (
            <TextField
              variant="outlined"
              fullWidth
              select
              value={props.value}
              onChange={(e) => props.onChange(e.target.value)}
            >
              <MenuItem key="KRITIK" value="KRITIK">
                Kritik
              </MenuItem>
              <MenuItem key="YUKSEK" value="YUKSEK">
                Yüksek
              </MenuItem>
              <MenuItem key="ORTA" value="ORTA">
                Orta
              </MenuItem>
              <MenuItem key="DUSUK" value="DUSUK">
                Düşük
              </MenuItem>
            </TextField>
          ),
        },
        {
          title: "Durum",
          field: "durum",
          editComponent: (props) => (
            <TextField
              variant="outlined"
              fullWidth
              select
              value={props.value}
              onChange={(e) => props.onChange(e.target.value)}
            >
              <MenuItem key="ACIK" value="ACIK">
                Açık
              </MenuItem>
              <MenuItem key="KAPALI-BASARILI" value="KAPALI-BASARILI">
                Kapalı - Başarılı
              </MenuItem>
              <MenuItem key="KAPALI-BASARISIZ" value="KAPALI-BASARISIZ">
                Kapalı - Başarısız
              </MenuItem>
              <MenuItem key="IPTAL" value="IPTAL">
                İptal
              </MenuItem>
            </TextField>
          ),
        },
      ]}
      editable={{
        onRowAdd: (newData) => 
          axios.post(
            `${baseUrl}/api/toplantilar/${toplanti._id}/karar`,
            { ...newData, musteri: toplanti.musteri },
            { headers: { authorization: token } }
          )//.then(reps => resp.data.data)
        ,
        onRowUpdate: (newData, oldData) => {},
        onRowDelete: (oldData) => {},
      }}
    />
  );
};

export default ToplantiKararList;
