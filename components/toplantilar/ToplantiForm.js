import React, { useState } from "react";
import { TextField, Checkbox, Divider, Button } from "@material-ui/core";
import { Autocomplete } from "@material-ui/lab";
import { DatePicker } from "@material-ui/pickers";
import CheckBoxOutlineBlankIcon from "@material-ui/icons/CheckBoxOutlineBlank";
import CheckBoxIcon from "@material-ui/icons/CheckBox";
import useSWR from "swr";
import baseUrl from "../../utils/baseUrl";
import axios from "axios";
import { useRouter } from "next/router";

const icon = <CheckBoxOutlineBlankIcon fontSize="small" />;
const checkedIcon = <CheckBoxIcon fontSize="small" />;

const ToplantiForm = (props) => {
  const router = useRouter();

  const { musteriler, personeller, toplanti, user, token } = props;
  const [form, setForm] = useState(toplanti);
  const [firmaKisiler, setFirmaKisiler] = useState([]);
  const [editable, setEditable] = useState(user.yetkiler.edit_toplanti);
  const [yeni, setYeni] = useState(router.query.id === "new");

  const onMusteriChanged = (m) => {
    axios
      .get(`${baseUrl}/api/kisiler`, {
        params: { filter: { musteri: m._id } },
        headers: { authorization: token },
      })
      .then((resp) => {
        1;
        setFirmaKisiler(resp.data.data);
      });
    setForm((prev) => ({ ...prev, musteri: m._id }));
  };

  const submit = () => {
    axios
      .post(`${baseUrl}/api/toplantilar`, form, {
        headers: { authorization: token },
      })
      .then((resp) => alert("ok"));
  };

//  debugger

  return (
    <div className="flex-col m-4">
      <div className="m-4 w-full">
        <Autocomplete
          id="musteriler-combo"
          options={musteriler}
          getOptionLabel={(option) => option.marka}
          fullWidth
          autoHighlight={true}
          autoSelect={true}
          clearOnEscape={true}
          groupBy={(o) => o.durum}
          value={form.musteri}
          disabled={!editable || !yeni}
          renderInput={(params) => (
            <TextField
              {...params}
              label="Müşteri"
              autoFocus
              variant="outlined"
            />
          )}
          onChange={(e, v, r) => {
            if (r === "select-option") {
              onMusteriChanged(v);
              //setForm((prev) => ({ musteri: v._id }));
            } else if (r === "clear") {
              //setForm((prev) => ({ musteri: null }));
            }
          }}
        />
      </div>
      <div className="flex m-4 w-full">
        <div className="mr-4 w-full">
          <Autocomplete
            fullWidth
            multiple
            id="firma-katilimcilar"
            options={firmaKisiler}
            value={form.firmaKatilimcilar}
            disableCloseOnSelect
            getOptionLabel={(option) =>
              `${option.adi} ${option.soyadi} / ${option.unvani}`
            }
            renderOption={(option, { selected }) => (
              <React.Fragment>
                <Checkbox
                  icon={icon}
                  checkedIcon={checkedIcon}
                  style={{ marginRight: 8 }}
                  checked={selected}
                />
                {`${option.adi} ${option.soyadi} / ${option.unvani}`}
              </React.Fragment>
            )}
            renderInput={(params) => (
              <TextField
                {...params}
                variant="outlined"
                label="Firma Katılımcılar"
                placeholder="Firma Katılımcılar"
              />
            )}
            onChange={(e, v, r) => {
              var k = v.map((i) => i._id);
              setForm((prev) => ({ ...prev, firmaKatilimcilar: k }));
            }}
          />
        </div>
        <div className="ml-4 w-full">
          <Autocomplete
            fullWidth
            multiple
            id="experto-katilimcilar"
            options={personeller}
            disableCloseOnSelect
            value={form.expertoKatilimcilar}
            getOptionLabel={(option) => option.adi || "?"}
            renderOption={(option, { selected }) => (
              <React.Fragment>
                <Checkbox
                  icon={icon}
                  checkedIcon={checkedIcon}
                  style={{ marginRight: 8 }}
                  checked={selected}
                />
                {option.adi || "?"}
              </React.Fragment>
            )}
            renderInput={(params) => (
              <TextField
                {...params}
                variant="outlined"
                label="Experto Katılımcılar"
                placeholder="Experto Katılımcılar"
              />
            )}
            onChange={(e, v, r) => {
              var k = v.map((i) => i._id);
              setForm((prev) => ({ ...prev, expertoKatilimcilar: k }));
            }}
          />
        </div>
      </div>
      <div className="flex m-4 w-full">
        <div className="mr-4 w-full">
          <TextField
            label="Konum"
            fullWidth
            variant="outlined"
            value={form.konum}
            InputProps={{
              readOnly: !editable,
            }}
            onChange={(e) => {
              e.preventDefault();
              var val = e.target.value;
              setForm((prev) => ({ ...prev, konum: val }));
            }}
          />
        </div>
        <div className="ml-4 w-full">
          <DatePicker
            autoOk
            format="hh:mm dd/MMMM/yyyy"
            //views={["year", "month", ]}
            label="Toplantı Tarihi"
            value={form.zaman}
            onChange={(e) => setForm((prev) => ({ ...prev, zaman: e }))}
            animateYearScrolling
            fullWidth
            inputVariant="outlined"
            InputAdornmentProps={{ position: "end" }}
            variant="inline"
          />
        </div>
      </div>
      <div className="flex w-full justify-end m-8">
        <Button variant="contained" color="primary" onClick={submit}>
          {yeni ? "Kaydet" : "Guncelle"}
        </Button>
      </div>
      <Divider />
    </div>
  );
};

export default ToplantiForm;
