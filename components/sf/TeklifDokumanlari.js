import React from "react";
import DescriptionIcon from "@material-ui/icons/Description";
import GetAppIcon from "@material-ui/icons/GetApp";
import {
  Card,
  CardContent,
  CardHeader,
  Divider,
  Typography,
  IconButton,
  ButtonBase,
} from "@material-ui/core";
import axios from 'axios'
import baseUrl from "../../utils/baseUrl";
import { saveAs } from 'file-saver';
import moment from 'moment'

const TeklifDokumanlari = (props) => {
  const { satisFirsati } = props;

  const handleDownloadDoc = (teklif) => {
    axios.get(`${baseUrl}/api/sfs/${satisFirsati._id}/docs/${teklif._id}`, {
      //params:{docId: id},
      responseType: 'blob',
      timeout: 30000
    })
      .then(resp => {
        saveAs(resp.data, `${satisFirsati.musteri.marka}_${teklif.urunAdi}_${moment(teklif.createdAt).format('DD.MM.YYYY')}.docx`)
      })
  };

  const handleDownloadAllDocs = () => {};

  return (
    <Card className="w-full border-t-4 border-red-200">
      <CardHeader
        title={<Typography variant="h6">Teklif Dokümanları</Typography>}
        subheader={`${satisFirsati.musteri.marka} için hazırlanmış teklif dokümanları`}
        avatar={<DescriptionIcon fontSize="large" />}
        action={
          <IconButton onClick={handleDownloadAllDocs}>
            <GetAppIcon fontSize="large" />
          </IconButton>
        }
      />
      <Divider />
      <CardContent>
        <div className="flex">
          {satisFirsati.teklif.teklifUrunleri.map((t, i) => (
            <div className="p-8" key={i}>
              <ButtonBase
                className="sm:w-1/1 flex-col justify-center border rounded shadow p-8"
                onClick={() => handleDownloadDoc(t)}
              >
                <div className='mt-8'>
                  <DescriptionIcon fontSize="large" />
                </div>
                <div className='mb-8 mx-8'>
                <Typography variant="caption" className="text-center">
                  {t.urunAdi}
                </Typography>
                </div>
              </ButtonBase>
            </div>
          ))}
        </div>
      </CardContent>
    </Card>
  );
};

export default TeklifDokumanlari;
