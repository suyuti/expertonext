import React, { useState } from "react";
import {
  Card,
  CardHeader,
  Divider,
  CardActions,
  CardContent,
  TextField,
  Button,
  MenuItem,
  Table,
  TableHead,
  TableRow,
  TableCell,
  Typography,
  Dialog,
  DialogActions,
  DialogTitle,
  TableBody,
  IconButton,
  Collapse,
  makeStyles,
  TableContainer,
  Paper,
  Tooltip,
  DialogContent,
  Box,
} from "@material-ui/core";
import { useRouter } from "next/router";
import axios from "axios";
import baseUrl from "../../utils/baseUrl";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import DescriptionIcon from "@material-ui/icons/Description";
import { saveAs } from "file-saver";
import moment from "moment";
import UrunTeklifEditComponent from "./UrunTeklif2";

const SonucAciklamaDialog = (props) => {
  const { show } = props;

  return (
    <Dialog open={show}>
      <DialogTitle title="Sonuc Aciklama" />
    </Dialog>
  );
};

const SilmeOnayDlg = (props) => {
  const { show, hide, secim, teklif, removeAction } = props;
  return (
    <Dialog open={show}>
      <DialogTitle title="Silme Onay" />
      <DialogContent>
        <div className="flex-col">
          <Typography>Teklif silinecek.</Typography>
          <Typography>{teklif.urunAdi}</Typography>

          <Typography>Emin misiniz?</Typography>
        </div>
      </DialogContent>
      <DialogActions>
        <div className="flex justify-between m-8 w-full">
          <Button
            variant="contained"
            color="propmary"
            onClick={() => {
              secim(false);
              hide();
            }}
          >
            Hayır
          </Button>
          <Button
            variant="contained"
            color="propmary"
            onClick={() => {
              secim(true);
              removeAction(teklif._id);
              hide();
            }}
          >
            Evet
          </Button>
        </div>
      </DialogActions>
    </Dialog>
  );
};

const useStyles = makeStyles((theme) => ({
  urun: {
    fontSize: 16,
  },
  field: {
    fontSize: 16,
    textAlign: "center",
    backgroundColor: theme.palette.info.dark,
    color: theme.palette.common.white,
  },
  fiyat: {
    fontSize: 16,
    textAlign: "right",
    fontWeight: "bold",
  },
}));

const SatisFirsatiSonuc = (props) => {
  const classes = useStyles();
  const router = useRouter();
  const { user, token, satisFirsati } = props;
  const [sonucBasarisiz, setSonucBasarisiz] = useState(false);
  const [form, setForm] = useState({ satisFirsati: satisFirsati });

  const [silmeOnayGoster, setSilmeOnayGoster] = useState(false);
  const [silmeOnaySecim, setSilmeOnaySecim] = useState(false);
  const [seciliTeklif, setSeciliTeklif] = useState(null);

  const [rowCollapseState, setRowCollapseState] = useState({});

  const handleChange = (e) => {
    var _sf = { ...form };
    _sf[e.target.name] = e.target.value;
    setForm(_sf);
  };

  const handleSubmit = (e) => {
    axios.put(
      `${baseUrl}/api/sfs/${satisFirsati._id}`,
      {
        sonuc: form.basariDurumu,
        sonucNotu: form.sonucNotu,
      },
      {
        headers: { authorization: token },
      }
    );
  };

  const handleBasarili = () => {};

  const handleBasarisiz = () => {
    setSonucBasarisiz(true);
  };

  const ilkFaturaTutari = (teklif) => {
    if (!teklif.onOdemeTutari) {
      return teklif.sabitOdemeTutari;
    } else {
      return teklif.onOdemeTutari;
    }
  };

  const removeTeklifAction = (teklif) => {
    axios.delete(`${baseUrl}/api/sfs/${satisFirsati._id}/teklif/${teklif}`)
    .then(resp => {
      if (resp.status === 200) {
        var f = { ...form };
        f.satisFirsati.teklif.teklifUrunleri = f.satisFirsati.teklif.teklifUrunleri.filter(
          (u) => u._id != teklif
        );
        setForm(f);
      }
    })
  };

  const removeTeklifUrun = (teklif) => {
    setSeciliTeklif(teklif);
    setSilmeOnayGoster(true);
  };

  const updateTeklifAction = (teklif) => {
    axios.put(`${baseUrl}/api/sfs/${satisFirsati._id}/teklif/${teklif.oncekiTeklif._id}`, teklif)
    .then(resp => {
      if (resp.status === 200) {
      }
    })
  }

  const editTeklifUrun = (teklif) => {
    if (!rowCollapseState[teklif._id]) {
      var r = { ...rowCollapseState };
      r[teklif._id] = true;
      setRowCollapseState(r);
    } else {
      setRowCollapseState((prev) => (prev[teklif._id] = !prev[teklif._id]));
    }
  };

  const downloadTeklifDokuman = (teklif) => {
    axios
      .get(`${baseUrl}/api/sfs/${satisFirsati._id}/docs/${teklif._id}`, {
        //params:{docId: id},
        responseType: "blob",
        timeout: 30000,
      })
      .then((resp) => {
        saveAs(
          resp.data,
          `${satisFirsati.musteri.marka}_${teklif.urunAdi}_${moment(
            teklif.createdAt
          ).format("DD.MM.YYYY")}.docx`
        );
      });
  };

  const canSubmit = () => {
    return form.basariDurumu === 'BASARILI' || (form.basariDurumu === 'BASARISIZ' && form.sonucNotu)
  }

  return (
    <React.Fragment>
      <Card className="w-full border-t-4 border-red-200">
        <CardHeader
          title="Satış Fırsatı Sonuçlandırma"
          subheader={`${satisFirsati.musteri.marka} için satış fırsatı sonuçlandırma`}
        />
        <Divider />
        <CardContent>
          <div className="flex-col">
            <div className="flex w-full">
              <TableContainer component={Paper}>
                <Table size="medium">
                  <TableHead>
                    <TableRow>
                      <TableCell className={classes.field}>Urun</TableCell>
                      <TableCell className={classes.field}>
                        İlk Fatura Tutarı
                      </TableCell>
                      <TableCell className={classes.field}>
                        Toplam Sözleşme Tutarı
                      </TableCell>
                      <TableCell className={classes.field}>Indirim</TableCell>
                      <TableCell className={classes.field}>Action</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {form.satisFirsati.teklif.teklifUrunleri.map((u) => {
                      return (
                        <>
                          <TableRow>
                            <TableCell>
                              <Typography className={classes.urun}>
                                {u.urunAdi}
                              </Typography>
                            </TableCell>
                            <TableCell>
                              <Typography className={classes.fiyat}>
                                {ilkFaturaTutari(u)} TL
                              </Typography>
                            </TableCell>
                            <TableCell>
                              <Typography className={classes.fiyat}>
                                {u.toplam} TL
                              </Typography>
                            </TableCell>
                            <TableCell>
                              <Typography className={classes.fiyat}>
                                {u.indirim} TL
                              </Typography>
                            </TableCell>
                            <TableCell>
                              <div className="flex justify-center">
                                <Tooltip title="Teklif dokumanı indir">
                                  <IconButton
                                    onClick={(e) => {
                                      downloadTeklifDokuman(u);
                                    }}
                                  >
                                    <DescriptionIcon
                                      style={{ color: "blue" }}
                                    />
                                  </IconButton>
                                </Tooltip>
                                <Tooltip title="Teklifi değiştir">
                                  <IconButton
                                    onClick={(e) => editTeklifUrun(u)}
                                  >
                                    <EditIcon />
                                  </IconButton>
                                </Tooltip>
                                <Tooltip title="Teklifi sil">
                                  <IconButton
                                    onClick={(e) => removeTeklifUrun(u)}
                                  >
                                    <DeleteIcon style={{ color: "red" }} />
                                  </IconButton>
                                </Tooltip>
                              </div>
                            </TableCell>
                          </TableRow>
                          <TableRow>
                            <TableCell
                              style={{ paddingBottom: 0, paddingTop: 0 }}
                              colSpan={3}
                            >
                              <Collapse
                                in={rowCollapseState[u._id]}
                                timeout="auto"
                                unmountOnExit
                              >
                                <Box margin={1}>
                                  <Typography className='text-center' variant='h5'>Teklif Değiştirme</Typography>
                                  <UrunTeklifEditComponent
                                    urun={u}
                                    onEdit={updateTeklifAction}
                                    {...props}
                                  />
                                </Box>
                              </Collapse>
                            </TableCell>
                          </TableRow>
                        </>
                      );
                    })}
                  </TableBody>
                </Table>
              </TableContainer>
            </div>
            <Divider />
            <div className="my-28">
              <TextField
                label="Başarı Durumu"
                variant="outlined"
                fullWidth
                select
                onChange={handleChange}
                name="basariDurumu"
                value={form.basariDurumu || ""}
              >
                <MenuItem key="BASARILI" value="BASARILI">
                  <Typography className="text-green">Basarili</Typography>
                </MenuItem>
                <MenuItem key="BASARISIZ" value="BASARISIZ">
                  <Typography className="text-red">Basarisiz</Typography>
                </MenuItem>
              </TextField>
            </div>
            {form.basariDurumu === "BASARISIZ" && (
              <div className="m-4">
                <TextField
                  label="Başarısızlık Nedeni"
                  variant="outlined"
                  multiline
                  rows={3}
                  fullWidth
                  required
                  name="sonucNotu"
                  value={form.sonucNotu}
                  onChange={handleChange}
                />
              </div>
            )}
          </div>
        </CardContent>
        <Divider />
        <CardActions>
          <div className="flex w-full justify-end p-8 pr-16">
            <Button variant="contained" color="primary" onClick={handleSubmit} disabled={!canSubmit()}>
              Sonuçlandir
            </Button>
          </div>
        </CardActions>
      </Card>
      {sonucBasarisiz && <SonucAciklamaDialog show={sonucBasarisiz} />}
      {silmeOnayGoster && (
        <SilmeOnayDlg
          show={silmeOnayGoster}
          hide={() => setSilmeOnayGoster(false)}
          secim={setSilmeOnaySecim}
          removeAction={removeTeklifAction}
          teklif={seciliTeklif}
        />
      )}
    </React.Fragment>
  );
};

export default SatisFirsatiSonuc;
