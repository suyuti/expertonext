import React from "react";
import CommentIcon from "@material-ui/icons/Comment";
import {
  Card,
  CardHeader,
  CardContent,
  Typography,
  Divider,
  Paper,
} from "@material-ui/core";

const SatisFirsatiNotlar = (props) => {
  const { satisFirsati } = props;

  return (
    <Card className="w-full border-t-4 border-red-200">
      <CardHeader
        title={<Typography variant="h6">Notlar</Typography>}
        avatar={<CommentIcon fontSize="large" />}
      />
      <Divider />
      <CardContent>
        <Paper className='p-8 my-4'>
          <Typography>{satisFirsati.not}</Typography>
        </Paper>
      </CardContent>
    </Card>
  );
};

export default SatisFirsatiNotlar;
