import React, { useState, useEffect } from "react";
import axios from "axios";
import useSWR from "swr";
import baseUrl from "../../utils/baseUrl";
import { Button, TextField, MenuItem, Divider, InputAdornment } from "@material-ui/core";
import SatisFirsatiDipToplam from "../satisFirsati/SatisFirsatiDipToplam";
import useForm from "../../utils/hooks/useForm";

const yeni = true;

const UrunTeklifEditComponent = (props) => {
  const { token, user, urun, onEdit } = props;
  const { data: urunData, error: urunDataError } = useSWR(
    `${baseUrl}/api/urun/${urun.urun}`,
    (url) =>
      axios
        .get(url, { headers: { authorization: token } })
        .then((res) => res.data.data)
  );
  const [toplamlar, setToplamlar] = useState({})
  const [degisiklikVar, setDegisiklikVar] = useState(false)

  const [form, setForm] = useState({
    onOdemeTutari: urun.onOdemeTutari,
    basariTutari: urun.basariTutari,
    sabitTutar: urun.sabitOdemeTutari,
    sabitTutarAy: urun.sabitOdemeAy,
    raporBasiOdemeTutari: urun .raporBasiOdemeTutari
  })

  const handleChange = (e) => {
    var f = {...form}
    f[e.target.name] = e.target.value
    setForm(f)
    setDegisiklikVar(true)
  }
  /*const teklifDegistirAction = (teklif) => {
    //console.log(teklif)
  }*/

  const onFiyatDegisti = (e) => {
    e.preventDefault();
    if (e.target.value.match(/^(\s*|\d+)$/)) {
      hesapla(e.target.name, e.target.value);
      handleChange(e);
    }
  };

  const hesapla = (fiyatAdi, fiyat) => {
    var toplam = 0;
    var araToplam = 0;
    var ay = form.sabitTutarAy;
    switch (fiyatAdi) {
      case "onOdemeTutari":
        toplam += urunData.onOdemeTutariVar ? parseInt(fiyat) || 0 : 0;
        toplam += urunData.basariTutariVar
          ? parseInt(form.basariTutari) || 0
          : 0;
        toplam += urunData.sabitTutarVar
          ? (parseInt(form.sabitTutar) || 0) *
            (parseInt(form.sabitTutarAy) || 0)
          : 0;
        toplam += urunData.raporBasiOdemeTutariVar
          ? parseInt(form.raporBasiOdemeTutari) || 0
          : 0;
        break;
      case "basariTutari":
        toplam += urunData.onOdemeTutariVar
          ? parseInt(form.onOdemeTutari) || 0
          : 0;
        toplam += urunData.basariTutariVar ? parseInt(fiyat) || 0 : 0;
        toplam += urunData.sabitTutarVar
          ? (parseInt(form.sabitTutar) || 0) *
            (parseInt(form.sabitTutarAy) || 0)
          : 0;
        toplam += urunData.raporBasiOdemeTutariVar
          ? parseInt(form.raporBasiOdemeTutari) || 0
          : 0;
        break;
      case "sabitTutar":
        toplam += urunData.onOdemeTutariVar
          ? parseInt(form.onOdemeTutari) || 0
          : 0;
        toplam += urunData.basariTutariVar
          ? parseInt(form.basariTutari) || 0
          : 0;
        toplam += urunData.sabitTutarVar
          ? (parseInt(fiyat) || 0) * (parseInt(form.sabitTutarAy) || 0)
          : 0;
        toplam += urunData.raporBasiOdemeTutariVar
          ? parseInt(form.raporBasiOdemeTutari) || 0
          : 0;
        break;
      case "sabitTutarAy":
        ay = parseInt(fiyat);
        toplam += urunData.onOdemeTutariVar
          ? parseInt(form.onOdemeTutari) || 0
          : 0;
        toplam += urunData.basariTutariVar
          ? parseInt(form.basariTutari) || 0
          : 0;
        toplam += urunData.sabitTutarVar
          ? (parseInt(form.sabitTutar) || 0) * (parseInt(fiyat) || 0)
          : 0;
        toplam += urunData.raporBasiOdemeTutariVar
          ? parseInt(form.raporBasiOdemeTutari) || 0
          : 0;
        break;
      case "raporBasiOdemeTutari":
        toplam += urunData.onOdemeTutariVar
          ? parseInt(form.onOdemeTutari) || 0
          : 0;
        toplam += urunData.basariTutariVar
          ? parseInt(form.basariTutari) || 0
          : 0;
        toplam += urunData.sabitTutarVar
          ? (parseInt(form.sabitTutar) || 0) *
            (parseInt(form.sabitTutarAy) || 0)
          : 0;
        toplam += urunData.raporBasiOdemeTutariVar ? parseInt(fiyat) || 0 : 0;
        break;
    }

    araToplam += urunData.onOdemeTutariVar ? urunData.onOdemeTutari : 0;
    araToplam += urunData.basariTutariVar ? urunData.basariTutari : 0;
    araToplam += urunData.sabitTutarVar ? urunData.sabitTutar * ay || 0 : 0;
    araToplam += urunData.raporBasiOdemeTutariVar
      ? urunData.raporBasiOdemeTutari
      : 0;


      var _toplamlar = {...toplamlar}
      _toplamlar.araToplam = araToplam;
      _toplamlar.indirim = Math.max(araToplam - toplam, 0);
      _toplamlar.kdvOran = urunData.kdvOrani;
      _toplamlar.kdv = (urunData.kdvOrani * toplam) / 100;
      _toplamlar.toplam = toplam;
      setToplamlar(_toplamlar)
  
/*
    var _form = { ...form };
    _form.araToplam = araToplam;
    _form.indirim = Math.max(araToplam - toplam, 0);
    _form.kdvOran = urun.kdvOrani;
    _form.kdv = (urun.kdvOrani * toplam) / 100;
    _form.toplam = toplam;

    if (araToplam - toplam !== 0) {
      _form.fiyatDegisikligiVar = true;
    } else {
      _form.fiyatDegisikligiVar = false;
    }

    if (araToplam - toplam > ((user.indirimOrani || 0) * toplam) / 100) {
      _form.onay = false;
    } else {
      _form.onay = true;
    }
    //console.log(_form)
    setForm(_form);
    */
  };

  if (!urunData) {
    return <></>
  }

  return (
    <div className="flex-col w-full">
      {form && urunData.onOdemeTutariVar && (
        <div className="my-6">
          <TextField
            label="Ön Ödeme Tutarı"
            variant="outlined"
            fullWidth
            helperText={`Ürün fiyatı ${urunData.onOdemeTutari} TL`}
            id="onOdemeTutari"
            name="onOdemeTutari"
            value={form.onOdemeTutari}
            InputProps={{
              endAdornment:(<InputAdornment position="end">TL</InputAdornment>)
            }}
            onChange={(e) => {
              if (e.target.value.match(/^(\s*|\d+)$/)) {
                onFiyatDegisti(e);
              }
            }}
          />
        </div>
      )}
      {form && urunData.basariTutariVar && (
        <div className="my-6">
          <TextField
            variant="outlined"
            helperText={`Ürün fiyatı ${urunData.basariTutari} TL`}
            required
            label="Başarı Primi"
            id="basariTutari"
            name="basariTutari"
            value={form.basariTutari}
            InputProps={{
              readOnly: !yeni,
              endAdornment:(<InputAdornment position="end">TL</InputAdornment>)
            }}
            onChange={(e) => {
              if (e.target.value.match(/^(\s*|\d+)$/)) {
                onFiyatDegisti(e);
              }
            }}
            fullWidth
          />
        </div>
      )}
      {form && urunData.sabitTutarVar && (
        <div className="flex my-6">
          <TextField
            variant="outlined"
            fullWidth
            helperText={`Ürün fiyatı ${urunData.sabitTutar} TL`}
            required
            label="Sabit Ödeme"
            id="sabitTutar"
            name="sabitTutar"
            value={form.sabitTutar}
            InputProps={{
              readOnly: !yeni,
              endAdornment:(<InputAdornment position="end">TL</InputAdornment>)
            }}
            onChange={(e) => {
              if (e.target.value.match(/^(\s*|\d+)$/)) {
                onFiyatDegisti(e);
              }
            }}
          />
          <div className="ml-4">
            <TextField
              label="Ay"
              variant="outlined"
              fullWidth
              id="sabitTutarAy"
              name="sabitTutarAy"
              value={form.sabitTutarAy}
              InputProps={{
                readOnly: !yeni,
                endAdornment:(<InputAdornment position="end">Ay</InputAdornment>)
              }}
              onChange={(e) => {
                if (e.target.value.match(/^(\s*|\d+)$/)) {
                  onFiyatDegisti(e);
                }
              }}
            />
          </div>
        </div>
      )}
      {form && urunData.raporBasiOdemeTutariVar && (
        <div className="my-6">
          <TextField
            variant="outlined"
            fullWidth
            helperText={`Ürün fiyatı ${urunData.raporBasiOdemeTutari} TL`}
            required
            label="Rapor Başına Ödeme"
            id="raporBasiOdemeTutari"
            name="raporBasiOdemeTutari"
            value={form.raporBasiOdemeTutari}
            InputProps={{
              readOnly: !yeni,
              endAdornment:(<InputAdornment position="end">TL</InputAdornment>)
            }}
            onChange={(e) => {
              if (e.target.value.match(/^(\s*|\d+)$/)) {
                onFiyatDegisti(e);
              }
            }}
          />
        </div>
      )}
      <Divider />
      {form && <SatisFirsatiDipToplam toplam={toplamlar} />}
      <div className="m-8">
        <Button
          variant="outlined"
          color="primary"
          onClick={() => {
            //urun(null);
            //onEkle(form);
            onEdit({
              oncekiTeklif: urun,
              yeniTeklif: form,
              yeniToplamlar: toplamlar
                  //  , ...form
                  //  , toplamlar
                  })
          }}
          disabled={!degisiklikVar}
        >
          Değiştir
        </Button>
      </div>
    </div>
  );
};

export default UrunTeklifEditComponent;
