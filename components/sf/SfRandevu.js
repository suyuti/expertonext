import React, { useState } from "react";
import {
  Card,
  CardHeader,
  Divider,
  CardActions,
  CardContent,
  TextField,
  Button,
  MenuItem,
  Typography,
} from "@material-ui/core";
import { useRouter } from "next/router";
import axios from "axios";
import baseUrl from "../../utils/baseUrl";
import { DateTimePicker, KeyboardDateTimePicker } from "@material-ui/pickers";
import EventIcon from '@material-ui/icons/Event';
import Snackbar from '../shared/SnackBar'


const SatisFirsatiRandevu = (props) => {
  const router = useRouter();
  const { user, token, personeller, satisFirsati } = props;

  const [snackbarInfo, setSnackbarInfo] = useState({severity:'', message: ''})
  const [open, setOpen] = React.useState(false);
  const [form, setForm] = useState({
    durum: 'RANDEVU'
  })
  
  const handleChange = (e) => {
    var _sf = { ...form };
    _sf[e.target.name] = e.target.value;
    setForm(_sf);
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  const handleSubmit = (e) => {
    axios.post(`${baseUrl}/api/sfs/${satisFirsati._id}/randevu`, 
      {
        musteri     : satisFirsati.musteri._id,
        konum       : form.randevuYeri,
        zaman       : form.randevuTarihi,
        aciklama    : form.randevuNotu,
        satisFirsati: satisFirsati._id
      },
      {headers: { authorization: token }}
    )
    .then(resp => {
      if (resp.status === 200) {
        setSnackbarInfo({severity:'success', message:'Randevu oluşturuldu'})
        setForm({})
      }
      else {
        setSnackbarInfo({severity:'error', message:'Randevu kaydedilemedi'})
      }
      setOpen(true);
    })
/*




    axios.put(`${baseUrl}/api/sfs/${satisFirsati._id}`, form, {
      headers: { authorization: token },
    })
    .then(resp => {
      if (resp.status === 200) {
        setSnackbarInfo({severity:'success', message:'Randevu oluşturuldu'})
        setForm({})
      }
      else {
        setSnackbarInfo({severity:'error', message:'Randevu kaydedilemedi'})
      }
      setOpen(true);
  })
  */
  };

  const handleRandevuZamani = (val) => {
    var _f = {...form}
    _f.randevuTarihi = val
    setForm(_f)
  }

  const canSubmit = () => {
    return form.randevuTarihi && form.randevuYeri
  }

  return (
    <Card className="w-full border-t-4 border-red-200">
      <CardHeader 
        title= {
          <Typography variant='h6'>Satış Fırsatı İçin Randevu Oluşturma</Typography>
        }
        subheader={`${satisFirsati.musteri.marka} için randevu oluşturma`}
        avatar={<EventIcon fontSize='large' />}
        />
      <Divider />
      <CardContent>
        <div className="flex-col">
          <div className="m-8">
            <TextField
              label="Musteri"
              variant="outlined"
              name="musteri"
              value={satisFirsati.musteri.marka}
              fullWidth
            />
          </div>
          <div className="m-8">
          <KeyboardDateTimePicker
                variant="inline"
                inputVariant='outlined'
                ampm={false}
                label="Randevu Tarihi"
                value={form.randevuTarihi}
                onChange={handleRandevuZamani}
                onError={console.log}
                //disableFuture
                //disablePast
                format="dd/MM/yyyy HH:mm"
                name="randevuTarihi"
                fullWidth
                autoOk
                showTodayButton
              />
          </div>
          <div className="m-8">
            <TextField
              label="Yer"
              variant="outlined"
              fullWidth
              name="randevuYeri"
              value={form.randevuYeri}
              onChange={handleChange}
            />
          </div>
          <div className="m-8">
            <TextField
              label="Katilimcilar"
              variant="outlined"
              fullWidth
              name="katilimcilar"
              value={form.katilimcilar}
              onChange={handleChange}
            />
          </div>

          <div className="m-8">
            <TextField
              label="Not"
              variant="outlined"
              fullWidth
              multiline
              rows={4}
              name="randevuNotu"
              value={form.randevuNotu}
              onChange={handleChange}
            />
          </div>
        </div>
      </CardContent>
      <Divider />
      <CardActions>
        <div className="flex w-full justify-end p-8 pr-16">
          <Button 
            variant="contained" 
            color="primary" 
            onClick={handleSubmit}
            disabled={!canSubmit()}
            >
            Oluştur
          </Button>
          {open && 
            <Snackbar open={open} severity={snackbarInfo.severity} message={snackbarInfo.message} handleClose={handleClose}/>
          }
        </div>
      </CardActions>
    </Card>
  );
};

export default SatisFirsatiRandevu;
