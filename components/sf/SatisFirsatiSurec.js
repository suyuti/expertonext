import React from 'react'
import { Paper, Stepper, Step, StepLabel } from '@material-ui/core'

const SatisFirsatiSurec = (props) => {
    const {satisFirsati} = props

    const getActiveStep = () => {
        if (satisFirsati.durum === 'YENI') {
            return 1
        }
        else if (satisFirsati.durum === 'RANDEVU') {
            return 2
        }
        else if (satisFirsati.durum === 'TEKLIF') {
            return 3
        }
        else if (satisFirsati.durum === 'ONAY') {
            return 4
        }
        if (satisFirsati.durum === 'KAPALI') {
            return 5
        }
    }

    return (
        <Paper className="w-full rounded  shadowed border-1 mb-8">
            <Stepper activeStep={getActiveStep()}>
                <Step key='1'><StepLabel>Yeni Satş Fırsatı</StepLabel></Step>
                <Step key='2'><StepLabel>Randevu Oluşturma</StepLabel></Step>
                <Step key='3'><StepLabel>Teklif Oluşturma</StepLabel></Step>
                <Step key='4'><StepLabel>Onaylama</StepLabel></Step>
                <Step key='5'><StepLabel>Sonuçlandırma</StepLabel></Step>
            </Stepper>
        </Paper>
    )
}

export default SatisFirsatiSurec