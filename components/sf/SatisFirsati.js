import React, { useState } from "react";
import {
  Card,
  CardHeader,
  Divider,
  CardActions,
  CardContent,
  TextField,
  Button,
  MenuItem,
  Typography,
} from "@material-ui/core";
import { useRouter } from "next/router";
import axios from "axios";
import baseUrl from "../../utils/baseUrl";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Snackbar from '../shared/SnackBar'
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import {Autocomplete} from '@material-ui/lab'

const SatisFirsati = (props) => {
  const prep = () => {
    var _sf = { ...satisFirsati };
    _sf.atananPersonel = user._id;
    return _sf;
  };
  const router = useRouter();
  const { user, token, musteriler, personeller, satisFirsati } = props;
  const [sf, setSf] = useState(prep());
  const [snackbarInfo, setSnackbarInfo] = useState({severity:'', message: ''})

  const handleChange = (e) => {
    var _sf = { ...sf };
    _sf[e.target.name] = e.target.value;
    setSf(_sf);
  };

  const handleSubmit = (e) => {
    axios
      .post(`${baseUrl}/api/sfs`, sf, {
        headers: { authorization: token },
      })
      .then((resp) => {
        if (resp.status === 200) {
          setSnackbarInfo({severity:'success', message:'Satış Fırsatı kaydedildi'})
          setSf({})
        }
        else {
          setSnackbarInfo({severity:'error', message:'Satış Fırsatı kaydedilemedi'})
        }
        setOpen(true);
      });
  };

  const [open, setOpen] = React.useState(false);

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  const canSubmit = () => {
    return sf.musteri && sf.atananPersonel
  }

  return (
    <Card className="w-full border-t-4 border-red-200">
      <CardHeader 
      title= {
        <Typography variant='h6'>Satış Fırsatı Oluşturma</Typography>
      }
      avatar={<AddShoppingCartIcon fontSize='large' />}
      />
      <Divider />
      <CardContent>
        <div className="flex-col">
          <div className='m-8'>
            <Autocomplete 
              id='musteriler-combo'
              options={musteriler.data}
              getOptionLabel={(option) => option.marka}
              //getOptionSelected={(o,v) => {console.log(v)}}
              fullWidth
              autoHighlight={true}
              autoSelect={true}
              clearOnEscape={true}
              groupBy={o => o.durum}
              value={sf.musteri}
              renderInput={(params) => <TextField {...params} label="Musteri" variant="outlined" />}
              onChange={(e, v, r) => {
                if (r === 'select-option') {
                  setSf(prev => ({musteri : v}))
                }
                else if (r === 'clear') {
                  setSf(prev => ({musteri : null}))
                }
              }}
            />
          </div>
          {/* 
          <div className="m-8">
            <TextField
              label="Müşteri"
              variant="outlined"
              name="musteri"
              value={sf.musteri}
              fullWidth
              onChange={handleChange}
              select
            >
              {musteriler &&
                musteriler.data.map((m) => (
                  <MenuItem key={m._id} value={m._id}>
                      <Typography className={
                        {
                          'text-blue':m.durum === 'potansiyel',
                          'text-green':m.durum === 'aktif',
                          'text-orange':m.durum === 'pasif',
                          'text-red':m.durum === 'sorunlu',
                        }
                      }>{`${m.marka}`}</Typography>
                  </MenuItem>
                ))}
            </TextField>
          </div>
                    */}

          <div className="m-8">
            <TextField
              label="Atanan Personel"
              variant="outlined"
              fullWidth
              select
              name="atananPersonel"
              value={sf.atananPersonel}
              onChange={handleChange}
            >
              {personeller &&
                personeller.data.map((p) => (
                  <MenuItem key={p._id} value={p._id}>
                    {user._id === p._id ? 
                      <Typography className='text-green-700'>{`${p.adi} ${p.soyadi} - (Ben)`}</Typography>
                    :
                      <Typography className=''>{`${p.adi} ${p.soyadi}`}</Typography>
                  }</MenuItem>
                ))}
            </TextField>
          </div>
          <div className="m-8">
            <TextField
              label="Not"
              variant="outlined"
              fullWidth
              multiline
              rows={4}
              name="not"
              value={sf.not}
              onChange={handleChange}
            />
          </div>
        </div>
      </CardContent>
      <Divider />
      <CardActions>
        <div className="flex w-full justify-end p-8 pr-16">
          <Button 
            variant="contained" 
            color="primary" 
            onClick={handleSubmit}
            disabled={!canSubmit()}
            >
            Satış Fırsatı Oluştur
          </Button>
          {open && 
            <Snackbar open={open} severity={snackbarInfo.severity} message={snackbarInfo.message} handleClose={handleClose}/>
          }
        </div>
      </CardActions>
    </Card>
  );
};

export default SatisFirsati;
