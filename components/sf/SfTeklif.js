import React, { useState } from "react";
import {
  Card,
  CardHeader,
  Divider,
  CardActions,
  CardContent,
  TextField,
  Button,
  MenuItem,
  Table,
  TableHead,
  TableRow,
  TableCell,
  makeStyles,
  Paper,
  Typography,
} from "@material-ui/core";
import { useRouter } from "next/router";
import axios from "axios";
import baseUrl from "../../utils/baseUrl";
import MaterialTable, { MTableBody } from "material-table";
import SwipeableViews from "react-swipeable-views";
import UrunTeklifComponent from "./UrunTeklif";
import { uuid } from "uuidv4";
import Snackbar from '../shared/SnackBar'


const useStyles = makeStyles((theme) => ({
  table: {
    border: "none",
    boxShadow: "none",
  },
}));

const SatisFirsatiTeklif = (props) => {
  const classes = useStyles();
  const prep = () => {
    var _sf = { ...satisFirsati };
    _sf.atananPersonel = user._id;
    return _sf;
  };
  const router = useRouter();
  const { user, token, musteriler, personeller, satisFirsati } = props;
  const [sf, setSf] = useState(prep());
  const [viewIndex, setViewIndex] = useState(0);
  const [teklifler, setTeklifler] = useState([]);
  const [snackbarInfo, setSnackbarInfo] = useState({severity:'', message: ''})
  const [open, setOpen] = React.useState(false);

  const handleChange = (e) => {
    var _sf = { ...sf };
    _sf[e.target.name] = e.target.value;
    setSf(_sf);
  };

  const handleChangeIndex = (i) => {
    setViewIndex(i);
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }
    setOpen(false);
  };

  const handleSubmit = (e) => {
    axios
      .put(
        `${baseUrl}/api/sfs/${satisFirsati._id}`,
        {
          teklifler: teklifler,
          durum: "TEKLIF",
        },
        {
          headers: { authorization: token },
        }
      )
      .then((resp) => {
        if (resp.status === 200) {
          setSnackbarInfo({
            severity: "success",
            message: "Teklifler oluşturuldu",
          });
         // setForm({});
        } else {
          setSnackbarInfo({
            severity: "error",
            message: "Teklifler kaydedilemedi",
          });
        }
        setOpen(true);
      });
  };

  const handleUrunEkle = (urun) => {
    var _t = [...teklifler];
    _t.push({ urun: urun, id: uuid() });
    setTeklifler(_t);
  };

  const handleUrunCikar = (id) => {
    var _t = teklifler.filter((t) => t.id != id);
    setTeklifler(_t);
  };

  return (
    <Card className="w-full border-t-4 border-red-200">
      <CardHeader
        title="Teklif Oluşturma"
        subheader={
          <Typography>{`${sf.musteri.marka} için teklif oluşturma`}</Typography>
        }
      />
      <Divider />
      <CardContent>
        <div className="flex-col">
          <SwipeableViews index={viewIndex} onChangeIndex={handleChangeIndex}>
            <div className="flex" index={0}>
              <div className="w-full">
                <MaterialTable
                  title="Secili Urunler"
                  columns={[
                    { title: "Ürün", field: "urun.seciliUrun.adi" },
                    { title: "Ara Toplam", field: "urun.araToplam" },
                    { title: "Toplam", field: "urun.toplam" },
                    { title: "İndirim", field: "urun.indirim" },
                  ]}
                  data={teklifler}
                  options={{
                    actionsColumnIndex: -1,
                    search: false,
                    paging: false,
                    minBodyHeight: 360,
                    padding: "dense",
                    showSelectAllCheckbox: true,
                  }}
                  localization={{
                    body: {
                      emptyDataSourceMessage: "Herhangi bir ürün seçilmedi",
                    },
                  }}
                  actions={[
                    {
                      icon: "add",
                      tooltip: "Urun Ekle",
                      isFreeAction: true,
                      onClick: (event) => setViewIndex(1),
                    },
                    {
                      icon: "delete",
                      tooltip: "Ürünü iptal et",
                      onClick: (event, rowData) => handleUrunCikar(rowData.id),
                    },
                  ]}
                  components={{
                    Container: (props) => (
                      <Paper className={classes.table} {...props} />
                    ),
                  }}
                />
              </div>
            </div>
            <div className="flex" index={1}>
              <UrunTeklifComponent
                onEkle={(urun) => {
                  handleUrunEkle(urun);
                  setViewIndex(0);
                }}
                onIptal={() => setViewIndex(0)}
                {...props}
              />
            </div>
          </SwipeableViews>
        </div>
      </CardContent>
      <Divider />
      <CardActions>
        <div className="flex w-full justify-end p-8 pr-16">
          <Button
            variant="contained"
            color="primary"
            onClick={handleSubmit}
            disabled={viewIndex != 0 || teklifler.length === 0}
          >
            Oluştur
          </Button>
          {open && 
            <Snackbar open={open} severity={snackbarInfo.severity} message={snackbarInfo.message} handleClose={handleClose}/>
          }
        </div>
      </CardActions>
    </Card>
  );
};

export default SatisFirsatiTeklif;
