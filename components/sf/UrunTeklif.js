import React, { useState, useEffect } from "react";
import axios from "axios";
import useSWR from "swr";
import baseUrl from "../../utils/baseUrl";
import { Button, TextField, MenuItem, Divider } from "@material-ui/core";
import SatisFirsatiDipToplam from "../satisFirsati/SatisFirsatiDipToplam";
import useForm from "../../utils/hooks/useForm";
import {Autocomplete} from '@material-ui/lab'

const yeni = true;

const UrunTeklifComponent = (props) => {
  const { token, user, onEkle, onIptal, urun } = props;
  const [seciliUrun, setSeciliUrun] = useState(null);
  const { data: urunler, error: urunError } = useSWR(
    `${baseUrl}/api/urunler`,
    (url) =>
      axios
        .get(url, { headers: { authorization: token } })
        .then((res) => res.data.data)
  );
  const { form, handleChange, setForm } = useForm({});

  const onUrunSelected = (e) => {
    urunSec(e.target.value);
  };

  const urunSec = (uid) => {
    if (yeni) {
      let _urun = urunler.find((u) => u._id === uid);
      let _form = { ...form };
      _form.onOdemeTutari = _urun.onOdemeTutari;
      _form.basariTutari = _urun.basariTutari;
      _form.sabitTutar = _urun.sabitTutar;
      _form.sabitTutarAy = 12;
      _form.raporBasiOdemeTutari = _urun.raporBasiOdemeTutari;
      _form.yuzdeTutari = _urun.yuzdeTutari;
      _form.seciliUrun = _urun;

      _form.araToplam =
        _urun.onOdemeTutari +
        _urun.basariTutari +
        _urun.sabitTutar * 12 +
        _urun.raporBasiOdemeTutari;
      _form.indirim = 0;
      _form.kdvOran = _urun.kdvOrani;
      _form.kdv = (_urun.kdvOrani * _form.araToplam) / 100;
      _form.toplam = _form.araToplam;

      setForm(_form);
      setSeciliUrun(_urun);
    } else {
      let _urun = urunler.find((u) => u._id === uid);
      setSeciliUrun(_urun);
    }
  };

  const onFiyatDegisti = (e) => {
    e.preventDefault();
    if (e.target.value.match(/^(\s*|\d+)$/)) {
      hesapla(e.target.name, e.target.value);
      handleChange(e);
    }
  };

  const hesapla = (fiyatAdi, fiyat) => {
    var toplam = 0;
    var araToplam = 0;
    var ay = form.sabitTutarAy;
    switch (fiyatAdi) {
      case "onOdemeTutari":
        toplam += seciliUrun.onOdemeTutariVar ? parseInt(fiyat) || 0 : 0;
        toplam += seciliUrun.basariTutariVar
          ? parseInt(form.basariTutari) || 0
          : 0;
        toplam += seciliUrun.sabitTutarVar
          ? (parseInt(form.sabitTutar) || 0) *
            (parseInt(form.sabitTutarAy) || 0)
          : 0;
        toplam += seciliUrun.raporBasiOdemeTutariVar
          ? parseInt(form.raporBasiOdemeTutari) || 0
          : 0;
        break;
      case "basariTutari":
        toplam += seciliUrun.onOdemeTutariVar
          ? parseInt(form.onOdemeTutari) || 0
          : 0;
        toplam += seciliUrun.basariTutariVar ? parseInt(fiyat) || 0 : 0;
        toplam += seciliUrun.sabitTutarVar
          ? (parseInt(form.sabitTutar) || 0) *
            (parseInt(form.sabitTutarAy) || 0)
          : 0;
        toplam += seciliUrun.raporBasiOdemeTutariVar
          ? parseInt(form.raporBasiOdemeTutari) || 0
          : 0;
        break;
      case "sabitTutar":
        toplam += seciliUrun.onOdemeTutariVar
          ? parseInt(form.onOdemeTutari) || 0
          : 0;
        toplam += seciliUrun.basariTutariVar
          ? parseInt(form.basariTutari) || 0
          : 0;
        toplam += seciliUrun.sabitTutarVar
          ? (parseInt(fiyat) || 0) * (parseInt(form.sabitTutarAy) || 0)
          : 0;
        toplam += seciliUrun.raporBasiOdemeTutariVar
          ? parseInt(form.raporBasiOdemeTutari) || 0
          : 0;
        break;
      case "sabitTutarAy":
        ay = parseInt(fiyat);
        toplam += seciliUrun.onOdemeTutariVar
          ? parseInt(form.onOdemeTutari) || 0
          : 0;
        toplam += seciliUrun.basariTutariVar
          ? parseInt(form.basariTutari) || 0
          : 0;
        toplam += seciliUrun.sabitTutarVar
          ? (parseInt(form.sabitTutar) || 0) * (parseInt(fiyat) || 0)
          : 0;
        toplam += seciliUrun.raporBasiOdemeTutariVar
          ? parseInt(form.raporBasiOdemeTutari) || 0
          : 0;
        break;
      case "raporBasiOdemeTutari":
        toplam += seciliUrun.onOdemeTutariVar
          ? parseInt(form.onOdemeTutari) || 0
          : 0;
        toplam += seciliUrun.basariTutariVar
          ? parseInt(form.basariTutari) || 0
          : 0;
        toplam += seciliUrun.sabitTutarVar
          ? (parseInt(form.sabitTutar) || 0) *
            (parseInt(form.sabitTutarAy) || 0)
          : 0;
        toplam += seciliUrun.raporBasiOdemeTutariVar ? parseInt(fiyat) || 0 : 0;
        break;
    }

    araToplam += seciliUrun.onOdemeTutariVar ? seciliUrun.onOdemeTutari : 0;
    araToplam += seciliUrun.basariTutariVar ? seciliUrun.basariTutari : 0;
    araToplam += seciliUrun.sabitTutarVar ? seciliUrun.sabitTutar * ay || 0 : 0;
    araToplam += seciliUrun.raporBasiOdemeTutariVar
      ? seciliUrun.raporBasiOdemeTutari
      : 0;

    var _form = { ...form };
    _form.araToplam = araToplam;
    _form.indirim = Math.max(araToplam - toplam, 0);
    _form.kdvOran = seciliUrun.kdvOrani;
    _form.kdv = (seciliUrun.kdvOrani * toplam) / 100;
    _form.toplam = toplam;

    if (araToplam - toplam !== 0) {
      _form.fiyatDegisikligiVar = true;
    } else {
      _form.fiyatDegisikligiVar = false;
    }

    if (araToplam - toplam > ((user.indirimOrani || 0) * toplam) / 100) {
      _form.onay = false;
    } else {
      _form.onay = true;
    }
    setForm(_form);
  };

  return (
    <div className="flex-col w-full">
      {!urun && (
        <div className="my-6">
            <Autocomplete 
              id='urunler-combo'
              options={urunler}
              getOptionLabel={(option) => option.adi}
              fullWidth
              autoHighlight={true}
              autoSelect={true}
              clearOnEscape={true}
              //groupBy={o => o.durum}
              //value={seciliUrun}
              renderInput={(params) => <TextField {...params} label="Ürün" variant="outlined" />}
              onChange={(e, v, r) => {
                if (r === 'select-option') {
                  urunSec(v._id)
                  //setSf(prev => ({musteri : v}))
                }
                else if (r === 'clear') {
                  setSeciliUrun(null)
                  //urunSec(null)
                  //setSf(prev => ({musteri : null}))
                }
              }}
            />

{/*


          <TextField
            label="Urun"
            name="seciliUrun"
            variant="outlined"
            onChange={onUrunSelected}
            value={seciliUrun ? seciliUrun._id : null}
            fullWidth
            select
            InputProps={{
              readOnly: !yeni,
            }}
          >
            {urunler &&
              urunler.map((u) => (
                <MenuItem key={u._id} value={u._id}>
                  {u.adi}
                </MenuItem>
              ))}
          </TextField>
        */}
        </div>
      )}
      {seciliUrun && seciliUrun.onOdemeTutariVar && (
        <div className="my-6">
          <TextField
            label="On Odeme Tutari"
            variant="outlined"
            fullWidth
            helperText={`Ürün fiyatı ${seciliUrun.onOdemeTutari} TL`}
            id="onOdemeTutari"
            name="onOdemeTutari"
            value={form.onOdemeTutari}
            InputProps={{
              readOnly: !yeni,
            }}
            onChange={(e) => {
              if (e.target.value.match(/^(\s*|\d+)$/)) {
                onFiyatDegisti(e);
              }
            }}
          />
        </div>
      )}
      {seciliUrun && seciliUrun.basariTutariVar && (
        <div className="my-6">
          <TextField
            variant="outlined"
            helperText={`Ürün fiyatı ${seciliUrun.basariTutari} TL`}
            required
            label="Başarı Primi"
            id="basariTutari"
            name="basariTutari"
            value={form.basariTutari}
            InputProps={{
              readOnly: !yeni,
            }}
            onChange={(e) => {
              if (e.target.value.match(/^(\s*|\d+)$/)) {
                onFiyatDegisti(e);
              }
            }}
            fullWidth
          />
        </div>
      )}
      {seciliUrun && seciliUrun.sabitTutarVar && (
        <div className="flex my-6">
          <TextField
            variant="outlined"
            fullWidth
            helperText={`Ürün fiyatı ${seciliUrun.sabitTutar} TL`}
            required
            label="Sabit Ödeme"
            id="sabitTutar"
            name="sabitTutar"
            value={form.sabitTutar}
            InputProps={{
              readOnly: !yeni,
            }}
            onChange={(e) => {
              if (e.target.value.match(/^(\s*|\d+)$/)) {
                onFiyatDegisti(e);
              }
            }}
          />
          <div className="ml-4">
            <TextField
              label="Ay"
              variant="outlined"
              fullWidth
              id="sabitTutarAy"
              name="sabitTutarAy"
              value={form.sabitTutarAy}
              InputProps={{
                readOnly: !yeni,
              }}
              onChange={(e) => {
                if (e.target.value.match(/^(\s*|\d+)$/)) {
                  onFiyatDegisti(e);
                }
              }}
            />
          </div>
        </div>
      )}
      {seciliUrun && seciliUrun.raporBasiOdemeTutariVar && (
        <div className="my-6">
          <TextField
            variant="outlined"
            fullWidth
            helperText={`Ürün fiyatı ${seciliUrun.raporBasiOdemeTutari} TL`}
            required
            label="Rapor Başına Ödeme"
            id="raporBasiOdemeTutari"
            name="raporBasiOdemeTutari"
            value={form.raporBasiOdemeTutari}
            InputProps={{
              readOnly: !yeni,
            }}
            onChange={(e) => {
              if (e.target.value.match(/^(\s*|\d+)$/)) {
                onFiyatDegisti(e);
              }
            }}
          />
        </div>
      )}
      {/*seciliUrun && seciliUrun.yuzdeTutariVar && (
        <div className="my-6">
          <TextField
            variant="outlined"
            helperText={`Ürün yüzdesi %${seciliUrun.yuzdeTutari}`}
            required
            label="Yüzde"
            id="yuzdeTutari"
            name="yuzdeTutari"
            value={form.yuzdeTutari}
            InputProps={{
              readOnly: !yeni,
            }}
            onChange={(e) => {
              if (e.target.value.match(/^(\s*|\d+)$/)) {
                handleChange(e);
              }
            }}
            fullWidth
          />
        </div>
          )
      */}{" "}
      <Divider />
      {seciliUrun && <SatisFirsatiDipToplam toplam={form} />}
      <div className="m-8">
        <Button
          variant="outlined"
          color="primary"
          onClick={() => {
            setSeciliUrun(null);
            onEkle(form);
          }}
          disabled={!seciliUrun}
        >
          Ekle
        </Button>
        <Button
          variant="outlined"
          color="primary"
          onClick={() => {
            setSeciliUrun(null);
            setForm({});
            onIptal();
          }}
        >
          Iptal
        </Button>
      </div>
    </div>
  );
};

export default UrunTeklifComponent;
