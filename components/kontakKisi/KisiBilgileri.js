import React, { useState } from "react";
import {
  Button,
  Typography,
  TextField,
  Card,
  CardHeader,
  Divider,
  CardContent,
  MenuItem,
  CardActions
} from "@material-ui/core";
import baseUrl from "../../utils/baseUrl";
import axios from "axios";
import { useRouter } from "next/router";

const KisiBilgileri = (props) => {
  const router = useRouter()
  const { kisi, musteriler, user, token } = props;
  const [form, setForm] = useState(kisi);
  const [editable, setEditable] = useState(user.yetkiler.edit_kisi);
  const [degisiklikVar, setDegisiklikVar] = useState(false);
  const [yeni, setYeni] = useState(router.query.id === 'new')
  const {id} = router.query

  const handleChange = (e) => {
    setDegisiklikVar(true);
    var m = { ...form };
    m[e.target.name] = e.target.value;
    setForm(m);
  };

  const save = async () => {
    if (yeni) {
      axios.post(`${baseUrl}/api/kisiler`, form, {headers: {authorization: token}})
    }
    else {
      axios.put(`${baseUrl}/api/kisiler/${id}`, form, {headers: {authorization: token}})
    }
  }

  return (
    <Card className="w-full border-t-4 border-blue-200">
      <CardHeader
        title={
          <Typography className="text-xl" variant="h6">
            Kontak Kişi Bilgileri
          </Typography>
        }
        subheader="Kontak kişi detayları"
        //avatar={<AccountBalanceIcon fontSize="large" />}
      />
      <Divider />
      <CardContent>
        <div className="m-4">
          <TextField 
            label="Musteri" 
            variant="outlined" 
            fullWidth 
            name='musteri'
            value={form.musteri}
            onChange={handleChange}
            InputProps={{
              readOnly: !editable || !yeni,
            }}

            select>
              {musteriler && musteriler.map(m => <MenuItem key={m._id} value={m._id}>{m.marka}</MenuItem>)}
            </TextField>
        </div>
        <div className="m-4">
          <div className="flex">
            <TextField
              label="Adi"
              variant="outlined"
              fullWidth
              name="adi"
              value={form.adi}
              onChange={handleChange}
              InputProps={{
                readOnly: !editable,
              }}
            />
            <TextField
              label="Soyadi"
              variant="outlined"
              fullWidth
              name="soyadi"
              value={form.soyadi}
              onChange={handleChange}
              InputProps={{
                readOnly: !editable,
              }}
            />
          </div>
        </div>
        <div className="m-4">
          <TextField
            label="Unvani"
            variant="outlined"
            fullWidth
            name="unvani"
            value={form.unvani}
            onChange={handleChange}
            InputProps={{
              readOnly: !editable,
            }}
          />
        </div>
        <div className="m-4">
          <TextField
            label="Yetki"
            variant="outlined"
            fullWidth
            name="yetki"
            value={form.yetki}
            onChange={handleChange}
            InputProps={{
              readOnly: !editable,
            }}
            select
          >
            <MenuItem key='yonetim' value='yonetim'>Yönetim Yetkilisi</MenuItem>
            <MenuItem key='teknik' value='teknik'>Teknik Yetkili</MenuItem>
            <MenuItem key='mali' value='mali'>Mali Yetkili</MenuItem>
          </TextField>
        </div>
        <div className="m-4">
          <div className="flex">
            <TextField
              label="Cinsiyet"
              variant="outlined"
              fullWidth
              select
              name="cinsiyet"
              value={form.cinsiyet}
              onChange={handleChange}
              InputProps={{
                readOnly: !editable,
              }}
            >
              <MenuItem key="Erkek" value="Erkek">
                Erkek
              </MenuItem>
              <MenuItem key="Kadın" value="Kadın">
                Kadın
              </MenuItem>
            </TextField>
            <TextField
              label="Dogum tarihi"
              variant="outlined"
              fullWidth
              InputProps={{
                readOnly: !editable,
              }}
            />
          </div>
        </div>
        <div className="m-4">
          <TextField
            label="Cep Telefonu"
            variant="outlined"
            fullWidth
            name="cepTelefon"
            value={form.cepTelefon}
            onChange={handleChange}
            InputProps={{
              readOnly: !editable,
            }}
          />
        </div>
        <div className="m-4">
          <div className="flex">
            <TextField
              label="Is Telefonu"
              variant="outlined"
              fullWidth
              name="isTelefon"
              value={form.isTelefon}
              onChange={handleChange}
              InputProps={{
                readOnly: !editable,
              }}
            />
            <TextField
              label="Dahili"
              variant="outlined"
              fullWidth
              name="dahili"
              value={form.dahili}
              onChange={handleChange}
              InputProps={{
                readOnly: !editable,
              }}
            />
          </div>
        </div>
        <div className="m-4">
          <TextField
            label="Mail"
            variant="outlined"
            fullWidth
            name="mail"
            value={form.mail}
            onChange={handleChange}
            InputProps={{
              readOnly: !editable,
            }}
          />
        </div>
        <div className="m-4">
          <TextField
            label="TCKN"
            variant="outlined"
            fullWidth
            name="tckn"
            value={form.tckn}
            onChange={handleChange}
            InputProps={{
              readOnly: !editable,
            }}
          />
        </div>
        <div className="m-4"></div>
      </CardContent>
      <Divider />
      <CardActions className="flex justify-end">
          <Button 
            variant="contained" 
            color="primary" 
            onClick={save}
            disabled={!((user.yetkiler.edit_kisi || user.yetkiler.create_kisi) && degisiklikVar)}
            >
            Kaydet
          </Button>
      </CardActions>
    </Card>
  );
};

export default KisiBilgileri;
