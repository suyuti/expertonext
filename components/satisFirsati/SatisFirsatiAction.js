import React, { useState } from "react";
import { Paper, Button, Typography } from "@material-ui/core";

const SatisFirsatiAction = (props) => {
  const [atamaYapabilir, setAtamaYapabilir] = useState(true);
  const [sonuclandirabilir, setSonuclandirabilir] = useState(true);
  const [onaylayabilir, setOnaylayabilir] = useState(true);

  return (
    <Paper className="w-full rounded  shadowed  mb-8 flex justify-between items-center p-8 border-t-4 border-green-200">
      <Typography className="mx-16 " variant="h5">
        Eylemler
      </Typography>
      <div className="flex justify-end">
        <Button
          className="m-8"
          variant="outlined"
          color="primary"
          disabled={!atamaYapabilir}
          //onClick={actions.teklifIndir}
        >
          Atama
        </Button>
        <Button
          className="m-8"
          variant="outlined"
          color="primary"
          disabled={!sonuclandirabilir}
          //onClick={actions.teklifIndir}
        >
          Sonuçlandır
        </Button>
        <Button
          className="m-8"
          variant="outlined"
          color="primary"
          disabled={!onaylayabilir}
          //onClick={actions.teklifIndir}
        >
          Onayla
        </Button>
        <Button
          className="m-8"
          variant="outlined"
          color="primary"
          //disabled={!teklifIndirebilir}
          //onClick={actions.teklifIndir}
        >
          Randevu
        </Button>
        <Button
          className="m-8"
          variant="outlined"
          color="primary"
          //disabled={!teklifIndirebilir}
          //onClick={actions.teklifIndir}
        >
          Teklif
        </Button>
        <Button
          className="m-8"
          variant="outlined"
          color="primary"
          //disabled={!teklifIndirebilir}
          //onClick={actions.teklifIndir}
        >
          İptal
        </Button>
      </div>
    </Paper>
  );
};
export default SatisFirsatiAction;
