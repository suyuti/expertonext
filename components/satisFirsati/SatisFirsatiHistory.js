import React, { useState } from "react";
import {
  Card,
  CardHeader,
  Divider,
  CardContent,
  Typography,
  TextField,
  IconButton,
  Chip,
  Collapse,
  Table,
  TableHead,
  TableRow,
  TableCell,
} from "@material-ui/core";
import HistoryIcon from "@material-ui/icons/History";
import GetAppIcon from "@material-ui/icons/GetApp";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import ExpandLessIcon from "@material-ui/icons/ExpandLess";

const SatisFirsatiHistory = (props) => {
  const [open, setOpen] = useState(false);

  return (
    <Card className="w-full border-t-4 border-red-500">
      <CardHeader
        title={
          <Typography className="text-xl" variant="h6">
            Geçmiş eylemler
          </Typography>
        }
        subheader="Bu satış fırsatındaki tüm eylemler"
        avatar={
          <div className="flex items-center">
            <IconButton onClick={() => setOpen(!open)}>
              {open ? <ExpandLessIcon /> : <ExpandMoreIcon />}
            </IconButton>
            <HistoryIcon fontSize="large" />
          </div>
        }
        action={
          <IconButton
          //component={Link}
          //to={yeniUrl}
          >
            <GetAppIcon />
          </IconButton>
        }
      />
      <Divider />
      <Collapse in={open} timeout="auto" unmountOnExit>
        <CardContent>
            <Table size='small'>
                <TableHead>
                    <TableRow>
                        <TableCell>Eylem</TableCell>
                        <TableCell>Kullanici</TableCell>
                        <TableCell>Zaman</TableCell>
                    </TableRow>
                </TableHead>
            </Table>

        </CardContent>
      </Collapse>
    </Card>
  );
};

export default SatisFirsatiHistory;
