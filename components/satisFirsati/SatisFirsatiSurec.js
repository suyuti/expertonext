import React from 'react'
import { Paper, Stepper, Step, StepLabel } from '@material-ui/core'

const SatisFirsatiSurec = (props) => {
    return (
        <Paper className="w-full rounded  shadowed border-1 mb-8">
            <Stepper>
                <Step key='1'><StepLabel>Oluşturma</StepLabel></Step>
                <Step key='2'><StepLabel>Onay</StepLabel></Step>
                <Step key='3'><StepLabel>Randevu</StepLabel></Step>
                <Step key='4'><StepLabel>Teklif</StepLabel></Step>
                <Step key='5'><StepLabel>Sonuç</StepLabel></Step>
            </Stepper>
        </Paper>
    )
}

export default SatisFirsatiSurec