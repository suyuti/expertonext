import React from 'react'
import {Table, TableBody, TableRow, TableCell, Typography, makeStyles} from '@material-ui/core'

const useStyles = makeStyles(theme => ({
    label: {
        fontWeight: 100,
        fontSize: 14
    }
}))

const SatisFirsatiDipToplam = (props) => {
    const { toplam } = props;
    const classes = useStyles()

    return (
        <div className="flex-col mt-8">
        <Typography variant="body1" className="text-18">Toplamlar</Typography>
        <Table size="small">
            <TableBody>
                <TableRow>
                    <TableCell><Typography className={classes.label}>Ara Toplam</Typography></TableCell>
                    <TableCell><Typography className="text-right text-16">{toplam.araToplam} TL</Typography></TableCell>
                </TableRow>
                <TableRow>
                    <TableCell><Typography className={classes.label}>İndirim</Typography></TableCell>
                    <TableCell><Typography className="text-right text-16">{toplam.indirim} TL</Typography></TableCell>
                </TableRow>
                <TableRow>
                    <TableCell><Typography className={classes.label}>Toplam</Typography></TableCell>
                    <TableCell><Typography className="text-right text-16">{toplam.toplam} TL</Typography></TableCell>
                </TableRow>
                <TableRow>
                <TableCell><Typography className={classes.label}>KDV %{toplam.kdvOran}</Typography></TableCell>
                    <TableCell><Typography className="text-right text-16">{toplam.kdv} TL</Typography></TableCell>
                </TableRow>
                <TableRow>
                    <TableCell><Typography className={classes.label}>Genel Toplam</Typography></TableCell>
                    <TableCell><Typography className="text-right text-16">{toplam.toplam + toplam.kdv} TL</Typography></TableCell>
                </TableRow>
            </TableBody>
        </Table>
        </div>
    )
}
export default SatisFirsatiDipToplam