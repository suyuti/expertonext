import React from "react";
import {
  TextField,
  Card,
  CardHeader,
  Divider,
  CardContent,
  MenuItem,
  CardActions,
  Button,
  Typography,
} from "@material-ui/core";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";

const MusteriyeYapilimisTeklifler = (props) => {
    const {musteriId} = props
    const fetcher = (...args) => fetch(...args).then((res) => res.json());
    //const { data: teklıfler, error } = useSWR(() => `${baseUrl}/api/satisfirsati?musteri=` + musteriId, fetcher);
  

  return (
    <Card className="w-full  border-t-4 border-teal-200">
      <CardHeader
        title={<Typography variant="h6">Önceki teklifler</Typography>}
        subheader="Müşteriye daha önceden yapılmış teklifler"
        avatar={<ShoppingCartIcon fontSize="large" />}
      />
      <Divider />
      <CardContent>

      </CardContent>
    </Card>
  );
};

export default MusteriyeYapilimisTeklifler;
