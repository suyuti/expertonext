import React, { useState, useEffect } from "react";
import {
  TextField,
  Card,
  CardHeader,
  Divider,
  CardContent,
  MenuItem,
  CardActions,
  Button,
  Typography,
} from "@material-ui/core";
import useSWR from "swr";
import baseUrl from "../../utils/baseUrl";
import _ from "../../utils/lodash";
import useForm from "../../utils/hooks/useForm";
import SatisFirsatiDipToplam from "./SatisFirsatiDipToplam";
import cookie from "js-cookie";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import axios from "axios";

const SatisFirsati = (props) => {
  const { sf, user, yeni, token } = props;

  const fetcher = (...args) => fetch(...args).then((res) => res.json());
  const { data: musteriler, error: musterilerError } = useSWR(
    `${baseUrl}/api/musteriler`,
    (url) =>
      axios
        .get(url, {
          params: { filter: {} },
          headers: {
            authorization: token,
          },
        })
        .then((response) => response.data)
  );

  const { data: urunler, error: urunError } = useSWR(
    `${baseUrl}/api/urunler`,
    fetcher
  );

  useEffect(() => {
    if (!yeni && urunler) {
      urunSec(form.seciliUrun);
    }
  }, []);

  const { form, handleChange, setForm } = useForm(sf);
  const [seciliUrun, setSeciliUrun] = useState(null);

  const onUrunSelected = (e) => {
    urunSec(e.target.value);
    handleChange(e);
  };

  const urunSec = (uid) => {
    if (yeni) {
      let _urun = urunler.data.find((u) => u._id === uid);
      let _form = { ...form };
      _form.teklifFiyatlari.onOdemeTutari = _urun.onOdemeTutari;
      _form.teklifFiyatlari.basariTutari = _urun.basariTutari;
      _form.teklifFiyatlari.sabitTutar = _urun.sabitTutar;
      _form.teklifFiyatlari.sabitTutarAy = 12;
      _form.teklifFiyatlari.raporBasiOdemeTutari = _urun.raporBasiOdemeTutari;
      _form.teklifFiyatlari.yuzdeTutari = _urun.yuzdeTutari;
      _form.seciliUrun = _urun;

      _form.teklifFiyatlari.araToplam =
        _urun.onOdemeTutari +
        _urun.basariTutari +
        _urun.sabitTutar * 12 +
        _urun.raporBasiOdemeTutari;
      _form.teklifFiyatlari.indirim = 0;
      _form.teklifFiyatlari.kdvOran = _urun.kdvOrani;
      _form.teklifFiyatlari.kdv =
        (_urun.kdvOrani * _form.teklifFiyatlari.araToplam) / 100;
      _form.teklifFiyatlari.toplam = _form.teklifFiyatlari.araToplam;

      setForm(_form);
      setSeciliUrun(_urun);
    } else {
      let _urun = urunler.data.find((u) => u._id === uid);
      setSeciliUrun(_urun);
    }
  };

  const onFiyatDegisti = (e) => {
    e.preventDefault();
    if (e.target.value.match(/^(\s*|\d+)$/)) {
      hesapla(e.target.name, e.target.value);
      handleChange(e);
    }
  };

  const hesapla = (fiyatAdi, fiyat) => {
    var toplam = 0;
    var araToplam = 0;
    var ay = form.teklifFiyatlari.sabitTutarAy;
    switch (fiyatAdi) {
      case "teklifFiyatlari.onOdemeTutari":
        toplam += seciliUrun.onOdemeTutariVar ? parseInt(fiyat) || 0 : 0;
        toplam += seciliUrun.basariTutariVar
          ? parseInt(form.teklifFiyatlari.basariTutari) || 0
          : 0;
        toplam += seciliUrun.sabitTutarVar
          ? (parseInt(form.teklifFiyatlari.sabitTutar) || 0) *
            (parseInt(form.teklifFiyatlari.sabitTutarAy) || 0)
          : 0;
        toplam += seciliUrun.raporBasiOdemeTutariVar
          ? parseInt(form.teklifFiyatlari.raporBasiOdemeTutari) || 0
          : 0;
        break;
      case "teklifFiyatlari.basariTutari":
        toplam += seciliUrun.onOdemeTutariVar
          ? parseInt(form.teklifFiyatlari.onOdemeTutari) || 0
          : 0;
        toplam += seciliUrun.basariTutariVar ? parseInt(fiyat) || 0 : 0;
        toplam += seciliUrun.sabitTutarVar
          ? (parseInt(form.teklifFiyatlari.sabitTutar) || 0) *
            (parseInt(form.teklifFiyatlari.sabitTutarAy) || 0)
          : 0;
        toplam += seciliUrun.raporBasiOdemeTutariVar
          ? parseInt(form.teklifFiyatlari.raporBasiOdemeTutari) || 0
          : 0;
        break;
      case "teklifFiyatlari.sabitTutar":
        toplam += seciliUrun.onOdemeTutariVar
          ? parseInt(form.teklifFiyatlari.onOdemeTutari) || 0
          : 0;
        toplam += seciliUrun.basariTutariVar
          ? parseInt(form.teklifFiyatlari.basariTutari) || 0
          : 0;
        toplam += seciliUrun.sabitTutarVar
          ? (parseInt(fiyat) || 0) *
            (parseInt(form.teklifFiyatlari.sabitTutarAy) || 0)
          : 0;
        toplam += seciliUrun.raporBasiOdemeTutariVar
          ? parseInt(form.teklifFiyatlari.raporBasiOdemeTutari) || 0
          : 0;
        break;
      case "teklifFiyatlari.sabitTutarAy":
        ay = parseInt(fiyat);
        toplam += seciliUrun.onOdemeTutariVar
          ? parseInt(form.teklifFiyatlari.onOdemeTutari) || 0
          : 0;
        toplam += seciliUrun.basariTutariVar
          ? parseInt(form.teklifFiyatlari.basariTutari) || 0
          : 0;
        toplam += seciliUrun.sabitTutarVar
          ? (parseInt(form.teklifFiyatlari.sabitTutar) || 0) *
            (parseInt(fiyat) || 0)
          : 0;
        toplam += seciliUrun.raporBasiOdemeTutariVar
          ? parseInt(form.teklifFiyatlari.raporBasiOdemeTutari) || 0
          : 0;
        break;
      case "teklifFiyatlari.raporBasiOdemeTutari":
        toplam += seciliUrun.onOdemeTutariVar
          ? parseInt(form.teklifFiyatlari.onOdemeTutari) || 0
          : 0;
        toplam += seciliUrun.basariTutariVar
          ? parseInt(form.teklifFiyatlari.basariTutari) || 0
          : 0;
        toplam += seciliUrun.sabitTutarVar
          ? (parseInt(form.teklifFiyatlari.sabitTutar) || 0) *
            (parseInt(form.teklifFiyatlari.sabitTutarAy) || 0)
          : 0;
        toplam += seciliUrun.raporBasiOdemeTutariVar ? parseInt(fiyat) || 0 : 0;
        break;
    }

    araToplam += seciliUrun.onOdemeTutariVar ? seciliUrun.onOdemeTutari : 0;
    araToplam += seciliUrun.basariTutariVar ? seciliUrun.basariTutari : 0;
    araToplam += seciliUrun.sabitTutarVar ? seciliUrun.sabitTutar * ay || 0 : 0;
    araToplam += seciliUrun.raporBasiOdemeTutariVar
      ? seciliUrun.raporBasiOdemeTutari
      : 0;

    var _form = { ...form };
    _form.teklifFiyatlari.araToplam = araToplam;
    _form.teklifFiyatlari.indirim = Math.max(araToplam - toplam, 0);
    _form.teklifFiyatlari.kdvOran = seciliUrun.kdvOrani;
    _form.teklifFiyatlari.kdv = (seciliUrun.kdvOrani * toplam) / 100;
    _form.teklifFiyatlari.toplam = toplam;

    if (araToplam - toplam !== 0) {
      _form.teklifFiyatlari.fiyatDegisikligiVar = true;
    } else {
      _form.teklifFiyatlari.fiyatDegisikligiVar = false;
    }

    if (araToplam - toplam > ((user.indirimOrani || 0) * toplam) / 100) {
      _form.onay = false;
    } else {
      _form.onay = true;
    }
    setForm(_form);
  };

  const kaydet = () => {
    const token = cookie.get("token");

    fetch(`${baseUrl}/api/satisfirsati`, {
      method: "POST",
      headers: {
        authorization: token,
      },
      body: JSON.stringify(form),
    });
  };

  const canSubmit = () => {
    return form.seciliUrun && form.musteri;
  };

  return (
    <div>
      <Card className="w-full  border-t-4 border-blue-500">
        <CardHeader
          title={<Typography variant="h6">Teklif</Typography>}
          subheader="Müşteriye yapılacak ürün teklifi"
          avatar={<ShoppingCartIcon fontSize="large" />}
        />
        <Divider />
        <CardContent>
          <div className="my-6">
            <TextField
              label="Musteri"
              name="musteri"
              variant="outlined"
              select
              fullWidth
              value={form.musteri}
              onChange={handleChange}
              InputProps={{
                readOnly: !yeni,
              }}
            >
              {musteriler &&
                musteriler.data &&
                musteriler.data.map((m) => (
                  <MenuItem key={m._id} value={m._id}>
                    {m.marka}
                  </MenuItem>
                ))}
            </TextField>
          </div>
          <div className="my-6">
            <TextField
              label="Urun"
              name="seciliUrun"
              variant="outlined"
              onChange={onUrunSelected}
              value={form.seciliUrun}
              fullWidth
              select
              InputProps={{
                readOnly: !yeni,
              }}
            >
              {urunler &&
                urunler.data &&
                urunler.data.map((u) => (
                  <MenuItem key={u._id} value={u._id}>
                    {u.adi}
                  </MenuItem>
                ))}
            </TextField>
          </div>
          {seciliUrun && seciliUrun.onOdemeTutariVar && (
            <div className="my-6">
              <TextField
                label="On Odeme Tutari"
                variant="outlined"
                fullWidth
                helperText={`Ürün fiyatı ${seciliUrun.onOdemeTutari} TL`}
                id="teklifFiyatlari.onOdemeTutari"
                name="teklifFiyatlari.onOdemeTutari"
                value={form.teklifFiyatlari.onOdemeTutari}
                InputProps={{
                  readOnly: !yeni,
                }}
                onChange={(e) => {
                  if (e.target.value.match(/^(\s*|\d+)$/)) {
                    onFiyatDegisti(e);
                  }
                }}
              />
            </div>
          )}
          {seciliUrun && seciliUrun.basariTutariVar && (
            <div className="my-6">
              <TextField
                variant="outlined"
                helperText={`Ürün fiyatı ${seciliUrun.basariTutari} TL`}
                required
                label="Başarı Primi"
                id="teklifFiyatlari.basariTutari"
                name="teklifFiyatlari.basariTutari"
                value={form.teklifFiyatlari.basariTutari}
                InputProps={{
                  readOnly: !yeni,
                }}
                onChange={(e) => {
                  if (e.target.value.match(/^(\s*|\d+)$/)) {
                    onFiyatDegisti(e);
                  }
                }}
                fullWidth
              />
            </div>
          )}
          {seciliUrun && seciliUrun.sabitTutarVar && (
            <div className="flex my-6">
              <TextField
                variant="outlined"
                fullWidth
                helperText={`Ürün fiyatı ${seciliUrun.sabitTutar} TL`}
                required
                label="Sabit Ödeme"
                id="teklifFiyatlari.sabitTutar"
                name="teklifFiyatlari.sabitTutar"
                value={form.teklifFiyatlari.sabitTutar}
                InputProps={{
                  readOnly: !yeni,
                }}
                onChange={(e) => {
                  if (e.target.value.match(/^(\s*|\d+)$/)) {
                    onFiyatDegisti(e);
                  }
                }}
              />
              <div className="ml-4">
                <TextField
                  label="Ay"
                  variant="outlined"
                  fullWidth
                  id="teklifFiyatlari.sabitTutarAy"
                  name="teklifFiyatlari.sabitTutarAy"
                  value={form.teklifFiyatlari.sabitTutarAy}
                  InputProps={{
                    readOnly: !yeni,
                  }}
                  onChange={(e) => {
                    if (e.target.value.match(/^(\s*|\d+)$/)) {
                      onFiyatDegisti(e);
                    }
                  }}
                />
              </div>
            </div>
          )}
          {seciliUrun && seciliUrun.raporBasiOdemeTutariVar && (
            <div className="my-6">
              <TextField
                variant="outlined"
                fullWidth
                helperText={`Ürün fiyatı ${seciliUrun.raporBasiOdemeTutari} TL`}
                required
                label="Rapor Başına Ödeme"
                id="teklifFiyatlari.raporBasiOdemeTutari"
                name="teklifFiyatlari.raporBasiOdemeTutari"
                value={form.teklifFiyatlari.raporBasiOdemeTutari}
                InputProps={{
                  readOnly: !yeni,
                }}
                onChange={(e) => {
                  if (e.target.value.match(/^(\s*|\d+)$/)) {
                    onFiyatDegisti(e);
                  }
                }}
              />
            </div>
          )}
          {seciliUrun && seciliUrun.yuzdeTutariVar && (
            <div className="my-6">
              <TextField
                variant="outlined"
                helperText={`Ürün yüzdesi %${seciliUrun.yuzdeTutari}`}
                required
                label="Yüzde"
                id="teklifFiyatlari.yuzdeTutari"
                name="teklifFiyatlari.yuzdeTutari"
                value={form.teklifFiyatlari.yuzdeTutari}
                InputProps={{
                  readOnly: !yeni,
                }}
                onChange={(e) => {
                  if (e.target.value.match(/^(\s*|\d+)$/)) {
                    handleChange(e);
                  }
                }}
                fullWidth
              />
            </div>
          )}{" "}
          <Divider />
          {seciliUrun && (
            <SatisFirsatiDipToplam toplam={form.teklifFiyatlari} />
          )}
        </CardContent>
        <Divider />
        <CardActions>
          {yeni && user.yetkiler.create_satisfirsati && (
            <Button
              variant="contained"
              color="primary"
              disabled={!canSubmit()}
              onClick={kaydet}
            >
              Kaydet
            </Button>
          )}
        </CardActions>
      </Card>
    </div>
  );
};

SatisFirsati.getInitialProps = async ({ Component, ctx }) => {
  let pageProps = {};
  const { token } = parseCookies(ctx);
  if (Component.getInitialProps) {
    pageProps = await Component.getInitialProps(ctx);
  }
  pageProps.token = token;
  return { pageProps };
};

export default SatisFirsati;
