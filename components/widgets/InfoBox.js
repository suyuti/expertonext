import React from "react";
import styled from "styled-components";
import { LinearProgress, makeStyles } from "@material-ui/core";
import useSWR from "swr";
import baseUrl from "../../utils/baseUrl";
import axios from "axios";

const StyledContainer = styled.div`
  border: ${(props) => `1px solid #E0E0E0`};
  border-radius: 4px;
  padding: 5px 20px 20px 20px;
  width: 100%;
  background-color: #e8f4f9;
`;
const Title = styled.h2`
  color: #000;
  font-weight: 400;
  font-size: 1.2rem;
  @media (max-width: 500px) {
    font-size: 1rem;
  }
`;
const Value = styled.h2`
  color: #0e3cf4;
  font-weight: 700;
  font-size: 3rem;
  @media (max-width: 500px) {
    font-size: 1rem;
  }
`;
const Value2 = styled.h2`
  color: #306982;
  font-weight: 300;
  font-size: 1.4rem;
  @media (max-width: 500px) {
    font-size: 1rem;
  }
`;


const useStyles = makeStyles(theme => ({
  not: {
    fontSize: 12,
    marginTop: 8,
  }
}))

const InfoBox = (props) => {
  const {token, title, value} = props
  const classes = useStyles()

  const {data, error} = useSWR(`${baseUrl}/api/me/satisinfo`, (url) => 
    axios.get(url, {headers: {authorization: token}}).then(resp => resp.data.data))
    if (!data) {
      return <></>
    }

  return (
    <StyledContainer>
      <Title>{title}</Title>
      <Value>{data.satisHedefi}</Value>
      <Value2>{data.gerceklesenSatis}</Value2>
      <LinearProgress variant="determinate" value={data.tamamlanmaOrani} />
      <p className={classes.not}>Son 3 ay</p>
    </StyledContainer>
  );
};

export default InfoBox;
