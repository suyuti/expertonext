import React from "react";
import {
  Paper,
  CardHeader,
  Avatar,
  Typography,
  IconButton,
  TableBody,
  TableRow,
  Table,
  TableCell,
  makeStyles,
  Menu,
  MenuItem,
  Box,
  Divider,
} from "@material-ui/core";
import { Chart } from "react-google-charts";
import MoreVertIcon from "@material-ui/icons/MoreVert";
import useSWR from "swr";
import baseUrl from "../../../utils/baseUrl";
import axios from "axios";

const useStyles = makeStyles((theme) => ({
  personelAdi: {
    fontSize: 28,
    marginLeft: 8,
  },
  hedefTitle: {
    fontSize: 18,
  },
  gerceklesen: {
    fontSize: 42,
    fontWeight: 400,
  },
  hedef: {
    fontSize: 24,
    fontWeight: 100,
  },
  toplamRakamlar: {
    fontSize: 42,
    fontWeight: 500,
  },
}));

const EkipListItem = (props) => {
  const classes = useStyles();
  const { personel, token } = props;
  const [anchorEl, setAnchorEl] = React.useState(null);
  const menuopen = Boolean(anchorEl);

  const {data: performansData, error} = useSWR(`${baseUrl}/api/widget/satisperformans/${personel._id}?ay=3`,
    (url) => 
      axios.get(url, {headers: {authorization: token}})
      .then(resp => resp.data.data)
  )

  if (!performansData) {
    return <></>
  }

  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <Paper className="w-full">
      <div className="flex-col w-full p-8">
        <div className="flex items-center justify-between pb-8">
          <div className="flex">
            <Avatar></Avatar>
            <Typography
              className={classes.personelAdi}
            >{`${personel.adi} ${personel.soyadi}`}</Typography>
          </div>
          <IconButton onClick={handleMenu}>
            <MoreVertIcon />
          </IconButton>
          <Menu
            id="menu-per"
            anchorEl={anchorEl}
            anchorOrigin={{
              vertical: "top",
              horizontal: "right",
            }}
            keepMounted
            transformOrigin={{
              vertical: "top",
              horizontal: "right",
            }}
            open={menuopen}
            onClose={handleClose}
          >
            <MenuItem
            //component={Link}
            //href='/profil'
            >
              Satış Hedefi Ver
            </MenuItem>
            <MenuItem
            //component={Link}
            //href='/profil'
            >
              Görüşme Hedefi Ver
            </MenuItem>
            <Divider />
          </Menu>
        </div>
        <Divider />
        <div className="flex m-4 mt-8">
          <div className="flex-col">
            <Chart
              width={"400px"}
              height={"200px"}
              chartType="Bar"
              loader={<div>Loading Chart</div>}
              data={[
                ["Ay", "İlk Fatura", 'Sözleşme Toplamı'],
               // ["Mayis", 21543, 16771], 
               // ["Haziran", 11000, 5500]
                ...performansData.satislar
              ]}
              options={{
                // Material design options
                chart: {
                  //title: "Performans",
                  //subtitle: "Son 3 aylık satış performansı",
                },
              }}
              // For tests
              rootProps={{ "data-testid": "2" }}
            />
            <Chart
              width={"400px"}
              height={"200px"}
              chartType="Bar"
              loader={<div>Loading Chart</div>}
              data={[
                ["Ay", "Telefon", "Toplanti", 'Mail'],
                ...performansData.gorusmeler
              ]}
              options={{
                // Material design options
                chart: {
                  //title: "Performans",
                  //subtitle: "Son 3 aylık görüşme performansı",
                },
              }}
              // For tests
              rootProps={{ "data-testid": "2" }}
            />
          </div>
          <div className="flex-col w-full">
            <div className="flex-col border-1 p-4 m-4">
              <Typography className={classes.hedefTitle}>
                Satış Hedefi
              </Typography>
              <div className="flex justify-end items-end">
            <Typography className={classes.gerceklesen}>{performansData.gerceklesenSatis}</Typography>
            <Typography className={classes.hedef}>/{performansData.satisHedefi}</Typography>
              </div>
            </div>

            <div className="flex-col border-1 p-4 m-4">
              <Typography className={classes.hedefTitle}>
                Görüşme Hedefi
              </Typography>
              <div className="flex justify-end items-end">
            <Typography className={classes.gerceklesen}>{performansData.gerceklesenGorusme}</Typography>
                <Typography className={classes.hedef}>/{performansData.gorusmeHedefi}</Typography>
              </div>
            </div>
          </div>
        </div>
        <Divider />
        <div className="flex justify-around m-4">
          <Paper className="p-8 sm:w-1/4 m-4">
            <div className="flex-col text-center">
              <Typography>Müşterileri</Typography>
            <Typography className={classes.toplamRakamlar}>{performansData.toplamlar.musteri}</Typography>
            </div>
          </Paper>
          <Paper className="p-8 sm:w-1/4 m-4">
            <div className="flex-col text-center">
              <Typography>Görüşmeleri</Typography>
              <Typography className={classes.toplamRakamlar}>{performansData.toplamlar.gorusmeler}</Typography>
            </div>
          </Paper>
          <Paper className="p-8 sm:w-1/4 m-4">
            <div className="flex-col text-center">
              <Typography>Satışları</Typography>
              <Typography className={classes.toplamRakamlar}>{performansData.toplamlar.satisAdedi}</Typography>
            </div>
          </Paper>
          <Paper className="p-8 sm:w-1/4 m-4">
            <div className="flex-col text-center">
              <Typography>Teklifleri</Typography>
              <Typography className={classes.toplamRakamlar}>{performansData.toplamlar.teklifAdedi}</Typography>
            </div>
          </Paper>
        </div>
      </div>
    </Paper>
  );
};

export default EkipListItem;
