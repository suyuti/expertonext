import React from "react";
import EkipListItem from "./EkipListItem";
import useSWR from "swr";
import baseUrl from "../../../utils/baseUrl";
import axios from "axios";

const EkipList = (props) => {
  const { token } = props;

  const { data: ekip, error } = useSWR(`${baseUrl}/api/ekip`, (url) => 
    axios
      .get(url, { headers: { authorization: token } })
      .then((resp) => resp.data)
  );

  if (!ekip) {
      return <></>
  }


  return (
    <div className="flex-wrap w-full">
      {ekip &&
        ekip.data.map((p) => {
            if (p.role === 'satis-personel') {
                return (
                    <div className="sm:w-1/2 m-4 shadow border-t-4 border-red-500">
                    <EkipListItem personel={p} {...props}/>
                  </div>
                        )
            }
        })}
    </div>
  );
};

export default EkipList;
