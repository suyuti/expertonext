import React from 'react'
import { Paper, Typography, Divider, Icon, IconButton } from '@material-ui/core'

const SatisHedefi = props => {
    const {user} = props
    return (
		<Paper className="w-full rounded-8 shadow-none border-1">
			<div className="flex items-center justify-between px-4 pt-4">
				<div className="flex items-center px-12">
					<Icon color="action">location_on</Icon>
					<Typography className="text-16 mx-8">
						loc
					</Typography>
				</div>
				<IconButton aria-label="more">
					<Icon>more_vert</Icon>
				</IconButton>
			</div>
			<div className="flex items-center justify-center p-16 pb-32">
				<Icon className="meteocons text-40 ltr:mr-8 rtl:ml-8" color="action">
					
				</Icon>
				<Typography className="text-44 mx-8" color="textSecondary">
					36
				</Typography>
				<Typography className="text-48 font-300" color="textSecondary">
					°
				</Typography>
				<Typography className="text-44 font-300" color="textSecondary">
					C
				</Typography>
			</div>
			<Divider />
        </Paper>
    )
}


export default SatisHedefi