var PizZip = require('pizzip');
var Docxtemplater = require('docxtemplater');
var fs = require('fs');
var path = require('path');


const PrepareTeklifDoc = async (teklif) => {
    ////console.log(teklif)
    try {
        //var fileName = `./templateFolder/${teklif.urunDetay.kisaKod}.docx`
        var fileName = `./templateFolder/ftms.docx`
        var content = fs.readFileSync(fileName, 'binary');
        var zip = new PizZip(content);
        var doc = new Docxtemplater(zip)
        doc.setData({
            firma_unvani: 'Musterim benim',//sf.Musteri.Unvani,
            firma_adres: 'Ikitelli',
            firma_marka: 'Bu da markasi'//sf.Musteri.Markasi,
        })
        var buff = doc.render().getZip().generate({
            type: 'nodebuffer',
        })
        fs.writeFileSync(`./dokumanlar/${teklif.teklifId}.docx`, buff)
    }
    catch(e) {
        ////console.log(e)
    }

    //res.end(buff)

}

module.exports = {
    PrepareTeklifDoc
}