import React from 'react'
import { makeStyles } from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
    root: {},
    header: {
        height: 140,
        backgroundColor: 'red',
    }
}))

const Page = (props) => {
    const classes = useStyles()
    return (
        <div className='flex-col w-full'>
            {props.header}
            <div className='p-8'>
                {props.content}
            </div>
        </div>
    )
}

export default Page