import getConfig from 'next/config'

const {publicRuntimeConfig} = getConfig()
const {API_URL} = publicRuntimeConfig

//console.log(process.env.NODE_ENV )

const baseUrl = API_URL
console.log(baseUrl)

/*const baseUrl =
  process.env.NODE_ENV === "production"
  ? 'http://206.189.198.51:3000' 
  : `${process.env.HOST}:${process.env.PORT}`
  //: "http://localhost:3000";
*/
export default baseUrl;
