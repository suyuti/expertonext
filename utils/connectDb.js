import mongoose from "mongoose";

const connection = {};

async function connectDb() {
  if (connection.isConnected) {
    ////console.log("Using existing connection");
    return;
  }
  const db = await mongoose.connect(`mongodb://${process.env.DB_USER}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_DATABASE}`, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
  });
  //console.log("DB Connected");
  connection.isConnected = db.connections[0].readyState;
}

export default connectDb;
