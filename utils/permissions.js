export const LIST_MUSTERI = 'list.musteri'

function can(yetkiler, user, entity, action) {
    if (user.role === 'admin') {
        return true
    }

    if (yetkiler.yetkiler[`${entity}`] === undefined) {
        return false
    }

    return yetkiler.yetkiler[`${entity}`].includes(action)
}

const yetkilendir = async (user, yetkiler) => {
    user.yetkiler = {}
    Object.keys(yetkiler.yetkiler).map((k, i) => {
        Object.keys(yetkiler.yetkiler[`${k}`]).map((a, j) => {
            user.yetkiler[`${yetkiler.yetkiler[k][a]}_${k}`] = true
        })
    })
}

module.exports = {can, yetkilendir}