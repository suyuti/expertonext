const OIDCStrategy = require('passport-azure-ad').OIDCStrategy;
const config = require('../../config')

passport.use(new OIDCStrategy(config.creds, (iss, sub, profile, accessToken, refreshToken, params, done) => {
    done(null, {
        profile,
        accessToken,
        refreshToken,
        params
    });
  }));

  module.exports = {
      profile
  }