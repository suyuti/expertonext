# Next.js example

## How to use

Download the example [or clone the repo](https://github.com/mui-org/material-ui):

```sh
curl https://codeload.github.com/mui-org/material-ui/tar.gz/master | tar -xz --strip=2  material-ui-master/examples/nextjs
cd nextjs
```

Install it and run:

```sh
npm install
npm run dev
```

or:

[![Edit on CodeSandbox](https://codesandbox.io/static/img/play-codesandbox.svg)](https://codesandbox.io/s/github/mui-org/material-ui/tree/master/examples/nextjs)

## The idea behind the example

[Next.js](https://github.com/zeit/next.js) is a framework for server-rendered React apps.





Satış Fırsatı Süreç

1.  Müşteri seç                                         --- OK
    Atanan personel seç. Default user gelir.            --- OK 
        Satış grubundaki kişiler listelenir.
    Not yazabilir                                       --- OK

    Atanan personel dashboardda YENI olarak listelenir  --- OK
    Yonetici dashboardda YENI olarak listelenebilir.

    Silinebilir.

    Aksiyonlar:
        Kaydet -> Durum: YENI


2.  Randevu alma
    Tarih ve yer belirtilerek randevu oluşturulur.
    Firma ve Experto katılımcıları eklenebilir.
    Not yazılabilir.

    Atanan Pessonel dashboardda RANDEVU olarak listelenir
    Yönetici dashboardda RANDEVU olarak listelenir

    Randevu EDIT edilebilir.
    İptal edilebilir. İptal notu alınır.

    Aksiyonlar:
        Randevu Al -> Durum: RANDEVU


3.  Teklif Hazırlama
    Birden fazla ürün seçilebilir.
    Ürünler için teklif fiyatları girilebilir.
    Toplam fiyat ve indirimler gösterilir
    Dip toplam ve indirimler gösterilir
    Teklif sepetindeki ürün çıkartılabilir.
    Personel indirim limiti içinde ise ONAYlanmış kabul edilir.
    Personel indirim limiti dışında ise ONAY beklenir.

4.  Onaylama
    Teklif onaya ihtiyaç duyuyorsa yönetici tarafından onaylanabilir.
    Düzeltme istenebilir.
        Not girilir
    İptal edilebilir.
        Not girilir

5.  Sonuçlandırma
    Teklif dokümanları müşteriye gönderilmiştir.
    Müşteriye gönderme tarihi alınır.
    Müşteri de onaylamışsa BAŞARILI kapatılır
        Sabit tutar atanan personelin Ciro Hedefi Gerçekleştirmesine yazılır
    Müşteri onaylamamışsa BAŞARISIZ kapatılır.
        Not alınır
    Fiyat düzeltme talep edilebilir.



//-----------------------------------------------------
KURULUM

nodejs 12
https://computingforgeeks.com/how-to-install-nodejs-on-ubuntu-debian-linux-mint/

sudo apt update
sudo apt -y upgrade
sudo apt update
sudo apt -y install curl dirmngr apt-transport-https lsb-release ca-certificates
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt -y install nodejs

yarn:
npm install -g yarn



mongo:
https://www.hostinger.web.tr/rehberler/ubuntu-uzerinde-mongodb-kurulumu/

sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4
echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.0.list
sudo apt update
sudo apt-get install -y mongodb
sudo systemctl enable mongodb
sudo systemctl start mongodb


mongo
use experto
db.createUser(
{
user: "gokhan",
pwd: "gokhan123",
roles: [ { role: "userAdminAnyDatabase", db: "admin" } ]
}
)

// roles: [ { role: "readWrite", db: "admin" } ]

sudo nano /etc/mongodb.conf
bind_ip: 0.0.0.0

sudo systemctl restart mongodb


//------------------------------------------------------------------------
// 20200617
206.189.198.51
mongo:
 db: experto

pm2 start npm --name 'experto' -- start

cronjob: 00 01 * * * 
/root/backup/ altına mongodump alır


