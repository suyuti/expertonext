import React, { useState } from "react";
import {
  Typography,
  Paper,
  TextField,
  Avatar,
  Tabs,
  Tab,
  MenuItem,
  Button,
  Divider,
} from "@material-ui/core";
import { DatePicker, KeyboardDatePicker } from "@material-ui/pickers";
import axios from "axios";
import { parseCookies, destroyCookie } from "nookies";
import baseUrl from "../../utils/baseUrl";

const UserForm = (props) => {
  /*
        Adi
        Soyadi
        Cinsiyet
        TCKN
        Dogum yeri
        Dogum Tarihi
        Medeni hali
        Iletisim
            adres
            telefon
            mail
        Egitim Bilgileri
            okul
            Bolum
            yil
            derece

    */
  const [form, setForm] = useState(props.userDetail || {});
  const handleChange = (e) => {
    var f = { ...form };
    f[e.target.name] = e.target.value;
    setForm(f);
  };
  const [selectedDate, handleDateChange] = useState(new Date());
  const { token } = props;

  const save = async () => {
    axios.put(`${baseUrl}/api/me`, form, { headers: { authorization: token } });
  };

  return (
    <div className="flex-col w-1/2 m-8">
      <div className="flex">
        <div className="m-4 w-full">
          <TextField
            variant="outlined"
            onChange={handleChange}
            fullWidth
            label="Adi"
            name="adi"
            value={form.adi}
          />
        </div>
        <div className="m-4 w-full">
          <TextField
            variant="outlined"
            onChange={handleChange}
            fullWidth
            label="Soyadi"
            name="soyadi"
            value={form.soyadi}
          />
        </div>
      </div>
      <div className="flex">
        <div className="m-4 w-full">
          <TextField
            variant="outlined"
            onChange={handleChange}
            fullWidth
            label="Cinsiyet"
            name="cinsiyet"
            value={form.cinsiyet}
            select
          >
            <MenuItem key="Erkek" value="Erkek">
              Erkek
            </MenuItem>
            <MenuItem key="Kadın" value="Kadın">
              Kadın
            </MenuItem>
          </TextField>
        </div>
        <div className="m-4 w-full">
          <TextField
            variant="outlined"
            onChange={handleChange}
            fullWidth
            label="TCKN"
            name="tckn"
            value={form.tckn}
          />
        </div>
      </div>
      <div className="flex w-full">
        <div className="m-4 w-full">
          <TextField
            variant="outlined"
            onChange={handleChange}
            fullWidth
            label="Doğum yeri"
            name="dogumYeri"
            value={form.dogumYeri}
          />
        </div>
        <div className="m-4 w-full">
          <KeyboardDatePicker
            autoOk
            fullWidth
            variant="inline"
            inputVariant="outlined"
            label="Doğum tarihi"
            format="dd/MM/yyyy"
            value={selectedDate}
            InputAdornmentProps={{ position: "end" }}
            onChange={(date) => handleDateChange(date)}
          />
        </div>
      </div>
      <div className="m-4">
        <TextField
          variant="outlined"
          onChange={handleChange}
          fullWidth
          label="Medeni Hali"
          disabled
          name="medeniHali"
          value={form.medeniHali}
        />
      </div>
      <div className="m-4">
        <TextField
          variant="outlined"
          onChange={handleChange}
          fullWidth
          label="Ev adresi"
          name="evAdresi"
          value={form.evAdresi}
          multiline
          rows={3}
        />
      </div>
      <div className="m-4">
        <TextField
          variant="outlined"
          onChange={handleChange}
          fullWidth
          label="Telefonu"
          name="telefonu"
          value={form.telefonu}
        />
      </div>

      <div className="m-4">
        <TextField
          variant="outlined"
          onChange={handleChange}
          fullWidth
          label="Görünür adı"
          name="gorunurAdi"
          value={form.gorunurAdi}
        />
      </div>

      <div className="m-4">
        <Button variant="contained" color="primary" onClick={save}>
          Kaydet
        </Button>
      </div>
    </div>
  );
};

const Profile = (props) => {
  const { user } = props;
  const [selectedTab, setSelectedTab] = useState(0);

  return (
    <div className="flex-col w-full mt-8 ml-4">
      <Typography variant="h4">Profil</Typography>
      <div className="flex m-8">
        <div className="flex-col sm:w-1/4">
          <Paper className="m-8 p-12 text-begin justify-center">
            <div className="flex w-full justify-center p-12">
              <Avatar></Avatar>
            </div>
            <Divider />
            <div className="flex justify-between m-8">
              <Typography>Rol</Typography>
              <Typography>...</Typography>
            </div>
            <Divider />
            <div className="flex justify-between m-8">
              <Typography>Yöneticim</Typography>
              <Typography>...</Typography>
            </div>
          </Paper>
        </div>
        <div className="flex-col w-full">
          <Paper className="m-8 p-8">
            <Tabs value={selectedTab} onChange={(e, n) => setSelectedTab(n)}>
              <Tab label="Hedefler ve Başarım" index={0}></Tab>
              <Tab label="Aktivite" index={1}></Tab>
              <Tab label="Ayarlar" index={2}></Tab>
            </Tabs>
            {selectedTab === 0 && (
              <div className="flex">
                <Typography>...</Typography>
              </div>
            )}
            {selectedTab === 1 && (
              <div className="flex">
                <Typography>...</Typography>
              </div>
            )}
            {selectedTab === 2 && (
              <div className="flex-col pt-12">
                <Divider />
                <UserForm me={props.userDetails} {...props} />
                <Divider />
                <Button variant="contained" color="secondary" disabled>
                  Şifre Değiştir
                </Button>
              </div>
            )}
          </Paper>
        </div>
      </div>
    </div>
  );
};

Profile.getInitialProps = async (ctx) => {
  const { token } = parseCookies(ctx);
  const resp = await axios.get(`${baseUrl}/api/me`, {
    headers: { authorization: token },
  });
  //console.log(resp.data.data);
  return { userDetail: resp.data.data };
};

export default Profile;
