import React, { useState } from "react";
import {
  TextField,
  Button,
  CardHeader,
  Card,
  Divider,
  CardContent,
  CardActions,
  Typography,
  makeStyles,
} from "@material-ui/core";
import baseUrl from "../utils/baseUrl";
import { handleLogin } from "../utils/auth";
import Link from '../components/Link'

const INITIAL_USER = {
  email: "",
  password: "",
};

const useStyles = makeStyles(theme => ({
  page: {
    backgroundImage: `url('/images/slide1.jpg')`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    width: '100%',
    marginLeft: 'auto',
    marginRight: 'auto',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    display: 'flex'
  }
}))


const Login = (props) => {
  const [user, setUser] = useState(INITIAL_USER);
  const [registerForm, setRegisterForm] = useState({});
  const [error, setError] = useState(null)
  const [loginPage, setLoginPage] = useState(0)
  const classes = useStyles()

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const res = await fetch(`${baseUrl}/api/login`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(user),
      });
      const response = await res.text();
      if (res.status >= 299) {
        //catchErrors(response, setErrorMsg);
        return;
      }
      handleLogin(response);
    } catch (e) {
      ////console.log(e);
    }
  };

  const handleRegister = async (e) => {
    e.preventDefault();
    try {
      if (registerForm.password1 !== registerForm.password2) {
        setError('password')
        return
      }
      const res = await fetch(`${baseUrl}/api/register`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({email: registerForm.email, password: registerForm.password1}),
      });
      const response = await res.text();
      if (res.status >= 299) {
        //catchErrors(response, setErrorMsg);
        return;
      }
      handleLogin(response);
    } catch (e) {
      ////console.log(e);
    }
  }

  const handleChange = (e) => {
    var u = {...user}
    u[e.target.name] = e.target.value
    setUser(u)
  }

  const handleChangeRegister = (e) => {
    var u = {...registerForm}
    u[e.target.name] = e.target.value
    setRegisterForm(u)
  }

  return (
        <div
          className={classes.page} 
          //className="mx-auto h-full flex justify-center items-center bg-teal-100 w-full"
          >
            <div className="w-1/4">
                {loginPage === 0 && (
                    <div className="flex-col border-teal p-8 border-t-12 bg-white mb-6 rounded shadow-lg">
                        <Typography className='p-8 text-center' variant='h5'>Experto Giriş</Typography>
                        <div className='px-8 mb-4'>
                            <TextField 
                                className='p-1'
                                label='Email veya Kullanıcı adı'
                                name='email'
                                value={user.email}
                                fullWidth
                                variant='outlined'
                                autoFocus
                                onChange={handleChange}
                            />
                        </div>
                        <div className='p-8 mb-8'>
                            <TextField 
                                className='p-8 mb-6'
                                label='Şifre'
                                name='password'
                                type='password'
                                value={user.password}
                                onChange={handleChange}
                                fullWidth
                                variant='outlined'
                            />
                        </div>
                        <div className='m-8 flex justify-between items-center'>
                          <Button className='ml-4 justify-center' variant='contained' color='primary' onClick={handleSubmit}>Giriş</Button>
                          <Button className='ml-4 justify-center' variant='outlined' color='primary' onClick={() => setLoginPage(1)}>Kayıt ol</Button>
                        </div>
                    </div>
                )}
                {loginPage === 1 && (
                    <div className="flex-col border-teal p-8 border-t-12 bg-white mb-6 rounded shadow-lg">
                        <Typography className='p-8 text-center' variant='h5'>Experto Kayıt</Typography>
                        <div className='px-8 mb-4'>
                            <TextField 
                                className='p-1'
                                label='Email veya Kullanıcı adı'
                                name='email'
                                value={registerForm.email}
                                fullWidth
                                variant='outlined'
                                onChange={handleChangeRegister}
                                autoFocus
                            />
                        </div>
                        <div className='p-8 mb-8'>
                            <TextField 
                                error={error}
                                className='p-8 mb-6'
                                name='password1'
                                value={registerForm.password1}
                                label='Şifre'
                                onChange={handleChangeRegister}
                                fullWidth
                                type='password'
                                variant='outlined'
                            />
                        </div>
                        <div className='px-8'>
                            <TextField 
                                error={error}
                                className='p-8 mb-6'
                                onChange={handleChangeRegister}
                                label='Şifre Tekrar'
                                name='password2'
                                type='password'
                                value={registerForm.password2}
                                fullWidth
                                variant='outlined'
                            />
                        </div>
                        <div className='m-8 flex justify-between items-center'>
                          <Button className='ml-4 justify-center' variant='contained' color='primary' onClick={handleRegister}>Kayıt</Button>
                          <Button className='ml-4 justify-center' variant='outlined' color='primary' onClick={() => setLoginPage(0)}>Giriş yap</Button>

                        </div>
                    </div>
                )}
            </div>
        </div>  )

  /*
  return (
    <div className="flex justify-center items-center ">
      <Card>
        <CardHeader title="LOGIN" />
        <Divider />
        <CardContent>
          <div className="flex-col">
            <div className="flex my-8">
              <TextField 
                className='m-8'
                label="username" 
                variant="outlined" 
                autoFocus 
              />
            </div>
            <div className="">
              <TextField 
                className='m-8'
                label="password" 
                variant="outlined" 
                />
            </div>
          </div>
        </CardContent>
        <CardActions>
          <Button variant="contained" color="primary" onClick={handleSubmit}>
            Login
          </Button>
        </CardActions>
      </Card>
    </div>
  );
  */
};

export default Login;
