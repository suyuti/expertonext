import "../styles.css";
import React from "react";
import Head from "next/head";
import Layout2 from "../components/shared/Layout2";
import { parseCookies, destroyCookie } from "nookies";
import { redirectUser } from "../utils/auth";
import baseUrl from "../utils/baseUrl";
import { Container } from "@material-ui/core";
import axios from "axios";
import * as Yetki from "../utils/permissions";
import DateFnsUtils from "@date-io/date-fns"; // choose your lib
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import trLocale from "date-fns/locale/tr";

const App = ({ Component, pageProps }) => {
  const { user } = pageProps;
  if (!user) {
    return (
      <React.Fragment>
        <Head>
          <title>Experto</title>
        </Head>
        <div className="bg-grey-lighter h-screen font-sans w-full">
          <Component {...pageProps} />
        </div>
      </React.Fragment>
    );
  }
  return (
    <React.Fragment>
      <Head>
        <title>Experto</title>
      </Head>
      <div className="flex-col">
        <MuiPickersUtilsProvider utils={DateFnsUtils} locale={trLocale}>
          <Layout2 {...pageProps}>
            <Component {...pageProps} />
          </Layout2>
        </MuiPickersUtilsProvider>
      </div>
    </React.Fragment>
  );
};

App.getInitialProps = async ({ Component, ctx }) => {
  let pageProps = {};
  const { token } = parseCookies(ctx);

  if (Component.getInitialProps) {
    pageProps = await Component.getInitialProps(ctx);
  }

  if (!token) {
    if (ctx.pathname !== "/login") {
      redirectUser(ctx, "/login");
    }
  } 
  else {
    try {
      const res = await fetch(`${baseUrl}/api/account`, {
        method: "GET",
        headers: {
          authorization: token,
        },
      });
      const user = await res.json();
      if (!user.aktif) {
        if (ctx.pathname !== '/yeni') {
          redirectUser(ctx, '/yeni')
        }
        return {pageProps}
      }
      pageProps.user = user;

      //console.log(user)
      const yetkiler = await axios.get(`${baseUrl}/api/yetki`, {
        params: { filter: { rol: user.role } },
        headers: {authorization: token}
      });

      //console.log(yetkiler)

      pageProps.yetkiler = yetkiler.data.data[0];
      pageProps.token = token;

      //console.log(yetkiler)

      Yetki.yetkilendir(user, yetkiler.data.data[0]);



    } catch (e) {
      destroyCookie(ctx, "token");
      redirectUser(ctx, "/login");
    }
  }
  return { pageProps };
};

export default App;
