import React from 'react'
import Page from '../src/Page'
import { Typography } from '@material-ui/core'

const ActiovationRequired = (props) => {
    return (
        <Page
        content={
            <Typography>Hesap aktifleştirilmemiş.</Typography>
        }
        />
    )
}

export default ActiovationRequired