import connectDb from "../../../utils/connectDb"
import Kisi from "../../../models/kisi"
import User from "../../../models/user"
import Yetki from "../../../models/yetki"
import jwt from "jsonwebtoken";

connectDb()

export default async (req, res) => {
    switch (req.method) {
      case "GET":
        await getKisiler(req, res);
        break;
        case "POST":
          await createKisi(req, res);
          break;
        default:
        res.setHeader("Allow", ["GET", 'POST']);
        res.status(405).end(`Method ${req.method} Not Allowed`);
        break;
    }
  };

  //---------------------------------------------------------------------------------

  async function getKisiler(req, res) {
    if (!("authorization" in req.headers)) {
      return res.status(401).send("No authorization token");
    }
    try {
      const { userId } = jwt.verify(
        req.headers.authorization,
        process.env.JWT_SECRET
      );
  
      const user = await User.findOne({ _id: userId });
      if (!user) {
        return res.status(401).send("No user");
      }
  
      const yetki = await Yetki.findOne({ rol: user.role });
      if (!yetki) {
        return res.status(401).send("rol icin yetki kaydi yok");
      }
  
      if (yetki.yetkiler.kisi && yetki.yetkiler.kisi.includes("list")) {
        let filters = req.query.filter ? JSON.parse(req.query.filter) : {};
        const k = await Kisi.find(filters).populate('musteri', ['marka'])
        return res.status(200).json({ data: k });
      } else {
        return res.status(401).send("yetkiniz yok");
      }
    } catch (e) {
      ////console.log(e);
      return res.status(401).json(e);
    }
  }
  
  //---------------------------------------------------------------------------------
  
  async function createKisi(req, res) {
    if (!("authorization" in req.headers)) {
      return res.status(401).send("No authorization token");
    }
    try {
      const { userId } = jwt.verify(
        req.headers.authorization,
        process.env.JWT_SECRET
      );
  
      const user = await User.findOne({ _id: userId });
      if (!user) {
        return res.status(401).send("No user");
      }
  
      const yetki = await Yetki.findOne({ rol: user.role });
      if (!yetki) {
        return res.status(401).send("rol icin yetki kaydi yok");
      }
  
      if (yetki.yetkiler.kisi && yetki.yetkiler.kisi.includes("create")) {
        var kisi = new Kisi(req.body)
        kisi.createdBy = user
        kisi.createdAt = new Date()
        let row = await kisi.save()
      return res.status(200).json({ data: row });
      } else {
        return res.status(401).send("yetkiniz yok");
      }
    } catch (e) {
      ////console.log(e);
      return res.status(401).json(e);
    }
  }
