import connectDb from "../../../utils/connectDb";
import User from "../../../models/user";
import Yetki from "../../../models/yetki";
import jwt from "jsonwebtoken";

connectDb();

export default async (req, res) => {
  switch (req.method) {
    case "GET":
      await getEkip(req, res);
      break;
    default:
      res.setHeader("Allow", ["GET"]);
      res.status(405).end(`Method ${req.method} Not Allowed`);
      break;
  }
};

async function getEkip(req, res) {
  if (!("authorization" in req.headers)) {
    return res.status(401).send("No authorization token");
  }
  try {
    const { userId } = jwt.verify(
      req.headers.authorization,
      process.env.JWT_SECRET
    );

    const user = await User.findOne({ _id: userId });
    if (!user) {
      return res.status(401).send("No user");
    }

    const yetki = await Yetki.findOne({ rol: user.role });
    if (!yetki) {
      return res.status(401).send("rol icin yetki kaydi yok");
    }

    if (yetki.yetkiler.personel && yetki.yetkiler.personel.includes("list")) {
        const ekip = await user.getAllChildren({})
        return res.status(200).json({data:ekip})
    } else {
      return res.status(401).send("yetkiniz yok");
    }
  } catch (e) {
    ////console.log(e);
    return res.status(401).json(e);
  }
}
