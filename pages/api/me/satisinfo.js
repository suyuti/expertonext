import connectDb from "../../../utils/connectDb";
import UserDetail from "../../../models/userDetails";
import User from "../../../models/user";
import Satis from "../../../models/satis";
import Yetki from "../../../models/yetki";
import jwt from "jsonwebtoken";
import mongoose from "mongoose";
import moment from "moment";

connectDb();

export default async (req, res) => {
  switch (req.method) {
    case "GET":
      await getUserSatisInfo(req, res);
      break;
    default:
      res.setHeader("Allow", ["GET"]);
      res.status(405).end(`Method ${req.method} Not Allowed`);
      break;
  }
};

async function getUserSatisInfo(req, res) {
  if (!("authorization" in req.headers)) {
    return res.status(401).send("No authorization token");
  }
  try {
    const { userId } = jwt.verify(
      req.headers.authorization,
      process.env.JWT_SECRET
    );

    const user = await User.findOne({ _id: userId });
    if (!user) {
      return res.status(401).send("No user");
    }

    const yetki = await Yetki.findOne({ rol: user.role });
    if (!yetki) {
      return res.status(401).send("rol icin yetki kaydi yok");
    }

    const userDetail = await UserDetail.findOne({ user: user._id });

    // -------
    const ay = 3; // TODO 3 ay query string ile gelmeli
    let baslangic = moment().subtract(ay, "months");
    let toplam = await Satis.aggregate(
      [{
        $match: {
          satisYapanPersonel: user._id,
          createdAt: { $gte: new Date(baslangic) },
        },
      },
      {
        $group: {
          _id: '$_id.satisYapanPersonel',
          toplamSatis: { $sum: "$ilkFaturaToplami" },
        },
      }]
    );

    var gerceklesen = 0
    var oran = 0
    if (toplam.length > 0) {
      gerceklesen = toplam[0].toplamSatis
      oran = (gerceklesen/userDetail.satisHedefi)*100
    }

    return res.status(200).json({
      data: {
        satisHedefi: userDetail.satisHedefi,
        gerceklesenSatis: gerceklesen,
        tamamlanmaOrani: oran,
      },
    });
  } catch (e) {
    console.log(e);
    return res.status(401).json(e);
  }
}
