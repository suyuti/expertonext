import connectDb from "../../../utils/connectDb";
import UserDetail from "../../../models/userDetails";
import User from '../../../models/user';
import Yetki from "../../../models/yetki";
import jwt from "jsonwebtoken";
import mongoose from "mongoose";

connectDb();

export default async (req, res) => {
  switch (req.method) {
    case "GET":
      await getUserDetail(req, res);
      break;
    case "PUT":
      await updateUserDetail(req, res);
      break;
    default:
      res.setHeader("Allow", ["GET", "PUT"]);
      res.status(405).end(`Method ${req.method} Not Allowed`);
      break;
  }
};

async function getUserDetail(req, res) {
  if (!("authorization" in req.headers)) {
    return res.status(401).send("No authorization token");
  }
  try {
    const { userId } = jwt.verify(
      req.headers.authorization,
      process.env.JWT_SECRET
    );

    const user = await User.findOne({ _id: userId });
    if (!user) {
      return res.status(401).send("No user");
    }

    const yetki = await Yetki.findOne({ rol: user.role });
    if (!yetki) {
      return res.status(401).send("rol icin yetki kaydi yok");
    }

    const userDetail = await UserDetail.findOne({ user: user._id });
    return res.status(200).json({ data: userDetail });

    /*if (yetki.yetkiler.personel && yetki.yetkiler.personel.includes("view")) {
          const personel = await User.findById(id);
          return res.status(200).json({ data: personel });
        } else {
          return res.status(401).send("yetkiniz yok");
        }*/
  } catch (e) {
    ////console.log(e);
    return res.status(401).json(e);
  }
}

async function updateUserDetail(req, res) {
    if (!("authorization" in req.headers)) {
        return res.status(401).send("No authorization token");
      }
      try {
        const { userId } = jwt.verify(
          req.headers.authorization,
          process.env.JWT_SECRET
        );
    
        const user = await User.findOne({ _id: userId });
        if (!user) {
          return res.status(401).send("No user");
        }
    
        const yetki = await Yetki.findOne({ rol: user.role });
        if (!yetki) {
          return res.status(401).send("rol icin yetki kaydi yok");
        }
    
        const userData = req.body
        var userDetail = await UserDetail.findOne({ user: user._id });
        if (!userDetail) {
            var ud = await new UserDetail({
                user: user._id,
                adi: userData.adi,
                soyadi: userData.soyadi,
                cinsiyet: userData.cinsiyet,
                dogumYeri: userData.dogumYeri,
                dogumTarihi: userData.dogumTarihi,
                evAdresi: userData.evAdresi,
                telefonu: userData.telefonu,
                tckn: userData.tckn
            }).save()
            return res.status(200).json({ data: ud });
        }
        else {
            userDetail.adi = userData.adi
            userDetail.soyadi = userData.soyadi
            userDetail.cinsiyet = userData.cinsiyet
            userDetail.dogumYeri = userData.dogumYeri
            userDetail.dogumTarihi = userData.dogumTarihi
            userDetail.evAdresi = userData.evAdresi
            userDetail.telefonu = userData.telefonu
            userDetail.tckn = userData.tck
        let r = await userDetail.save()            
            return res.status(200).json({ data: r });
        }
        
        
        return res.status(200).json({ data: userDetail });
    
        /*if (yetki.yetkiler.personel && yetki.yetkiler.personel.includes("view")) {
              const personel = await User.findById(id);
              return res.status(200).json({ data: personel });
            } else {
              return res.status(401).send("yetkiniz yok");
            }*/
      } catch (e) {
        ////console.log(e);
        return res.status(401).json(e);
      }
    }
