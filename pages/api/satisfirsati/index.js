import connectDb from "../../../utils/connectDb"
import User from "../../../models/user"
import SatisFirsati from "../../../models/satisFirsati"
import jwt from "jsonwebtoken";
import mongoose  from "mongoose";

connectDb()

export default async (req, res) => {
    switch (req.method) {
      case "GET":
        await handleGetReq(req, res);
        break;
      case "POST":
        await handlePostReq(req, res);
        break;
      default:
        res.setHeader("Allow", ["GET", "POST"]);
        res.status(405).end(`Method ${req.method} Not Allowed`);
        break;
    }
  };

  async function handleGetReq(req, res) {
    let filters = req.query.filter ? JSON.parse(req.query.filter) : {};
    const m = await SatisFirsati.find(filters).populate('musteri').populate('seciliUrun')
    res.status(200).json({ data: m });
  }
  

  async function handlePostReq(req, res)  {
    if (!("authorization" in req.headers)) {
        return res.status(401).send("No authorization token");
    }
    try {
        const { userId } = jwt.verify(
            req.headers.authorization,
            process.env.JWT_SECRET//process.env.JWT_SECRET
        );

        const user = User.findById(userId)
        if (!user) {
            return res.status(401).send("No user");
        }
        var form  = JSON.parse(req.body);
        form.createdBy = userId
        //form.createdBy =  new mongoose.Types.ObjectId(userId)
        delete form._id

        var sf = new SatisFirsati(form)
        await sf.save((e,s) => {
        })
        res.status(203).send("SatisFirsati created");
    }
    catch(e) {
        //console.log('error: ', e)
    }
    

  }

