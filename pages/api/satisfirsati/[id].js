import connectDb from "../../../utils/connectDb";
import SatisFirsati from "../../../models/satisFirsati";

connectDb();

export default async (req, res) => {
  switch (req.method) {
    case "GET":
      await handleGetReq(req, res);
      break;
    case "PUT":
      //await handlePutReq(req, res);
      break;
    default:
      res.setHeader("Allow", ["GET", "PUT"]);
      res.status(405).end(`Method ${req.method} Not Allowed`);
      break;
  }
};

async function handleGetReq(req, res) {
  const {
    query: { id },
  } = req;
  const sf = await SatisFirsati.findById(id);
  res.status(200).json({ data: sf });
}
