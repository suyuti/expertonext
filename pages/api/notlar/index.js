import connectDb from "../../../utils/connectDb";
import Urun from "../../../models/urun";
import Yetki from "../../../models/yetki";
import User from "../../../models/user";
import Gorev from "../../../models/gorev";
import jwt from "jsonwebtoken";

connectDb();

export default async (req, res) => {
  switch (req.method) {
    case "GET":
      await getNotlar(req, res);
      break;
    case "POST":
      await createNot(req, res);
      break;
    default:
      res.setHeader("Allow", ["GET", "POST"]);
      res.status(405).end(`Method ${req.method} Not Allowed`);
      break;
  }
};

async function getNotlar(req, res) {
    if (!("authorization" in req.headers)) {
      return res.status(401).send("No authorization token");
    }
    try {
      const { userId } = jwt.verify(
        req.headers.authorization,
        process.env.JWT_SECRET //process.env.JWT_SECRET
      );
  
      const user = await User.findOne({ _id: userId });
      if (!user) {
        return res.status(401).send("No user");
      }
  
      const yetki = await Yetki.findOne({ rol: user.role });
      if (!yetki) {
        return res.status(401).send("rol icin yetki kaydi yok");
      }
  
      if (yetki.yetkiler.urun && yetki.yetkiler.urun.includes("list")) {
        let filters = req.query.filter ? JSON.parse(req.query.filter) : {};
        const m = await Urun.find(filters)
        return res.status(200).json({ data: m });
      } else {
        return res.status(401).send("yetkiniz yok");
      }
    } catch (e) {
      ////console.log(e);
      return res.status(401).json(e);
    }
  }

  async function createNot(req, res) {
    if (!("authorization" in req.headers)) {
      return res.status(401).send("No authorization token");
    }
    try {
      const { userId } = jwt.verify(
        req.headers.authorization,
        process.env.JWT_SECRET //process.env.JWT_SECRET
      );
  
      const user = await User.findOne({ _id: userId });
      if (!user) {
        return res.status(401).send("No user");
      }
  
      const yetki = await Yetki.findOne({ rol: user.role });
      if (!yetki) {
        return res.status(401).send("rol icin yetki kaydi yok");
      }
  
      if (yetki.yetkiler.not && yetki.yetkiler.not.includes("create")) {
        const {relation, id, not} = req.body
        if (relation === 'gorev') {
            var g = await Gorev.findById(id)
            if (g) {
                not.yazar = user
                not.tarih = new Date()
                g.notlar.push(not)
                let r = await g.save()
                return res.status(200).json(r)
            }
        }
        return res.status(200).json({});
      } else {
        return res.status(401).send("yetkiniz yok");
      }
    } catch (e) {
      ////console.log(e);
      return res.status(401).json(e);
    }
  }
