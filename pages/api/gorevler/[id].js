import connectDb from "../../../utils/connectDb";
import Gorev from "../../../models/gorev";
import Yetki from "../../../models/yetki";
import User from "../../../models/user";
import jwt from "jsonwebtoken";

connectDb();

export default async (req, res) => {
  switch (req.method) {
    case "GET":
      await getGorev(req, res);
      break;
    case "PUT":
      await updateGorev(req, res);
      break;
    default:
      res.setHeader("Allow", ["GET", "PUT"]);
      res.status(405).end(`Method ${req.method} Not Allowed`);
      break;
  }
};

async function getGorev(req, res) {
  if (!("authorization" in req.headers)) {
    return res.status(401).send("No authorization token");
  }
  try {
    const { userId } = jwt.verify(
      req.headers.authorization,
      process.env.JWT_SECRET //process.env.JWT_SECRET
    );

    const user = await User.findOne({ _id: userId });
    if (!user) {
      return res.status(401).send("No user");
    }

    const yetki = await Yetki.findOne({ rol: user.role });
    if (!yetki) {
      return res.status(401).send("rol icin yetki kaydi yok");
    }

    if (yetki.yetkiler.gorev && yetki.yetkiler.gorev.includes("view")) {
      const {
        query: { id },
      } = req;

      if (id === "new") {
        return res.status(200).json({ data: {} });
      }
      //const g = await Gorev.findById(id)
      //  .populate({
      //    path: "notlar.yazar",
      //    model: "User",
      //  })
      //  .exec();
      const g = await Gorev.findByIdAndUpdate(id, {viewed: true, viewedAt: new Date()}, {new: true, lean: true})
        .populate({
          path: "notlar.yazar",
          model: "User",
        })
        //console.log(g)
      return res.status(200).json({ data: g });
    } else {
      return res.status(401).send("yetkiniz yok");
    }
  } catch (e) {
    ////console.log(e);
    return res.status(401).json(e);
  }
}

//-------------------------------------------------------------------------------------

async function updateGorev(req, res) {
  if (!("authorization" in req.headers)) {
    return res.status(401).send("No authorization token");
  }
  try {
    const { userId } = jwt.verify(
      req.headers.authorization,
      process.env.JWT_SECRET //process.env.JWT_SECRET
    );

    const user = await User.findOne({ _id: userId });
    if (!user) {
      return res.status(401).send("No user");
    }

    const yetki = await Yetki.findOne({ rol: user.role });
    if (!yetki) {
      return res.status(401).send("rol icin yetki kaydi yok");
    }

    if (yetki.yetkiler.gorev && yetki.yetkiler.gorev.includes("edit")) {
      const {
        query: { id },
      } = req;

      let m = await Gorev.findByIdAndUpdate(id, req.body, {
        new: true,
        lean: true,
      });

      return res.status(200).json({ data: m });
    } else {
      return res.status(401).send("yetkiniz yok");
    }
  } catch (e) {
    ////console.log(e);
    return res.status(401).json(e);
  }
}
