import connectDb from "../../../utils/connectDb"
import Urun from "../../../models/urun"
import Yetki from "../../../models/yetki";
import User from "../../../models/user";
import jwt from "jsonwebtoken";

connectDb()

export default async (req, res) => {
    switch (req.method) {
      case "GET":
        await getUrun(req, res);
        break;
      case "PUT":
        await updateUrun(req, res);
        break;
      case "DELETE":
        await removeUrun(req, res);
        break;
      default:
        res.setHeader("Allow", ["GET", "PUT", "REMOVE"]);
        res.status(405).end(`Method ${req.method} Not Allowed`);
        break;
    }
  };
  

  async function getUrun(req, res) {
    if (!("authorization" in req.headers)) {
      return res.status(401).send("No authorization token");
    }
    try {
      const { userId } = jwt.verify(
        req.headers.authorization,
        process.env.JWT_SECRET //process.env.JWT_SECRET
      );
  
      const user = await User.findOne({ _id: userId });
      if (!user) {
        return res.status(401).send("No user");
      }
  
      const yetki = await Yetki.findOne({ rol: user.role });
      if (!yetki) {
        return res.status(401).send("rol icin yetki kaydi yok");
      }
  
      if (
        yetki.yetkiler.urun &&
        yetki.yetkiler.urun.includes("view")
      ) {
        const {
          query: { id },
        } = req;
        if (id === 'new') {
            return res.status(200).json({ data: 
                {
                    onOdemeTutariVar: false,
                    onOdemeTutari: 0,
                    sabitTutarVar: false,
                    sabitTutar: 0,
                    basariTutariVar: false,
                    basariTutari: 0,
                    raporBasiOdemeTutariVar: false,
                    raporBasiOdemeTutari: 0,
                    kdvOrani: 18
                } });
        }
        const m = await Urun.findById(id)
        return res.status(200).json({ data: m });
      } else {
        return res.status(401).send("yetkiniz yok");
      }
    } catch (e) {
      ////console.log(e);
      return res.status(401).json(e);
    }
  }
  
  //--------------------------------------------------------------------------------------------

  async function updateUrun(req, res) {
    if (!("authorization" in req.headers)) {
      return res.status(401).send("No authorization token");
    }
    try {
      const { userId } = jwt.verify(
        req.headers.authorization,
        process.env.JWT_SECRET //process.env.JWT_SECRET
      );
  
      const user = await User.findOne({ _id: userId });
      if (!user) {
        return res.status(401).send("No user");
      }
  
      const yetki = await Yetki.findOne({ rol: user.role });
      if (!yetki) {
        return res.status(401).send("rol icin yetki kaydi yok");
      }
  
      if (
        yetki.yetkiler.urun &&
        yetki.yetkiler.urun.includes("edit")
      ) {
        const {
          query: { id },
        } = req;
        const m = await Urun.findByIdAndUpdate(id, req.body, {new: true, lean: true})
        return res.status(200).json({ data: m });
      } else {
        return res.status(401).send("yetkiniz yok");
      }
    } catch (e) {
      ////console.log(e);
      return res.status(401).json(e);
    }
  }
  
  async function removeUrun(req, res) {
    if (!("authorization" in req.headers)) {
      return res.status(401).send("No authorization token");
    }
    try {
      const { userId } = jwt.verify(
        req.headers.authorization,
        process.env.JWT_SECRET //process.env.JWT_SECRET
      );
  
      const user = await User.findOne({ _id: userId });
      if (!user) {
        return res.status(401).send("No user");
      }
  
      const yetki = await Yetki.findOne({ rol: user.role });
      if (!yetki) {
        return res.status(401).send("rol icin yetki kaydi yok");
      }
  
      if (
        yetki.yetkiler.urun &&
        yetki.yetkiler.urun.includes("view")
      ) {
        const {
          query: { id },
        } = req;
        const m = await Urun.findById(id)
        return res.status(200).json({ data: m });
      } else {
        return res.status(401).send("yetkiniz yok");
      }
    } catch (e) {
      ////console.log(e);
      return res.status(401).json(e);
    }
  }
  