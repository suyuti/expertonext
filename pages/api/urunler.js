import connectDb from "../../utils/connectDb"
import Urun from "../../models/urun"

connectDb()

async function handleGetReq(req, res)  {
    const urunler = await Urun.find({})
    res.status(200).json({data: urunler})
}
export default async (req, res) => {
    switch (req.method) {
      case "GET":
        await handleGetReq(req, res);
        break;
      case "POST":
        await handlePostReq(req, res);
        break;
      default:
        res.setHeader("Allow", ["GET", "POST"]);
        res.status(405).end(`Method ${req.method} Not Allowed`);
        break;
    }
  };
  
  async function handlePostReq(req, res)  {
      var form  = {...req.body};
      var id = form._id
      delete form._id
      var m = new Urun(form)
      await m.save()
      res.status(203).send("urun created");
    }
  