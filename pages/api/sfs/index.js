import connectDb from "../../../utils/connectDb";
import SatisFirsati from "../../../models/satisFirsati2";
import Yetki from "../../../models/yetki";
import User from "../../../models/user";
import jwt from "jsonwebtoken";

connectDb();

export default async (req, res) => {
  switch (req.method) {
    case "GET":
      await getSatisFirsatlari(req, res);
      break;
    case "POST":
      await createSatisFirsati(req, res);
      break;
    default:
      res.setHeader("Allow", ["GET", "POST"]);
      res.status(405).end(`Method ${req.method} Not Allowed`);
      break;
  }
};

async function getSatisFirsatlari(req, res) {
  if (!("authorization" in req.headers)) {
    return res.status(401).send("No authorization token");
  }
  try {
    const { userId } = jwt.verify(
      req.headers.authorization,
      process.env.JWT_SECRET //process.env.JWT_SECRET
    );

    const user = await User.findOne({ _id: userId });
    if (!user) {
      return res.status(401).send("No user");
    }

    const yetki = await Yetki.findOne({ rol: user.role });
    if (!yetki) {
      return res.status(401).send("rol icin yetki kaydi yok");
    }

    if (yetki.yetkiler.satisfirsati && yetki.yetkiler.satisfirsati.includes("list")) {
      let filters = req.query.filter ? JSON.parse(req.query.filter) : {};
      if (user.role !== 'satis-yonetici') {
        filters.atananPersonel = user._id
      }
      const m = await SatisFirsati.find(filters)
        .populate('musteri', ['marka'])
        .populate('atananPersonel', ['adi', 'soyadi'])
      return res.status(200).json({ data: m });
    } else {
      return res.status(401).send("yetkiniz yok");
    }
  } catch (e) {
    ////console.log(e);
    return res.status(401).json(e);
  }
}

async function createSatisFirsati(req, res) {
  if (!("authorization" in req.headers)) {
    return res.status(401).send("No authorization token");
  }
  try {
    const { userId } = jwt.verify(
      req.headers.authorization,
      process.env.JWT_SECRET //process.env.JWT_SECRET
    );

    const user = await User.findOne({ _id: userId });
    if (!user) {
      return res.status(401).send("No user");
    }

    const yetki = await Yetki.findOne({ rol: user.role });
    if (!yetki) {
      return res.status(401).send("rol icin yetki kaydi yok");
    }

    if (yetki.yetkiler.satisfirsati && yetki.yetkiler.satisfirsati.includes("create")) {
        var sf = new SatisFirsati(req.body)
        sf.createdBy = user
        sf.createdAt = new Date()
        let row = await sf.save()
          return res.status(200).json({ data: row });
    } else {
      return res.status(401).send("yetkiniz yok");
    }
  } catch (e) {
    ////console.log(e);
    return res.status(401).json(e);
  }
}
