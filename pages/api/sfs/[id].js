import connectDb from "../../../utils/connectDb";
import SatisFirsati from "../../../models/satisFirsati2";
import Satis from "../../../models/satis";
import Yetki from "../../../models/yetki";
import User from "../../../models/user";
import Teklif from "../../../models/teklifUrun";
import jwt from "jsonwebtoken";
import { PrepareTeklifDoc } from "../../../src/Document";

connectDb();

export default async (req, res) => {
  switch (req.method) {
    case "GET":
      await getSatisFirsati(req, res);
      break;
    case "PUT":
      await updateSatisFirsati(req, res);
      break;
    case "DELETE":
      await removeSatisFirsati(req, res);
      break;
    default:
      res.setHeader("Allow", ["GET", "PUT", "REMOVE"]);
      res.status(405).end(`Method ${req.method} Not Allowed`);
      break;
  }
};

//------------------------------------------------------------------------------------------------

async function getSatisFirsati(req, res) {
  if (!("authorization" in req.headers)) {
    return res.status(401).send("No authorization token");
  }
  try {
    const { userId } = jwt.verify(
      req.headers.authorization,
      process.env.JWT_SECRET //process.env.JWT_SECRET
    );

    const user = await User.findOne({ _id: userId });
    if (!user) {
      return res.status(401).send("No user");
    }

    const yetki = await Yetki.findOne({ rol: user.role });
    if (!yetki) {
      return res.status(401).send("rol icin yetki kaydi yok");
    }

    if (
      yetki.yetkiler.satisfirsati &&
      yetki.yetkiler.satisfirsati.includes("view")
    ) {
      const {
        query: { id },
      } = req;
      const m = await SatisFirsati.findById(id)
        .populate("musteri", ["marka"])
        .populate("atananPersonel", ["adi", "soyadi"])
        .populate({
          path:'teklif.teklifUrunleri',
          model: 'Teklif'
        })
        .exec()
      return res.status(200).json({ data: m });
    } else {
      return res.status(401).send("yetkiniz yok");
    }
  } catch (e) {
    ////console.log(e);
    return res.status(401).json(e);
  }
}

//------------------------------------------------------------------------------------------------

const saveTeklif = async (teklif) => {};

async function updateSatisFirsati(req, res) {
  if (!("authorization" in req.headers)) {
    return res.status(401).send("No authorization token");
  }
  try {
    const { userId } = jwt.verify(
      req.headers.authorization,
      process.env.JWT_SECRET //process.env.JWT_SECRET
    );

    const user = await User.findOne({ _id: userId });
    if (!user) {
      return res.status(401).send("No user");
    }

    const yetki = await Yetki.findOne({ rol: user.role });
    if (!yetki) {
      return res.status(401).send("rol icin yetki kaydi yok");
    }

    if (
      yetki.yetkiler.satisfirsati &&
      yetki.yetkiler.satisfirsati.includes("edit")
    ) {
      const {
        query: { id },
      } = req;

      debugger;
      let sf = await SatisFirsati.findById(id);
      if (sf) {
        if (sf.durum === "YENI") {
          const randevuData = req.body;

          let row = await SatisFirsati.findByIdAndUpdate(
            id,
            {
              durum: "RANDEVU",
              randevu: {
                tarih: randevuData.randevuTarihi,
                yer: randevuData.randevuYeri,
                katilimcilar: randevuData.katilimcilar,
                not: randevuData.randevuNotu,
              },
            },
            { new: true, lean: true }
          );
          return res.status(200).json({ data: row });
        }

        if (sf.durum === "RANDEVU") {
          // Onceki durumu RANDEVU idi. Simdi Teklif detaylari geliyor
          const teklifData = req.body;
          var updateBody = {
            teklif: {
              teklifUrunleri: [],
            },
            durum: "TEKLIF",
          };

          for (var i = 0; i < teklifData.teklifler.length; ++i) {
            let item = teklifData.teklifler[i];
            const _teklif = {
              urun                : item.urun.seciliUrun._id,
              urunAdi             : item.urun.seciliUrun.adi,
              onOdemeTutari       : item.urun.onOdemeTutari,
              basariTutari        : item.urun.basariTutari,
              sabitOdemeTutari    : item.urun.sabitTutar,
              sabitOdemeAy        : item.urun.sabitTutarAy,
              raporBasiOdemeTutari: item.urun.raporBasiOdemeTutari,
              araToplam           : item.urun.araToplam,
              indirim             : item.urun.indirim,
              kdv: item.urun.kdv,
              toplam: item.urun.toplam,
            };
            var t = new Teklif(_teklif);
            let _t = await t.save();
            updateBody.teklif.teklifUrunleri.push(_t._id);

            await PrepareTeklifDoc({
              ..._teklif,
              teklifId: _t._id,
              urunDetay: item.urun.seciliUrun,
            });
          }
          //return res.status(800).send("test");
          let row = await SatisFirsati.findByIdAndUpdate(id, updateBody, {
            new: true,
            lean: true,
          });
          return res.status(200).json({ data: row });
        } 
        
        else if (sf.durum === "TEKLIF") {
          const sonucData = req.body;

          var _sf = await SatisFirsati.findById(id)

          if (sonucData.sonuc === 'BASARISIZ') {
            let r = await SatisFirsati.findByIdAndUpdate(
              id,
              {
                sonuc: {
                  sonuc: sonucData.sonuc,
                  sonuclandiran: user._id,
                  sonucNotu: sonucData.sonucNotu,
                  sonucTarihi: new Date(),
                },
                durum: "KAPALI",
              },
              {new: true, lean: true})
              return res.status(200).json(r)
          }
          else if (sonucData.sonuc === 'BASARILI') {
            var _eldeEdilenCiro = 0
            var _toplamFatura = 0
            for (var i=0; i<sf.teklif.teklifUrunleri.length; ++i) {
              let t = await Teklif.findById(sf.teklif.teklifUrunleri[i])
              if (t.onOdemeTutari) {
                _eldeEdilenCiro += parseInt(t.onOdemeTutari)
              }
              else {
                _eldeEdilenCiro += parseInt(t.sabitOdemeTutari)
              }
              _toplamFatura += t.toplam - t.kdv
            }
            let satis = await new Satis({
              satisFirsati        : sf._id,
              satisYapanPersonel  : sf.atananPersonel,
              satisToplami        : _toplamFatura,
              ilkFaturaToplami    : _eldeEdilenCiro,
              createdBy           : user._id,
            }).save()

            let r = await SatisFirsati.findByIdAndUpdate(
              id,
              {
                sonuc: {
                  sonuc: sonucData.sonuc,
                  sonuclandiran: user._id,
                  //sonucNotu: sonucData.sonucNotu,
                  sonucTarihi: new Date(),
                },
                durum: "KAPALI",
              },
              {new: true, lean: true})
              return res.status(200).json(r)
          }
          return res.status(401).send('unknown sonuc');
        }
      }

      let row = await SatisFirsati.findByIdAndUpdate(id, req.body, {
        new: true,
        lean: true,
      });
      return res.status(200).json({ data: row });
    } else {
      return res.status(401).send("yetkiniz yok");
    }
  } catch (e) {
    ////console.log(e);
    return res.status(401).json(e);
  }
}

//------------------------------------------------------------------------------------------------

async function removeSatisFirsati(req, res) {
  return res.status(200).send("remove satis firsati not implemented yet.");
}
