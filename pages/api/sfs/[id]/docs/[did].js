//import connectDb from "../../../utils/connectDb";
//import SatisFirsati from "../../../models/satisFirsati2";
//import Yetki from "../../../models/yetki";
//import User from "../../../models/user";
//import Teklif from "../../../models/teklifUrun";
//import jwt from "jsonwebtoken";
//import { PrepareTeklifDoc } from "../../../src/Document";
//import { useRouter } from "next/router";
var fs = require("fs");
var PizZip = require('pizzip');

import mime from "mime";

//connectDb();

export default async (req, res) => {
  switch (req.method) {
    case "GET":
      await getDoc(req, res);
      break;
    default:
      res.setHeader("Allow", ["GET"]);
      res.status(405).end(`Method ${req.method} Not Allowed`);
      break;
  }
};

async function getDoc(req, res) {
  // TODO auth ve role check
  try {
    var file = `./dokumanlar/${req.query.did}.docx`;
    var filename = `${req.query.docId}.docx`;

    var content = fs.readFileSync(file, 'binary')
    var zip = new PizZip(content);
    var buff = zip.generate({
      type: 'nodebuffer',
    })
    res.end(buff)
  } catch (e) {
    ////console.log(e);
  }
}
