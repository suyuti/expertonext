import connectDb from "../../../../../utils/connectDb";
import SatisFirsati from "../../../../../models/satisFirsati2";
import Teklif from '../../../../../models/teklifUrun'
import mongoose from "mongoose";

connectDb();

export default async (req, res) => {
  switch (req.method) {
    case "DELETE":
      await removeTeklif(req, res);
      break;
      case "PUT":
        await updateTeklif(req, res);
        break;
      default:
      res.setHeader("Allow", ["DELETE"]);
      res.status(405).end(`Method ${req.method} Not Allowed`);
      break;
  }
};

async function removeTeklif(req, res) {
  const {
    query: { id, tid },
  } = req;

  var sf = await SatisFirsati.findById(id);
  if (!sf) {
    return res.status(404).send("SF not found");
  }
  sf.teklif.teklifUrunleri = sf.teklif.teklifUrunleri.filter((u) => {
    return !u.equals(mongoose.Types.ObjectId(tid));
  });
  let r = await sf.save();
  return res.status(200).json(r);
}

//-------------------------------------------------------------------------------------------

async function updateTeklif(req, res) {
  const {
    query: { id, tid },
    body: payload
  } = req;


  var sf = await SatisFirsati.findById(id);
  if (!sf) {
    return res.status(404).send("SF not found");
  }

  // Yeni teklifi kaydet
  var _yeniTeklif = new Teklif({
    urun                : payload.oncekiTeklif.urun,
    urunAdi             : payload.oncekiTeklif.urunAdi,
    onOdemeTutari       : payload.yeniTeklif.onOdemeTutari,
    basariTutari        : payload.yeniTeklif.basariTutari,
    sabitOdemeTutari    : payload.yeniTeklif.sabitTutar,
    sabitOdemeAy        : payload.yeniTeklif.sabitTutarAy,
    raporBasiOdemeTutari: payload.yeniTeklif.raporBasiOdeme,
    araToplam           : payload.yeniToplamlar.araToplam,
    indirim             : payload.yeniToplamlar.indirim,
    kdv                 : payload.yeniToplamlar.kdv,
    toplam              : payload.yeniToplamlar.toplam,
  })
  let yeniTeklif = await _yeniTeklif.save()

  // Eski teklifi listeden cikar
  sf.teklif.teklifUrunleri = sf.teklif.teklifUrunleri.filter((u) => {
    return !u.equals(mongoose.Types.ObjectId(tid));
  });
  
  // Yeni teklifi listeye ekle
  sf.teklif.teklifUrunleri.push(yeniTeklif._id)
  let result = await sf.save()

  return res.status(200).json(result);
}
