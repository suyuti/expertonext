import connectDb from "../../../../utils/connectDb";
import SatisFirsati from "../../../../models/satisFirsati2";
import Yetki from "../../../../models/yetki";
import User from "../../../../models/user";
import jwt from "jsonwebtoken";
import Randevu from "../../../../models/randevu";

connectDb();

export default async (req, res) => {
  switch (req.method) {
    case "GET":
      await getRandevular(req, res);
      break;
    case "POST":
      await createRandevu(req, res);
      break;
    case "PUT":
      await updateRandevu(req, res);
      break;
    case "DELETE":
      await removeRandevu(req, res);
      break;
    default:
      res.setHeader("Allow", ["GET", "POST", "PUT", "DELETE"]);
      res.status(405).end(`Method ${req.method} Not Allowed`);
      break;
  }
};

async function getRandevular(req, res) {
  return res.status(200).send("Not implemented");
}

async function createRandevu(req, res) {
  if (!("authorization" in req.headers)) {
    return res.status(401).send("No authorization token");
  }
  try {
    const { userId } = jwt.verify(
      req.headers.authorization,
      process.env.JWT_SECRET //process.env.JWT_SECRET
    );

    const user = await User.findOne({ _id: userId });
    if (!user) {
      return res.status(401).send("No user");
    }

    const yetki = await Yetki.findOne({ rol: user.role });
    if (!yetki) {
      return res.status(401).send("rol icin yetki kaydi yok");
    }

    if (yetki.yetkiler.randevu && yetki.yetkiler.randevu.includes("create")) {
      const {
        query: { id },
      } = req;

      let sf = await SatisFirsati.findById(id);
      if (!sf) {
        return res.status(401).send("Satis Firsati bulunamadi");
      }

      var _randevu = new Randevu(req.body)
      _randevu.createdBy = user
      let randevu = await _randevu.save();

      if (randevu) {
        sf.randevu = randevu;
        sf.durum = 'RANDEVU'
        await sf.save();
        return res.status(200).send("ok");
      }
      else {
          return res.status(401).send('Randevu olusmadi')
      }
    } else {
      return res.status(401).send("yetkiniz yok");
    }
  } catch (e) {
    ////console.log(e);
    return res.status(401).send(e);
  }
}
//---------------------------------------------------------------------------------
async function updateRandevu(req, res) {
  return res.status(200).send("Not implemented");
}

async function removeRandevu(req, res) {
  return res.status(200).send("Not implemented");
}
