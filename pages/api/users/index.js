import connectDb from "../../../utils/connectDb";
import User from "../../../models/user";
import Yetki from "../../../models/yetki";
import jwt from "jsonwebtoken";

connectDb();

export default async (req, res) => {
  switch (req.method) {
    case "GET":
      await getUserList(req, res);
      break;
    case "POST":
      await createUser(req, res);
      break;
    default:
      res.setHeader("Allow", ["GET", "POST"]);
      res.status(405).end(`Method ${req.method} Not Allowed`);
      break;
  }
};

async function getUserList(req, res) {
  if (!("authorization" in req.headers)) {
    return res.status(401).send("No authorization token");
  }
  try {
    const { userId } = jwt.verify(
      req.headers.authorization,
      process.env.JWT_SECRET
    );

    const user = await User.findOne({ _id: userId });
    if (!user) {
      return res.status(401).send("No user");
    }

    const yetki = await Yetki.findOne({ rol: user.role });
    if (!yetki) {
      return res.status(401).send("rol icin yetki kaydi yok");
    }

    if (yetki.yetkiler.personel && yetki.yetkiler.personel.includes("view")) {
      let {all, tree, siblings} = req.query
      if (all) {
        console.log(all)
        let allChildren = await user.getAllChildren()
        console.log(allChildren)
        return res.status(200).json({data: allChildren})
      }
      else if (tree) {
        let allTree = await user.getChildrenTree()
        return res.status(200).json({data: allTree})
      }
      else {
        let users = await User.find({}).populate('parent', ['adi', 'soyadi'])
        return res.status(200).json({data: users})
      }


      /*
      console.log(req.query)
      let hierarcy = req.query.hierarcy 
      if (hierarcy) {
        var allChildren = await user.getAllChildren()
        var parent = await user.getParent()
        var siblings = []
        if (parent) {
          //siblings = await parent.getImmediateChildren()
        }
        console.log(allChildren)
        console.log(siblings)
        return res.status(200).json({data: [...allChildren, ...siblings]})
      }
      else {
        const personel = await User.find({}).populate('parent', ['adi', 'soyadi'])
        return res.status(200).json({ data: personel });
      }
      */
    } else {
      return res.status(401).send("yetkiniz yok");
    }
  } catch (e) {
    ////console.log(e);
    return res.status(401).json(e);
  }
}
