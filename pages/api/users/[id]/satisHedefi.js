import connectDb from "../../../../utils/connectDb";
import UserDetail from "../../../../models/userDetails";
import User from "../../../../models/user";
import Yetki from "../../../../models/yetki"
import jwt from "jsonwebtoken";
import mongoose  from "mongoose";
import moment from 'moment'

connectDb();

export default async (req, res) => {
  switch (req.method) {
    case "POST":
      await addSatisHedefi(req, res);
      break;
    default:
      res.setHeader("Allow", ["GET", "PUT"]);
      res.status(405).end(`Method ${req.method} Not Allowed`);
      break;
  }
};

async function addSatisHedefi(req, res) {
    const {
        query: { id },
      } = req;
      if (!("authorization" in req.headers)) {
        return res.status(401).send("No authorization token");
      }
      try {
        const { userId } = jwt.verify(
          req.headers.authorization,
          process.env.JWT_SECRET
        );
    
        const user = await User.findOne({ _id: userId });
        if (!user) {
          return res.status(401).send("No user");
        }
    
        const yetki = await Yetki.findOne({ rol: user.role });
        if (!yetki) {
          return res.status(401).send("rol icin yetki kaydi yok");
        }

        // todo yetki denetimi

        var _userDetail = await UserDetail.findOne({user: id})
        if (_userDetail) {
            if (!_userDetail.satisHedefleri) {
                _userDetail.satisHedefleri = []
            }
            const payload = req.body

            console.log(payload)
            _userDetail.satisHedefleri.push({ay: moment(payload.ay).format('YYYYMM'), hedefciro: payload.hedefciro})
            _userDetail = await _userDetail.save()
            console.log(_userDetail)
            return res.status(200).json(_userDetail)
        }
        else {
            return res.status(401).send('userDetail not found');
        }
      } catch (e) {
        ////console.log(e);
        return res.status(401).json(e);
      }
    }

