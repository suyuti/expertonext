import connectDb from "../../../../utils/connectDb";
import User from "../../../../models/user";
import UserDetail from "../../../../models/userDetails";
import Yetki from "../../../../models/yetki";
import Satis from "../../../../models/satis";
import Gorusme from "../../../../models/gorusme";
import Musteri from "../../../../models/musteri";
import Teklif from "../../../../models/teklifUrun";
import jwt from "jsonwebtoken";
import moment from "moment";
import mongoose from "mongoose";

connectDb();

export default async (req, res) => {
  switch (req.method) {
    case "GET":
      await getUserSatisPerformans(req, res);
      break;
    default:
      res.setHeader("Allow", ["GET"]);
      res.status(405).end(`Method ${req.method} Not Allowed`);
      break;
  }
};

//---------------------------------------------------------------------------------------

async function getUserSatisPerformans(req, res) {    
    if (!("authorization" in req.headers)) {
      return res.status(401).send("No authorization token");
    }
    try {
      const { userId } = jwt.verify(
        req.headers.authorization,
        process.env.JWT_SECRET
      );
  
      const user = await User.findOne({ _id: userId });
      if (!user) {
        return res.status(401).send("No user");
      }
  
      const yetki = await Yetki.findOne({ rol: user.role });
      if (!yetki) {
        return res.status(401).send("rol icin yetki kaydi yok");
      }
  
      if (
        yetki.yetkiler.satisperformans &&
        yetki.yetkiler.satisperformans.includes("view")
      ) {
        const { id: personelId, ay } = req.query;

        var result = {
            hedefler: []
        }
  
        // ----------------------------------------------------------------
        // 1. personel bulunur
        let personel        = await User.findById(personelId).lean();
        let personelDetail  = await UserDetail.findOne({user: personelId}).lean();

        if (!personelDetail) {
            return res.status(404).send(`user detail not found ${personelId}`)
        }
        if (personelDetail.satisHedefleri) {
            personelDetail.satisHedefleri.map(sh => {
                result.hedefler.push({ay: sh.ay, hedefciro: sh.hedefciro, gerceklesenciro: 0, hedeftoplanti: 0, gerceklesentoplanti: 0})
            })
        }

        let satislarByAy = await Satis.aggregate([
            {
              $match: {
                satisYapanPersonel: mongoose.Types.ObjectId(personelId),
                //createdAt: { $gte: new Date(baslangic) },
              },
            },
            {
              $group: {
                _id: {
                  tarih: { $dateToString: { format: "%Y%m", date: "$createdAt" } },
                  sozlesmeToplami: "$satisToplami",
                  ilkFatura: "$ilkFaturaToplami",
                  satisAdedi: { $sum: 1 },
                  toplamSatis: { $sum: "$ilkFaturaToplami" },
                },
              },
            },
            {
              $group: {
                _id: "$_id.tarih",
                toplamSozlesmeTutari: { $sum: "$_id.sozlesmeToplami" },
                toplamIlkFaturaTutari: { $sum: "$_id.ilkFatura" },
              },
            },
            { $sort: { tarih: 1 } },
          ]);

          console.log(result.hedefler)

          result.hedefler.map(s => {
              let h = satislarByAy.find(h => h._id === s.ay)
          })


/*          satislarByAy.map(s => {
              let h = result.hedefler.find(h => h.ay == s._id)
              if (h) {
                  h.gerceklesenciro = s.toplamIlkFaturaTutari
              }
          })
*/


          console.log(satislarByAy)






        return res.status(200).json(result)


/*








        // ----------------------------------------------------------------
        // 2. Baslangic tarihi hesaplanir
        let baslangic = moment().subtract(ay, "months");
  
        // ----------------------------------------------------------------
        // 3. Aylara gore satislar
        let satislarByAy = await Satis.aggregate([
          {
            $match: {
              satisYapanPersonel: mongoose.Types.ObjectId(personelId),
              createdAt: { $gte: new Date(baslangic) },
            },
          },
          {
            $group: {
              _id: {
                tarih: { $dateToString: { format: "%Y-%m", date: "$createdAt" } },
                sozlesmeToplami: "$satisToplami",
                ilkFatura: "$ilkFaturaToplami",
                satisAdedi: { $sum: 1 },
                toplamSatis: { $sum: "$ilkFaturaToplami" },
              },
            },
          },
          {
            $group: {
              _id: "$_id.tarih",
              toplamSozlesmeTutari: { $sum: "$_id.sozlesmeToplami" },
              toplamIlkFaturaTutari: { $sum: "$_id.ilkFatura" },
            },
          },
          { $sort: { tarih: 1 } },
        ]);
        var _satislarByAy = [];
        var gerceklesenSatis = 0
        satislarByAy.map((s) => {
          var _i = [];
          _i.push(s._id);
          _i.push(s.toplamSozlesmeTutari);
          _i.push(s.toplamIlkFaturaTutari);
          _satislarByAy.push(_i);
          gerceklesenSatis += s.toplamIlkFaturaTutari
        });
  
        // ----------------------------------------------------------------
        // 3. Aylara gore gorusmeler
  
        let gorusmelerByAyKanal = await Gorusme.aggregate([
          //{$match: {createdBy: mongoose.Types.ObjectId(personelId)}},
          {
            $group: {
              _id: {
                tarih: { $dateToString: { format: "%Y-%m", date: "$createdAt" } },
                kanal: "$kanal",
              },
              count: { $sum: 1 },
            },
          },
          {
            $group: {
              _id: "$_id.tarih",
              kanalGrup: { $push: { kanal: "$_id.kanal", adet: "$count" } },
            },
          },
          { $sort: { tarih: 1 } },
        ]);
  
        //-------------------------------------------------------------
  
        var _gorusmelerByAy = [];
        var gerceklesenGorusme = 0
        gorusmelerByAyKanal.map((g) => {
          var _i = [];
          _i.push(g._id);
  
          var ki = g.kanalGrup.find((k) => k.kanal === "telefon");
  
          if (ki) {
              _i.push(ki.adet);
              gerceklesenGorusme += ki.adet
          } else {
              _i.push(0);
          }
  
          ki = g.kanalGrup.find((k) => k.kanal === "toplanti");
          if (ki) {
            _i.push(ki.adet);
            gerceklesenGorusme += ki.adet
          } else {
            _i.push(0);
          }
  
          ki = g.kanalGrup.find((k) => k.kanal === "mail");
          if (ki) {
            _i.push(ki.adet);
            gerceklesenGorusme += ki.adet
          } else {
            _i.push(0);
          }
          _gorusmelerByAy.push(_i);
        });
  
  
        //-------------------------------------------------------------
        // 4. Diger toplamlar
        var musteriSayisi = await Musteri.find({musteriTemsilcisi: personelId}).count()
        var gorusmeSayisi = await Gorusme.find({personel: personelId}).count()
        var satisSayisi   = await Satis.find({satisYapanPersonel: personelId}).count()
  
        var responseData = {
            satisHedefleri: [
                {ay: '202007', hedefciro: 15000, gerceklesenciro: 7500, hedeftoplanti: 12, gerceklesentoplanti: 3},
                {ay: '202008', hedefciro: 12000, gerceklesenciro: 8500, hedeftoplanti: 12, gerceklesentoplanti: 3}
            ],





          satisHedefi: personel.satisPersonelData
            ? personel.satisPersonelData.satisHedefi
            : 0,
          gerceklesenSatis: gerceklesenSatis,
          gorusmeHedefi: personel.satisPersonelData
            ? personel.satisPersonelData.gorusmeHedefi
            : 0,
          gerceklesenGorusme: gerceklesenGorusme,
          satislar: _satislarByAy,
          gorusmeler: _gorusmelerByAy,
          toplamlar: {
            musteri: musteriSayisi,
            gorusmeler: gorusmeSayisi,
            satisAdedi: satisSayisi,
            teklifAdedi: '-',
          },
        };
  
        return res.status(200).json({ data: responseData });
  */
        // https://stackoverflow.com/questions/42456436/mongodb-aggregate-nested-group/42456854  **
        // https://stackoverflow.com/questions/17062992/groups-by-month-and-year-using-mongoose-js
        // Geriye dogru aylar bulunur
  /*      var dateStart = baslangic;
        var dateEnd = moment();
        var aylar = [];
        while (
          dateEnd > dateStart ||
          dateStart.format("M") === dateEnd.format("M")
        ) {
          aylar.push(dateStart.format("YYYY-MM"));
          dateStart.add(1, "month");
        }
  */
      } else {
        return res.status(401).send("yetkiniz yok");
      }
    } catch (e) {
      console.log(e);
      return res.status(401).json(e);
    }
  }
  