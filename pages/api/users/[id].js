import connectDb from "../../../utils/connectDb";
import User from "../../../models/user";
import Yetki from "../../../models/yetki"
import jwt from "jsonwebtoken";
import mongoose  from "mongoose";
import UserDetail from "../../../models/userDetails";

connectDb();

export default async (req, res) => {
  switch (req.method) {
    case "GET":
      await getUser(req, res);
      break;
    case "PUT":
      await updateUser(req, res);
      break;
    case "DELETE":
      await removeUser(req, res);
      break;
    default:
      res.setHeader("Allow", ["GET", "PUT", "DELETE"]);
      res.status(405).end(`Method ${req.method} Not Allowed`);
      break;
  }
};

//------------------------------------------------------------------------------------

async function getUser(req, res) {
  const {
    query: { id },
  } = req;
  if (!("authorization" in req.headers)) {
    return res.status(401).send("No authorization token");
  }
  try {
    const { userId } = jwt.verify(
      req.headers.authorization,
      process.env.JWT_SECRET
    );

    const user = await User.findOne({ _id: userId });
    if (!user) {
      return res.status(401).send("No user");
    }

    const yetki = await Yetki.findOne({ rol: user.role });
    if (!yetki) {
      return res.status(401).send("rol icin yetki kaydi yok");
    }

    if (yetki.yetkiler.personel && yetki.yetkiler.personel.includes("view")) {
      
      console.log(id)
      
      const personel = await User.findById(id).select('-password -path ').lean();

      console.log('---------------------1---------------------')
      console.log(personel)

      var detail = await UserDetail.findOne({user:id}).lean()
      if (detail) {
        delete detail._id
        console.log('--------------------2----------------------')
        console.log(detail)
      }

      return res.status(200).json({ data: {...personel, ...detail} });
    } else {
      return res.status(401).send("yetkiniz yok");
    }
  } catch (e) {
    ////console.log(e);
    return res.status(401).json(e);
  }
}

//------------------------------------------------------------------------------------

async function removeUser(req, res) {
  const {
    query: { id },
  } = req;
  const musteri = await Musteri.findById(id);
  res.status(200).json({ data: musteri });
}

//------------------------------------------------------------------------------------

async function updateUser(req, res) {
  const {
    query: { id },
  } = req;
  if (!("authorization" in req.headers)) {
    return res.status(401).send("No authorization token");
  }
  try {
    const { userId } = jwt.verify(
      req.headers.authorization,
      process.env.JWT_SECRET
    );

    const user = await User.findOne({ _id: userId });
    if (!user) {
      return res.status(401).send("No user");
    }

    const yetki = await Yetki.findOne({ rol: user.role });
    if (!yetki) {
      return res.status(401).send("rol icin yetki kaydi yok");
    }

    if (yetki.yetkiler.personel && yetki.yetkiler.personel.includes("view")) {
      const payload = req.body

      var personel = await User.findById(id);
      if (personel) {
        var detail = await UserDetail.findOne({user:id})
        if (!detail) {
          detail = await new UserDetail({user: personel._id})
        }
        Object.assign(personel, payload)
        let _personel = await personel.save()
        //return res.status(200).json({data: {..._personel, ...detail}})
        return res.status(200).json({data: _personel})
      }

/*
      var personel = await User.findByIdAndUpdate(id, payload, {new: true, lean: true})
      var detail = await UserDetail.findOne({user:id})
      if (detail) {
        //detail.satisHedefi  = parseInt(payload.satisHedefi)
        //detail.indirimOrani = parseInt(payload.indirimOrani)
        detail = await detail.save()
      }
      else {
        detail = await new UserDetail({
          user: personel._id,
          //satisHedefi  : parseInt(payload.satisHedefi),
          //indirimOrani : parseInt(payload.indirimOrani)
        }).save()
      }
      return res.status(200).json({ data: {...personel, ...detail} });
*/

      //var u = await User.findById(id)
      //if (u) {
      //  u = {...req.body}
     // }

      ////console.log(req.body)

      //var u = await User.findById(id)
      //if (u) {
      //  var payload = req.body
      //  Object.assign(u, payload)
      //  let r = await u.save()
      //  return res.status(200).json(r)
      //}
    } else {
      return res.status(401).send("yetkiniz yok");
    }
  } catch (e) {
    console.log(e);
    return res.status(401).json(e);
  }
}
