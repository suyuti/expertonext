import connectDb from "../../utils/connectDb";
import Yetki from "../../models/yetki";

connectDb();

async function handleGetReq(req, res) {
  let filters = req.query.filter ? JSON.parse(req.query.filter) : {};
  const y = await Yetki.find(filters);
  res.status(200).json({ data: y });
}

export default async (req, res) => {
  switch (req.method) {
    case "GET":
      await handleGetReq(req, res);
      break;
    case "POST":
      await handlePostReq(req, res);
      break;
    default:
      res.setHeader("Allow", ["GET", "POST"]);
      res.status(405).end(`Method ${req.method} Not Allowed`);
      break;
  }
};

async function handlePostReq(req, res)  {
    var form  = {...req.body};
    var id = form._id
    delete form._id
    var m = new Yetki(form)
    await m.save()
    res.status(203).send("Yetki created");
  }
