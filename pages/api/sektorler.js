import connectDb from "../../utils/connectDb"
import Sektor from "../../models/sektor"

connectDb()

export default async (req, res) => {
    const sektorler = await Sektor.find({})
    res.status(200).json({data: sektorler})
}
