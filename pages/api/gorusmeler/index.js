import connectDb from "../../../utils/connectDb";
import Gorusme from "../../../models/gorusme";
import User from "../../../models/user";
import Yetki from "../../../models/yetki";
import jwt from "jsonwebtoken";

connectDb();

export default async (req, res) => {
  switch (req.method) {
    case "GET":
      await getGorusmelerList(req, res);
      break;
    case "POST":
      await createGorusme(req, res);
      break;
    default:
      res.setHeader("Allow", ["GET", "PUT"]);
      res.status(405).end(`Method ${req.method} Not Allowed`);
      break;
  }
};

async function getGorusmelerList(req, res) {
  if (!("authorization" in req.headers)) {
    return res.status(401).send("No authorization token");
  }
  try {
    const { userId } = jwt.verify(
      req.headers.authorization,
      process.env.JWT_SECRET
    );

    const user = await User.findOne({ _id: userId });
    if (!user) {
      return res.status(401).send("No user");
    }

    const yetki = await Yetki.findOne({ rol: user.role });
    if (!yetki) {
      return res.status(401).send("rol icin yetki kaydi yok");
    }

    if (yetki.yetkiler.gorusme && yetki.yetkiler.gorusme.includes("list")) {
      let filters = req.query.filter ? JSON.parse(req.query.filter) : {};
      let projection = req.query.projection
        ? JSON.parse(req.query.projection)
        : { fields: null };


        //console.log(filters)
      const gorusmeler = await Gorusme.find(filters, projection.fields)
        .populate("musteri")
        .populate("satisFirsati")
        .populate("kisi")
        .populate("personel");

      return res.status(200).json({ data: gorusmeler });
    } else {
      return res.status(401).send("yetkiniz yok");
    }
  } catch (e) {
    ////console.log(e);
    return res.status(401).json(e);
  }
}

async function createGorusme(req, res) {
  if (!("authorization" in req.headers)) {
    return res.status(401).send("No authorization token");
  }
  try {
    const { userId } = jwt.verify(
      req.headers.authorization,
      process.env.JWT_SECRET
    );

    const user = await User.findOne({ _id: userId });
    if (!user) {
      return res.status(401).send("No user");
    }

    const yetki = await Yetki.findOne({ rol: user.role });
    if (!yetki) {
      return res.status(401).send("rol icin yetki kaydi yok");
    }

    if (yetki.yetkiler.gorusme && yetki.yetkiler.gorusme.includes("create")) {
      var g = new Gorusme(req.body);
      g.createdAt = new Date();
      g.createdBy = user;
      let row = await g.save();
      return res.status(200).json({ data: row });
    } else {
      return res.status(401).send("yetkiniz yok");
    }
  } catch (e) {
    ////console.log(e);
    return res.status(401).json(e);
  }
}
