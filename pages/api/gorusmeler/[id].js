import connectDb from "../../../utils/connectDb";
import Gorusme from "../../../models/gorusme";

connectDb();

export default async (req, res) => {
  switch (req.method) {
    case "GET":
      await getGorusme(req, res);
      break;
    default:
      res.setHeader("Allow", ["GET", "PUT"]);
      res.status(405).end(`Method ${req.method} Not Allowed`);
      break;
  }
};

async function getGorusme(req, res) {
  const {
    query: { id, mid },
  } = req;

  if (id === "new") {
    res.status(200).json({ data: {/*musteri: mid*/} });
  } else {
    const gorusme = await Gorusme.findById(id)
      .populate("musteri")
      .populate("satisFirsati")
      .populate("kisi")
      .populate("personel");
    res.status(200).json({ data: gorusme });
  }
}
