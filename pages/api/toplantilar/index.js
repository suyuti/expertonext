import connectDb from "../../../utils/connectDb";
import Yetki from "../../../models/yetki";
import User from "../../../models/user";
import Toplanti from "../../../models/toplanti";
import jwt from "jsonwebtoken";

connectDb();

export default async (req, res) => {
  switch (req.method) {
    case "GET":
      await getToplantilar(req, res);
      break;
    case "POST":
      await createToplanti(req, res);
      break;
    default:
      res.setHeader("Allow", ["GET", "POST"]);
      res.status(405).end(`Method ${req.method} Not Allowed`);
      break;
  }
};

async function getToplantilar(req, res) {
    if (!("authorization" in req.headers)) {
      return res.status(401).send("No authorization token");
    }
    try {
      const { userId } = jwt.verify(
        req.headers.authorization,
        process.env.JWT_SECRET
      );
  
      const user = await User.findOne({ _id: userId });
      if (!user) {
        return res.status(401).send("No user");
      }
  
      const yetki = await Yetki.findOne({ rol: user.role });
      if (!yetki) {
        return res.status(401).send("rol icin yetki kaydi yok");
      }
  
      if (yetki.yetkiler.toplanti && yetki.yetkiler.toplanti.includes("list")) {
        let filters = req.query.filter ? JSON.parse(req.query.filter) : {};
        const t = await Toplanti.find(filters)
          .populate('musteri', ['marka'])
          .populate({
            path:'firmaKatilimcilar',
            model: 'Kisi',
            select: ['adi', 'soyadi', 'unvani']
          })
          .populate({
            path:'expertoKatilimcilar',
            model: 'User',
            select: ['adi', 'soyadi']
          }).exec()
            return res.status(200).json({ data: t });
      } else {
        return res.status(401).send("yetkiniz yok");
      }
    } catch (e) {
      ////console.log(e);
      return res.status(401).json(e);
    }
  }
  
async function createToplanti(req, res) {
    if (!("authorization" in req.headers)) {
      return res.status(401).send("No authorization token");
    }
    try {
      const { userId } = jwt.verify(
        req.headers.authorization,
        process.env.JWT_SECRET
      );
  
      const user = await User.findOne({ _id: userId });
      if (!user) {
        return res.status(401).send("No user");
      }
  
      const yetki = await Yetki.findOne({ rol: user.role });
      if (!yetki) {
        return res.status(401).send("rol icin yetki kaydi yok");
      }
  
      if (yetki.yetkiler.toplanti && yetki.yetkiler.toplanti.includes("create")) {
        const payload = req.body
        let toplanti = await new Toplanti(payload).save()
        return res.status(200).json({ data: toplanti });
      } else {
        return res.status(401).send("yetkiniz yok");
      }
    } catch (e) {
      console.log(e);
      return res.status(401).json(e);
    }
  }
  