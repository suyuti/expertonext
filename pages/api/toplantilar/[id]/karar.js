import connectDb from "../../../../utils/connectDb";
import Yetki from "../../../../models/yetki";
import User from "../../../../models/user";
import Gorev from "../../../../models/gorev";
import Toplanti from "../../../../models/toplanti";
import jwt from "jsonwebtoken";

connectDb();

export default async (req, res) => {
  switch (req.method) {
    case "POST":
      await createToplantiKarar(req, res);
      break;
    default:
      res.setHeader("Allow", ["POST"]);
      res.status(405).end(`Method ${req.method} Not Allowed`);
      break;
  }
};

async function createToplantiKarar(req, res) {
    if (!("authorization" in req.headers)) {
      return res.status(401).send("No authorization token");
    }
    try {
      const { userId } = jwt.verify(
        req.headers.authorization,
        process.env.JWT_SECRET
      );
  
      const user = await User.findOne({ _id: userId });
      if (!user) {
        return res.status(401).send("No user");
      }
  
      const yetki = await Yetki.findOne({ rol: user.role });
      if (!yetki) {
        return res.status(401).send("rol icin yetki kaydi yok");
      }
  
      if (yetki.yetkiler.toplanti && yetki.yetkiler.toplanti.includes("edit")) {
        const {
          query: { id },
        } = req;
        const payload = req.body

        var t = await Toplanti.findById(id)
        if (t) {
          //-----------------------------------
          // Gorev olusturulur
          const gorev = await new Gorev({
            baslik: payload.konu,
            musteri: payload.musteri,
            //toplanti: ,
            personel: payload.ilgiliPersonel,
            durum: 'ACIK',
            icGorev: false,
            // ilerleme
            termin: payload.sonTarih,
            aciklama: payload.konu,
            priority: payload.oncelik
          }).save()



            t.kararlar.push({
                //proje: ,
                konu: payload.konu,
                ilgiliPersonel: payload.ilgiliPersonel,
                terminTarihi: payload.sonTarih,
                oncelik: payload.oncelik,
                gorev : gorev,
                durum : 'ACIK'
            })
            delete t._id 
            let r = await t.save()
            
            // Tekrar gorevin toplantisi set edilir
            gorev.toplanti = r._id
            gorev.save()

            return res.status(200).json({ data: r });
        }
                
        return res.status(404).send('Toplanti bulunamadi');
      } else {
        return res.status(401).send("yetkiniz yok");
      }
    } catch (e) {
      console.log(e);
      return res.status(401).json(e);
    }
  }
  