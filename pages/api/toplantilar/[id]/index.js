import connectDb from "../../../../utils/connectDb";
import Yetki from "../../../../models/yetki";
import User from "../../../../models/user";
import Toplanti from "../../../../models/toplanti";
import jwt from "jsonwebtoken";

connectDb();

export default async (req, res) => {
  switch (req.method) {
    case "GET":
      await getToplanti(req, res);
      break;
    case "PUT":
      await updateToplanti(req, res);
      break;
    default:
      res.setHeader("Allow", ["GET", "POST"]);
      res.status(405).end(`Method ${req.method} Not Allowed`);
      break;
  }
};

async function getToplanti(req, res) {
  if (!("authorization" in req.headers)) {
    return res.status(401).send("No authorization token");
  }
  try {
    const { userId } = jwt.verify(
      req.headers.authorization,
      process.env.JWT_SECRET
    );

    const user = await User.findOne({ _id: userId });
    if (!user) {
      return res.status(401).send("No user");
    }

    const yetki = await Yetki.findOne({ rol: user.role });
    if (!yetki) {
      return res.status(401).send("rol icin yetki kaydi yok");
    }

    if (yetki.yetkiler.toplanti && yetki.yetkiler.toplanti.includes("view")) {
      const {
        query: { id },
      } = req;
      if (id === 'new') {
        return res.status(200).json({ data: {} });
      }
      const t = await Toplanti.findById(id)
        .populate("musteri", ["marka"])
        .populate({path: 'firmaKatilimcilar', model: 'Kisi', select: ['adi', 'soyadi', 'unvani'] })
        .populate({path: 'expertoKatilimcilar', model: 'User', select: ['adi', 'soyadi'] })
        .populate({path: 'kararlar.ilgiliPersonel', model: 'User', select: ['adi', 'soyadi'] })
        .exec()
      return res.status(200).json({ data: t });
    } else {
      return res.status(401).send("yetkiniz yok");
    }
  } catch (e) {
    ////console.log(e);
    return res.status(401).json(e);
  }
}

async function updateToplanti(req, res) {
  if (!("authorization" in req.headers)) {
    return res.status(401).send("No authorization token");
  }
  try {
    const { userId } = jwt.verify(
      req.headers.authorization,
      process.env.JWT_SECRET
    );

    const user = await User.findOne({ _id: userId });
    if (!user) {
      return res.status(401).send("No user");
    }

    const yetki = await Yetki.findOne({ rol: user.role });
    if (!yetki) {
      return res.status(401).send("rol icin yetki kaydi yok");
    }

    if (yetki.yetkiler.toplanti && yetki.yetkiler.toplanti.includes("edit")) {
      //const payload = req.body
      //let toplanti = await new Toplanti(payload).save()
      //return res.status(200).json({ data: t });
      return res.status(404).send("not implemented yet");
    } else {
      return res.status(401).send("yetkiniz yok");
    }
  } catch (e) {
    ////console.log(e);
    return res.status(401).json(e);
  }
}
