import connectDb from "../../../../utils/connectDb"
import SatisFirsati from "../../../../models/satisFirsati"

connectDb()

export default async (req, res) => {
    switch (req.method) {
      case "GET":
          //console.log('GET musteri Satis Firsatlari')
        await handleGetReq(req, res);
        break;
      case "PUT":
        await handlePutReq(req, res);
        break;
      default:
        res.setHeader("Allow", ["GET", "PUT"]);
        res.status(405).end(`Method ${req.method} Not Allowed`);
        break;
    }
  };
  

async function handleGetReq(req, res)  {
    const {
        query: {id}
    } = req
    const sf = await SatisFirsati.find()
    res.status(200).json({data: sf})
}

async function handlePutReq(req, res)  {
    var form  = {...req.body};
    var id = form._id
    delete form._id
    var m = await Musteri.findById(id)
    if (m) {
      m.unvan = form.unvan
      m.sektor = form.sektor
      await m.save()
    }
    //var r = await Musteri.findOneAndUpdate(id , form);
    ////console.log(r)
    res.status(203).send("User updated");
  }
