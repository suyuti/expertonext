import connectDb from "../../../../utils/connectDb"
import Gorusme from "../../../../models/gorusme"

connectDb()

export default async (req, res) => {
    switch (req.method) {
      case "GET":
        await getMusteriGorusmeler(req, res);
        break;
      case "PUT":
        await handlePutReq(req, res);
        break;
      default:
        res.setHeader("Allow", ["GET", "PUT"]);
        res.status(405).end(`Method ${req.method} Not Allowed`);
        break;
    }
  };
  

async function getMusteriGorusmeler(req, res)  {
    const {
        query: {id}
    } = req
    const gorusmeler = await Gorusme.find({musteri: id}).populate('kisi').populate('personel')
    res.status(200).json({data: gorusmeler})
}

async function handlePutReq(req, res)  {
    var form  = {...req.body};
    var id = form._id
    delete form._id
    var m = await Musteri.findById(id)
    if (m) {
      m.unvan = form.unvan
      m.sektor = form.sektor
      await m.save()
    }
    res.status(203).send("User updated");
  }
