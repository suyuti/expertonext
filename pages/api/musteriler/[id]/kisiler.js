import connectDb from "../../../../utils/connectDb"
import Kisi from "../../../../models/kisi"

connectDb()

export default async (req, res) => {
    switch (req.method) {
      case "GET":
        await handleGetReq(req, res);
        break;
      case "POST":
        await handlePostReq(req, res);
        break;
      default:
        res.setHeader("Allow", ["GET", "POST"]);
        res.status(405).end(`Method ${req.method} Not Allowed`);
        break;
    }
  };
  

async function handleGetReq(req, res)  {
    const {
        query: {id}
    } = req
    const kisi = await Kisi.find({musteri:id})
    res.status(200).json({data: kisi})
}

// TODO
async function handlePostReq(req, res)  {
    var form  = {...req.body};
    var id = form._id
    delete form._id
    var m = await Musteri.findById(id)
    if (m) {
      m.unvan = form.unvan
      m.sektor = form.sektor
      await m.save()
    }
    //var r = await Musteri.findOneAndUpdate(id , form);
    ////console.log(r)
    res.status(203).send("User updated");
  }
