import connectDb from "../../../utils/connectDb";
import Musteri from "../../../models/musteri";
import Yetki from "../../../models/yetki";
import User from "../../../models/user";
import jwt from "jsonwebtoken";

connectDb();

export default async (req, res) => {
  switch (req.method) {
    case "GET":
      await getMusteri(req, res);
      break;
    case "PUT":
      await updateMusteri(req, res);
      break;
    default:
      res.setHeader("Allow", ["GET", "PUT"]);
      res.status(405).end(`Method ${req.method} Not Allowed`);
      break;
  }
};

async function getMusteri(req, res) {
  if (!("authorization" in req.headers)) {
    return res.status(401).send("No authorization token");
  }
  try {
    const { userId } = jwt.verify(
      req.headers.authorization,
      process.env.JWT_SECRET //process.env.JWT_SECRET
    );

    const user = await User.findOne({ _id: userId });
    if (!user) {
      return res.status(401).send("No user");
    }

    const yetki = await Yetki.findOne({ rol: user.role });
    if (!yetki) {
      return res.status(401).send("rol icin yetki kaydi yok");
    }

    if (
      yetki.yetkiler.musteri &&
      yetki.yetkiler.musteri.includes("view")
    ) {
      const {
        query: { id },
      } = req;


      if (id === 'new') {
        return res.status(200).json({data: {}})
      }
      const musteri = await Musteri.findById(id)
      return res.status(200).json({ data: musteri });
    } else {
      return res.status(401).send("yetkiniz yok");
    }
  } catch (e) {
    ////console.log(e);
    return res.status(401).json(e);
  }
}

//-------------------------------------------------------------------------------------

async function updateMusteri(req, res) {
  if (!("authorization" in req.headers)) {
    return res.status(401).send("No authorization token");
  }
  try {
    const { userId } = jwt.verify(
      req.headers.authorization,
      process.env.JWT_SECRET //process.env.JWT_SECRET
    );

    const user = await User.findOne({ _id: userId });
    if (!user) {
      return res.status(401).send("No user");
    }

    const yetki = await Yetki.findOne({ rol: user.role });
    if (!yetki) {
      return res.status(401).send("rol icin yetki kaydi yok");
    }

    if (
      yetki.yetkiler.musteri &&
      yetki.yetkiler.musteri.includes("edit")
    ) {
      const {
        query: { id },
      } = req;

      let m = await Musteri.findByIdAndUpdate(id, req.body, {new: true, lean: true})

      return res.status(200).json({ data: m });
    } else {
      return res.status(401).send("yetkiniz yok");
    }
  } catch (e) {
    ////console.log(e);
    return res.status(401).json(e);
  }
}
