import connectDb from "../../../utils/connectDb";
import Randevu from "../../../models/randevu";
import Yetki from "../../../models/yetki";
import User from "../../../models/user";
import jwt from "jsonwebtoken";

connectDb();

export default async (req, res) => {
  switch (req.method) {
    case "GET":
      await getRandevu(req, res);
      break;
    case "PUT":
      await updateRandevu(req, res);
      break;
    case "DELETE":
      await removeRandevu(req, res);
      break;
    default:
      res.setHeader("Allow", ["GET", "PUT", "POST", "DELETE"]);
      res.status(405).end(`Method ${req.method} Not Allowed`);
      break;
  }
};


async function getRandevu(req, res) {
    if (!("authorization" in req.headers)) {
      return res.status(401).send("No authorization token");
    }
    try {
      const { userId } = jwt.verify(
        req.headers.authorization,
        process.env.JWT_SECRET //process.env.JWT_SECRET
      );
  
      const user = await User.findOne({ _id: userId });
      if (!user) {
        return res.status(401).send("No user");
      }
  
      const yetki = await Yetki.findOne({ rol: user.role });
      if (!yetki) {
        return res.status(401).send("rol icin yetki kaydi yok");
      }
  
      if (yetki.yetkiler.randevu && yetki.yetkiler.randevu.includes("view")) {
        const {
          query: { id },
        } = req;

        let randevu = await Randevu.findById(id)
            .populate('musteri', ['marka'])
        return res.status(200).json(randevu)
      } else {
        return res.status(401).send("yetkiniz yok");
      }
    } catch (e) {
      ////console.log(e);
      return res.status(401).json(e);
    }
  }
  

  async function createRandevu(req, res) {
    if (!("authorization" in req.headers)) {
      return res.status(401).send("No authorization token");
    }
    try {
      const { userId } = jwt.verify(
        req.headers.authorization,
        process.env.JWT_SECRET //process.env.JWT_SECRET
      );
  
      const user = await User.findOne({ _id: userId });
      if (!user) {
        return res.status(401).send("No user");
      }
  
      const yetki = await Yetki.findOne({ rol: user.role });
      if (!yetki) {
        return res.status(401).send("rol icin yetki kaydi yok");
      }
  
      if (yetki.yetkiler.randevu && yetki.yetkiler.randevu.includes("create")) {
        const {
          query: { id },
        } = req;
  
        let sf = await SatisFirsati.findById(id);
        if (!sf) {
          return res.status(401).send("Satis Firsati bulunamadi");
        }
  
        var _randevu = new Randevu(req.body)
        randevu.createdBy = user
        let randevu = await _randevu.save();
  
        if (randevu) {
          sf.randevu = randevu;
          await sf.save();
          return res.status(200).send("ok");
        }
        else {
            return res.status(401).send('Randevu olusmadi')
        }
      } else {
        return res.status(401).send("yetkiniz yok");
      }
    } catch (e) {
      ////console.log(e);
      return res.status(401).json(e);
    }
  }
  