import connectDb from "../../../utils/connectDb";
import Departman from "../../../models/departman";
import Yetki from "../../../models/yetki";
import User from "../../../models/user";
import jwt from "jsonwebtoken";

connectDb();

export default async (req, res) => {
  switch (req.method) {
    case "GET":
      await getDepartmanlar(req, res);
      break;
    case "POST":
      await createDepartman(req, res);
      break;
    default:
      res.setHeader("Allow", ["GET", "POST"]);
      res.status(405).end(`Method ${req.method} Not Allowed`);
      break;
  }
};

async function getDepartmanlar(req, res) {
  if (!("authorization" in req.headers)) {
    return res.status(401).send("No authorization token");
  }
  try {
    const { userId } = jwt.verify(
      req.headers.authorization,
      process.env.JWT_SECRET //process.env.JWT_SECRET
    );

    const user = await User.findOne({ _id: userId });
    if (!user) {
      return res.status(401).send("No user");
    }

    const yetki = await Yetki.findOne({ rol: user.role });
    if (!yetki) {
      return res.status(401).send("rol icin yetki kaydi yok");
    }

    if (yetki.yetkiler.departman && yetki.yetkiler.departman.includes("list")) {
      let filters = req.query.filter ? JSON.parse(req.query.filter) : {};
      let projection = req.query.projection
        ? JSON.parse(req.query.projection)
        : { fields: null };
      const m = await Departman.find(filters, projection.fields)
        .populate("yoneticisi", ["adi", "soyadi"])
        .exec();
      return res.status(200).json({ data: m });
    } else {
      return res.status(401).send("yetkiniz yok");
    }
  } catch (e) {
    ////console.log(e);
    return res.status(401).json(e);
  }
}

async function createDepartman(req, res) {
  if (!("authorization" in req.headers)) {
    return res.status(401).send("No authorization token");
  }
  try {
    const { userId } = jwt.verify(
      req.headers.authorization,
      process.env.JWT_SECRET //process.env.JWT_SECRET
    );

    const user = await User.findOne({ _id: userId });
    if (!user) {
      return res.status(401).send("No user");
    }

    const yetki = await Yetki.findOne({ rol: user.role });
    if (!yetki) {
      return res.status(401).send("rol icin yetki kaydi yok");
    }

    if (
      yetki.yetkiler.departman &&
      yetki.yetkiler.departman.includes("create")
    ) {
      var dep = new Departman(req.body);
      dep.createdBy = user;
      dep.createdAt = new Date();
      let row = await dep.save();
      return res.status(200).json({ data: row });
    } else {
      return res.status(401).send("yetkiniz yok");
    }
  } catch (e) {
    ////console.log(e);
    return res.status(401).json(e);
  }
}
