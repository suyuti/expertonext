import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";

import User from "../../models/user";
import UserDetail from "../../models/userDetails";
import connectDb from "../../utils/connectDb";

connectDb();

export default async (req, res) => {
  const { email, password } = req.body;
  try {
    const user = await User.findOne({ email }).select("+password");
    if (user) {
      return res.status(404).send("A user exists with that email");
    }
    const hashedPassword = await bcrypt.hash(password, 10);


    const newUser = await new User({
      email: email,
      password: hashedPassword
    }).save()

    new UserDetail({
      user: newUser,
      satisHedefleri: []
    }).save()

    const token = jwt.sign({ userId: newUser._id }, process.env.JWT_SECRET, {
      expiresIn: "1d"
    });
    res.status(201).json(token);

  } catch (err) {
    console.error(err);
    res.status(500).send("Error logging in user");
  }
};
