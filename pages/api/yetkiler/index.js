import connectDb from "../../../utils/connectDb";
import User from "../../../models/user";
import Yetki from "../../../models/yetki";
import jwt from "jsonwebtoken";

connectDb();

export default async (req, res) => {
  switch (req.method) {
    case "GET":
      await getYetkiList(req, res);
      break;
      case "POST":
        await createYetki(req, res);
        break;
      default:
      res.setHeader("Allow", ["GET"]);
      res.status(405).end(`Method ${req.method} Not Allowed`);
      break;
  }
};

async function getYetkiList(req, res) {
  if (!("authorization" in req.headers)) {
    return res.status(401).send("No authorization token");
  }
  try {
    const { userId } = jwt.verify(
      req.headers.authorization,
      process.env.JWT_SECRET
    );

    const user = await User.findOne({ _id: userId });
    if (!user) {
      return res.status(401).send("No user");
    }

    const yetki = await Yetki.findOne({ rol: user.role });
    if (!yetki) {
      return res.status(401).send("rol icin yetki kaydi yok");
    }

    if (yetki.yetkiler.yetki && yetki.yetkiler.yetki.includes("view")) {
      const yetkiler = await Yetki.find({})
      //console.log('Yerklier//////')
      //console.log(yetkiler)
      return res.status(200).json(yetkiler);
    } else {
      return res.status(401).send("yetkiniz yok");
    }
  } catch (e) {
    ////console.log(e);
    return res.status(401).json(e);
  }
}


async function createYetki(req, res) {
  if (!("authorization" in req.headers)) {
    return res.status(401).send("No authorization token");
  }
  try {
    const { userId } = jwt.verify(
      req.headers.authorization,
      process.env.JWT_SECRET
    );

    const user = await User.findOne({ _id: userId });
    if (!user) {
      return res.status(401).send("No user");
    }

    const yetki = await Yetki.findOne({ rol: user.role });
    if (!yetki) {
      return res.status(401).send("rol icin yetki kaydi yok");
    }

    if (yetki.yetkiler.yetki && yetki.yetkiler.yetki.includes("create")) {
      const data = req.body.yetkiler
      var yetkiler = {}
      data.map(d => {
          yetkiler[`${d.field}`] = []
          if (d.list) {
              yetkiler[`${d.field}`].push('list')
          }
          if (d.create) {
              yetkiler[`${d.field}`].push('create')
          }
          if (d.edit) {
              yetkiler[`${d.field}`].push('edit')
          }
          if (d.view) {
              yetkiler[`${d.field}`].push('view')
          }
          if (d.delete) {
              yetkiler[`${d.field}`].push('delete')
          }
      })

      let y = await new Yetki({
        rol: req.body.rolAdi,
        yetkiler: yetkiler}).save()
      return res.status(200).json({data:y});
    } else {
      return res.status(401).send("yetkiniz yok");
    }
  } catch (e) {
    ////console.log(e);
    return res.status(401).json(e);
  }
}
