import connectDb from "../../../utils/connectDb";
import User from "../../../models/user";
import Yetki from "../../../models/yetki";
import jwt from "jsonwebtoken";

connectDb();

export default async (req, res) => {
  switch (req.method) {
    case "GET":
      await getYetki(req, res);
      break;
    case "PUT":
      await updateYetki(req, res);
      break;
    default:
      res.setHeader("Allow", ["GET", 'PUT']);
      res.status(405).end(`Method ${req.method} Not Allowed`);
      break;
  }
};

async function getYetki(req, res) {
  if (!("authorization" in req.headers)) {
    return res.status(401).send("No authorization token");
  }
  try {
    const { userId } = jwt.verify(
      req.headers.authorization,
      process.env.JWT_SECRET
    );

    const user = await User.findOne({ _id: userId });
    if (!user) {
      return res.status(401).send("No user");
    }

    const yetki = await Yetki.findOne({ rol: user.role });
    if (!yetki) {
      return res.status(401).send("rol icin yetki kaydi yok");
    }

    if (yetki.yetkiler.yetki && yetki.yetkiler.yetki.includes("view")) {
        const {id} = req.query
      const yetki = await Yetki.findById(id);
      return res.status(200).json(yetki);
    } else {
      return res.status(401).send("yetkiniz yok");
    }
  } catch (e) {
    ////console.log(e);
    return res.status(401).json(e);
  }
}

//-------------------------------------------------------------------------------------

async function updateYetki(req, res) {
    if (!("authorization" in req.headers)) {
      return res.status(401).send("No authorization token");
    }
    try {
      const { userId } = jwt.verify(
        req.headers.authorization,
        process.env.JWT_SECRET
      );
  
      const user = await User.findOne({ _id: userId });
      if (!user) {
        return res.status(401).send("No user");
      }
  
      const yetki = await Yetki.findOne({ rol: user.role });
      if (!yetki) {
        return res.status(401).send("rol icin yetki kaydi yok");
      }
  
      if (yetki.yetkiler.yetki && yetki.yetkiler.yetki.includes("view")) {
          const {id} = req.query
        const data = req.body.yetkiler
        var yetkiler = {}
        data.map(d => {
            yetkiler[`${d.field}`] = []
            if (d.list) {
                yetkiler[`${d.field}`].push('list')
            }
            if (d.create) {
                yetkiler[`${d.field}`].push('create')
            }
            if (d.edit) {
                yetkiler[`${d.field}`].push('edit')
            }
            if (d.view) {
                yetkiler[`${d.field}`].push('view')
            }
            if (d.delete) {
                yetkiler[`${d.field}`].push('delete')
            }
        })


        const y = await Yetki.findById(id);
        y.yetkiler = yetkiler
        await y.save()
        return res.status(200).send('ok');
      } else {
        return res.status(401).send("yetkiniz yok");
      }
    } catch (e) {
      ////console.log(e);
      return res.status(401).json(e);
    }
  }
  