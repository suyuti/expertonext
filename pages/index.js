import React from 'react';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import ProTip from '../src/ProTip';
import Link from '../components/Link';
import Copyright from '../src/Copyright';
import SatisDashboard from '../components/dashboard/SatisDashboard';
import Page from '../src/Page';
import { redirectUser } from '../utils/auth';

const Index = (props) => {
  return (
    <Page
      content= {
        <SatisDashboard {...props}/>
      }
    />
  );
}

export default Index
