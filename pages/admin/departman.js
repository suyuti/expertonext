import React from "react";
import baseUrl from "../../utils/baseUrl";
import { parseCookies } from "nookies";
import axios from "axios";
import MaterialTable from "material-table";
import { Chip, TextField, MenuItem, Typography } from "@material-ui/core";

const DepartmanPage = (props) => {
  const { departmanlar, personeller, token } = props;

  return (
    <div className="p-8">
      <MaterialTable
        title="Departmanlar"
        data={departmanlar}
        columns={[
          {
            title: "Departman Adı",
            field: "adi",
            editComponent: (props) => (
              <TextField
                autoFocus
                label="Departman adı"
                variant="outlined"
                fullWidth
                value={props.value}
                onChange={(e) => props.onChange(e.target.value)}
              />
            ),
          },
          {
            title: "Yöneticisi",
            field: "yoneticisi",
            render: (rowData) => (
              rowData.yoneticisi ? 
              <Typography >{`${rowData.yoneticisi.adi} ${rowData.yoneticisi.soyadi}`}</Typography>
              :
              <Typography >Yönetici tanımlanmamış</Typography>
            ),
            editComponent: (props) => (
              <TextField
                label="Yöneticisi"
                variant="outlined"
                fullWidth
                value={props.value}
                onChange={(e) => props.onChange(e.target.value)}
                select
              >
                {personeller &&
                  personeller.map((p) => (
                    <MenuItem
                      key={p._id}
                      value={p._id}
                    >{`${p.adi} ${p.soyadi}`}</MenuItem>
                  ))}
              </TextField>
            ),
          },
        ]}
        editable={{
          onRowAdd: (newData) =>
            axios.post(`${baseUrl}/api/departmanlar`, newData, {
              headers: { authorization: token },
            }),
          onRowUpdate: (newData, oldData) => {},
          onRowDelete: (oldData) => 
            axios.delete(`${baseUrl}/api/departmanlar/${oldData._id}`, {
            headers: { authorization: token },
          })
        }}
      />
    </div>
  );
};

DepartmanPage.getInitialProps = async (ctx) => {
  const { token } = parseCookies(ctx);
  let resp = await axios.get(`${baseUrl}/api/departmanlar`, {
    headers: { authorization: token },
  });
  let respUsers = await axios.get(`${baseUrl}/api/users`, {
    headers: { authorization: token },
  });

  return {
    departmanlar: resp.data.data,
    personeller: respUsers.data.data,
  };
};

export default DepartmanPage;
