import React from 'react'
import Page from '../src/Page'
import { Typography, AppBar, Paper } from '@material-ui/core'

const Test = (props) => {
    return (
        <Page
            header={
                <Paper className='p-5 border-red-600 flex justify-end'>
                    <Typography variant='h5'>Test</Typography>
                </Paper>
            }
            content={
                <Paper className='border shadow w-full'>
                    <Typography>Hello</Typography>
                </Paper>
            }
        >

        </Page>
    )
}
export default Test