import React from "react";
import Page from "../../../src/Page";
import SatisFirsati from "../../../components/sf/SatisFirsati";
import { parseCookies, destroyCookie } from "nookies";
import baseUrl from "../../../utils/baseUrl";
import SatisFirsatiSurec from "../../../components/sf/SatisFirsatiSurec";
import SatisFirsatiRandevu from "../../../components/sf/SfRandevu";
import SatisFirsatiTeklif from "../../../components/sf/SfTeklif";
import SatisFirsatiSonuc from "../../../components/sf/SfSonuc";
import SatisFirsatiNotlar from "../../../components/sf/Notlar";
import TeklifDokumanlari from "../../../components/sf/TeklifDokumanlari";
import axios from "axios";

const SatisFirsatiPage = (props) => {
  const { satisFirsati } = props;
  return (
    <Page
      content={
        <div className="flex-col">
          <div className="sm:w-1/1">
            <SatisFirsatiSurec {...props} />
          </div>
          <div className="flex">
            {satisFirsati.durum === undefined && (
              <div className="sm:w-1/2 mr-4">
                <SatisFirsati {...props} />
              </div>
            )}
            {satisFirsati.durum === "YENI" && (
              <div className="sm:w-1/2 mr-4">
                <SatisFirsatiRandevu {...props} />
              </div>
            )}
            {satisFirsati.durum === "RANDEVU" && (
              <div className="sm:w-1/2 mr-4">
                <SatisFirsatiTeklif {...props} />
              </div>
            )}
            {satisFirsati.durum === "TEKLIF" && (
              <div className="flex-col w-full">
                <div className="sm:w-1/1 mb-4">
                  <SatisFirsatiSonuc satisFirsati={satisFirsati} {...props} />
                </div>
                {/* 
                <div className="sm:w-1/1 mt-4">
                  <TeklifDokumanlari satisFirsati={satisFirsati} {...props} />
                </div>
                */}
              </div>
            )}

            {satisFirsati.durum !== undefined && (
              <div className="sm:w-1/2 ml-4">
                <SatisFirsatiNotlar satisFirsati={satisFirsati} {...props} />
              </div>
            )}
          </div>
        </div>
      }
    />
  );
};

export default SatisFirsatiPage;

export const getServerSideProps = async (ctx) => {
  const { token } = parseCookies(ctx);
  const res = await axios.get(`${baseUrl}/api/musteriler`, {
    params: {
      filter: {
        durum: { $ne: "sorunlu" },
      },
      projection: {fields: "marka durum"}
    },
    headers: { authorization: token },
  });
  const musteriler = res.data;

  /*  const res = await fetch(`${baseUrl}/api/musteriler`, {
    method: "GET",
    headers: {
      authorization: token,
    },
  });
  const musteriler = await res.json();
*/
  const resPersonel = await fetch(`${baseUrl}/api/users`, {
    method: "GET",
    headers: {
      authorization: token,
    },
  });
  const personeller = await resPersonel.json();

  if (ctx.query.id === "new") {
    return {
      props: {
        musteriler: musteriler,
        personeller: personeller,
        satisFirsati: {
          musteri: null,
          notlar: [],
        },
      },
    };
  } else {
    const resSf = await fetch(`${baseUrl}/api/sfs/${ctx.query.id}`, {
      method: "GET",
      headers: {
        authorization: token,
      },
    });
    const sf = await resSf.json();
    return {
      props: {
        musteriler: musteriler,
        personeller: personeller,
        satisFirsati: sf.data,
      },
    };
  }
};
