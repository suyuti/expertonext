import React from "react";
import Page from "../../../src/Page";
import { parseCookies, destroyCookie } from "nookies";
import baseUrl from "../../../utils/baseUrl";
import {
  TextField,
  Card,
  CardHeader,
  CardContent,
  Divider,
  CardActions,
  Typography,
  Button,
} from "@material-ui/core";
import EventIcon from "@material-ui/icons/Event";
import { DateTimePicker, KeyboardDateTimePicker } from "@material-ui/pickers";
import { useRouter } from "next/router";

const RandevuPage = (props) => {
  const { user, token, randevu } = props;
  const router = useRouter();

  const handleRandevuZamani = (val) => {
    //var _f = {...form}
    //_f.randevuTarihi = val
    //setForm(_f)
  };

  const handleSubmit = () => {};

  const canSubmit = () => {
    return true; //form.randevuTarihi && form.randevuYeri
  };

  return (
    <Page
      content={
        <div className="flex">
          <Card className="sm:w-1/2 border-t-4 border-red-200">
            <CardHeader
              title={
                <Typography className="text-xl" variant="h6">
                  Randevu
                </Typography>
              }
              subheader="Randevu detayları"
              avatar={<EventIcon fontSize="large" />}
            />
            <Divider />
            <CardContent>
              <div className="flex-col">
                <div className="m-4">
                  <TextField
                    label="Musteri"
                    variant="outlined"
                    fullWidth
                    select
                  />
                </div>
                <div className="m-4">
                  <KeyboardDateTimePicker
                    variant="inline"
                    inputVariant="outlined"
                    ampm={false}
                    label="Randevu Tarihi"
                    //value={form.randevuTarihi}
                    onChange={handleRandevuZamani}
                    onError={console.log}
                    //disableFuture
                    disablePast
                    format="dd/MM/yyyy HH:mm"
                    name="randevuTarihi"
                    fullWidth
                    autoOk
                    showTodayButton
                  />
                </div>

                <div className="m-4">
                  <TextField label="Konum" variant="outlined" fullWidth />
                </div>
                <div className="m-4">
                  <TextField
                    label="Firma Katilimcilar"
                    variant="outlined"
                    fullWidth
                  />
                </div>
                <div className="m-4">
                  <TextField
                    label="Experto Katilimcilar"
                    variant="outlined"
                    fullWidth
                    select
                  />
                </div>
                <div className="m-4">
                  <TextField
                    label="Konu"
                    variant="outlined"
                    fullWidth
                    multiline
                    rows={3}
                  />
                </div>
              </div>
            </CardContent>
            <Divider />
            <CardActions>
              <div className="flex w-full justify-between p-8 pr-16">
                  <div className="flex">
                {router.query.id !== "new" && (
                    <Button 
                        variant="outlined"
                        color='primary'
                        >Toplanti Yap</Button>
                        )}
                  </div>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={handleSubmit}
                  disabled={!canSubmit()}
                >
                  {router.query.id === "new" ? "Olustur" : "Guncelle"}
                </Button>
              </div>
            </CardActions>
          </Card>
        </div>
      }
    />
  );
};

export default RandevuPage;

export const getServerSideProps = async (ctx) => {
  const { token } = parseCookies(ctx);

  if (ctx.query.id === "new") {
    return {
      props: {
        randevu: {},
      },
    };
  } else {
    const res = await fetch(`${baseUrl}/api/randevular/${ctx.query.id}`, {
      method: "GET",
      headers: { authorization: token },
    });
    const randevu = await res.json();
    return {
      props: {
        randevu: randevu,
      },
    };
  }
};
