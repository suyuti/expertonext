import React, {useState} from "react";
import baseUrl from "../../../utils/baseUrl";
import Linko from "../../../components/Link";
import { useRouter } from "next/router";
import Link from "next/link";
import { Button, Typography, Box, TextField, formatMs } from "@material-ui/core";
import Page from "../../../src/Page";
import UrunBilgiler from "../../../components/urun/UrunBilgiler";
import UrunDokumanlar from "../../../components/urun/UrunDokumanlar";

const Urun = (props) => {
  const { urun } = props;
  const router = useRouter();
  const {id} = router.query
  const [yeni, setYeni] = useState(id === 'new')
  const [form, setForm] = useState({ ...urun });

  const save = () => {
    fetch(`${baseUrl}/api/urunler`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(form),
    });
  };

  const update = () => {
    fetch(`${baseUrl}/api/urun/${form._id}`, {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(form),
    });
  };

  const handleChange = (e) => {
    const u = {...form}
    u[e.target.name] = e.target.type === 'checkbox' ? e.target.checked : e.target.value
    setForm(u)
  }


  return (
    <Page
      header={
        <Box id="header" className="flex w-full p-10 bg-blue-200 justify-between">
        <Typography>{yeni ? 'Yeni Urun' : 'Urun Guncelleme'}</Typography>
        {/*
        <Typography>{yeni ? 'Yeni Urun' : urun.adi}</Typography>
        <Button variant='contained' color='primary'
          onClick={(e) => {
            e.preventDefault();
            if (yeni) {
              save()
            }
            else {
              update()
            }
          }}
        >Kaydet</Button>
      */}
      </Box>

      }

      content={
        <div className="flex flex-wrap p-8">
          <div className="p-4 sm:w-1/2 md:w-1/2">
            <UrunBilgiler {...props}
            //urun={form} 
            //handleChange={handleChange} 
            />
          </div>
          <div className="p-4 sm:w-1/2 md:w-1/2">
            <UrunDokumanlar data={urun} />
          </div>
        </div>
      }
    />
  );
};

export default Urun;

//export const getServerSideProps = async (ctx) => {
//  if (ctx.query.id !== 'new') {
//
//  const res = await fetch(`${baseUrl}/api/urun/${ctx.query.id}`);
//  const data = await res.json();
//  return { props: {urun: data.data} };
//  }
//  else {
//    return { props: {urun: {}}}
//  }
//};
