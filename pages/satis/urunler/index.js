import React from "react";
import MaterialTable from "material-table";
import baseUrl from "../../../utils/baseUrl";
import { useRouter } from "next/router";
import Page from "../../../src/Page";
import { Typography, Box, Button } from "@material-ui/core";

const Urunler = (props) => {
  const router = useRouter();
  const { user } = props;

  return (
    <Page
      header={
        <Box
          id="header"
          className="flex w-full p-10 bg-blue-200 justify-between"
        >
          <Typography>Urunler</Typography>
          {user.yetkiler.create_urun && (
            <Button
              variant="contained"
              color="primary"
              onClick={(e) => {
                e.preventDefault();
                router.push("/satis/urunler/[id]", `/satis/urunler/new`);
              }}
            >
              Yeni
            </Button>
          )}
        </Box>
      }
      content={
        <div className="p-15">
          <MaterialTable
            title="Ürünler"
            columns={[
              { title: "Urun adi", field: "adi" },
              {
                title: "Ön Ödeme",
                field: "onOdemeTutari",
                render: (rowData) =>
                  rowData.onOdemeTutariVar ? rowData.onOdemeTutari : "-",
              },
              {
                title: "Sabit Ödeme",
                field: "sabitTutar",
                render: (rowData) =>
                  rowData.sabitTutarVar ? rowData.sabitTutar : "-",
              },
              {
                title: "Başarı",
                field: "basariTutari",
                render: (rowData) =>
                  rowData.basariTutariVar ? rowData.basariTutari : "-",
              },
              {
                title: "Rapor Başı Ödeme",
                field: "raporBasiOdemeTutari",
                render: (rowData) =>
                  rowData.raporBasiOdemeTutariVar
                    ? rowData.raporBasiOdemeTutari
                    : "-",
              },
            ]}
            data={props.data}
            onRowClick={(e, rowData) => {
              e.preventDefault();
              if (user.yetkiler.view_urun) {
                router.push(
                  "/satis/urunler/[id]",
                  `/satis/urunler/${rowData._id}`
                );
              }
            }}
            actions={[
              {
                icon: "delete",
                tooltip: "Sil",
                onClick: (event, rowData) =>
                  confirm(rowData.adi + " silinecek."),
                disabled: !user.yetkiler.delete_urun,
              },
            ]}
            options={{
              actionsColumnIndex: -1,
            }}
          />
        </div>
      }
    />
  );
};

Urunler.getInitialProps = async (ctx) => {
  const res = await fetch(`${baseUrl}/api/urunler`);
  const result = await res.json();
  return result;
};

export default Urunler;
