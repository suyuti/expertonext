import React from "react";
import Page from "../../../src/Page";
import { Typography } from "@material-ui/core";
import MaterialTable from "material-table";
import baseUrl from "../../../utils/baseUrl";
import { parseCookies, destroyCookie } from "nookies";
import { useRouter } from "next/router";
import moment from "moment";

const Personel = (props) => {
  const { user, token, data } = props;
  const router = useRouter();
  return (
    <Page
      content={
        <div className="flex">
          <div className="w-full">
            <MaterialTable
              title="Kullanicilar"
              columns={[
                {
                  title: "Adı Soyadı",
                  render: (rowData) => `${rowData.adi} ${rowData.soyadi}`,
                },
                { title: "Kullanıcı Adı", field: "displayName" },
                {
                  title: "Kayıt Tarihi",
                  render: (rowData) =>
                    moment(rowData.createdAt).format("DD/MM/YYYY HH:mm"),
                },
                { title: "Yetki", field: "role" },
                {
                  title: "Aktif",
                  render: (rowData) =>
                    rowData.aktif ? "Aktif" : "Aktif Değil",
                },
                { title: "Mail", field: "email" },
                { title: "Yoneticisi",  render: rowData => rowData.parent ? `${rowData.parent.adi} ${rowData.parent.soyadi}` : '' },
              ]}
              data={data}
              actions={[
                {
                  icon: 'delete',
                  tooltip: 'Sil',
                  onClick: (event, rowData) => confirm(rowData.adi + ' ' + rowData.soyadi + ' silinecek.'),
                  disabled: !user.yetkiler.delete_personel
                }
              ]}
              options={{
                actionsColumnIndex: -1,
                pageSize: 100
              }}
              onRowClick={(e, rowData) => {
                e.preventDefault();
                //if (user.yetkiler.view_personel) {
                router.push(
                  "/satis/personeller/[id]",
                  `/satis/personeller/${rowData._id}`
                );
                //}
              }}
            />
          </div>
        </div>
      }
    />
  );
};

Personel.getInitialProps = async (ctx) => {
  const { token } = parseCookies(ctx);

  const res = await fetch(`${baseUrl}/api/users`, {
    method: "GET",
    headers: {
      authorization: token,
    },
  });
  const result = await res.json();
  return result;
};

export default Personel;
