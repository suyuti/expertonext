import React, { useState } from "react";
import Page from "../../../src/Page";
import baseUrl from "../../../utils/baseUrl";
import { parseCookies, destroyCookie } from "nookies";
import {
  TextField,
  Button,
  MenuItem,
  FormControlLabel,
} from "@material-ui/core";
import moment from "moment";
import IOSSwitch from "../../../components/IOSSwitch";
import axios from "axios";
import { useRouter } from "next/router";

const roller = [
  //{ rolAdi: "Admin",                        key: "admin"                            },
  { rolAdi: "Yönetici", key: "Yönetici" },
  { rolAdi: "Satış Yöneticisi", key: "Satış Yöneticisi" },
  { rolAdi: "Satış Personel", key: "Satış Personel" },
  { rolAdi: "Müşteri", key: "Müşteri" },
  { rolAdi: "Ziyaretçi", key: "Ziyaretçi" },
  { rolAdi: "Proje Yönetim Grup Md.", key: "Proje Yönetim Grup Md." },
  { rolAdi: "Proje Yönetim Uzm.", key: "Proje Yönetim Uzm." },
  { rolAdi: "Proje Yönetim Uzm. Yrd.", key: "Proje Yönetim Uzm. Yrd." },
  { rolAdi: "ArGe Muhasebesi Direktörü", key: "ArGe Muhasebesi Direktörü" },
  { rolAdi: "ArGe Muhasebesi Uzm.", key: "ArGe Muhasebesi Uzm." },
  { rolAdi: "Teknik Grup Md. (Metatek)", key: "Teknik Grup Md. (Metatek)" },
  { rolAdi: "Teknik Uzm. (Metatek)", key: "Teknik Uzm. (Metatek)" },
  { rolAdi: "Teknik Grup Md. (MakiTek)", key: "Teknik Grup Md. (MakiTek)" },
  { rolAdi: "Teknik Uzm. (Makitek)", key: "Teknik Uzm. (Makitek)" },
  { rolAdi: "IK ve İdari İşler Md.", key: "IK ve İdari İşler Md." },
  { rolAdi: "Muhasebe Uzm.", key: "Muhasebe Uzm." },
  { rolAdi: "Asistan", key: "Asistan" },
  { rolAdi: "İdari İşler", key: "İdari İşler" },
  { rolAdi: "Kurumsal İletişim Direktörü", key: "Kurumsal İletişim Direktörü" },
  { rolAdi: "SGK Operasyon Uzm.", key: "SGK Operasyon Uzm." },
  { rolAdi: "Genel Müdür Yard.", key: "Genel Müdür Yard." },
  { rolAdi: "CEO", key: "CEO" },
  { rolAdi: "Yönetim Kurulu Başkanı", key: "Yönetim Kurulu Başkanı" },
];

const Personel = (props) => {
  const { personelData, personeller, token } = props;
  const router = useRouter();
  moment.locale("tr");
  const [personel, setPersonel] = useState(personelData);

  console.log(personel)
  const handleChange = (e) => {
    var p = { ...personel };
    p[e.target.name] = e.target.value;
    setPersonel(p);
  };

  const toggleAtive = (e) => {
    axios
      .put(
        `${baseUrl}/api/users/${router.query.id}`,
        { aktif: e.target.checked },
        {
          headers: {
            authorization: token,
          },
        }
      )
      .then((resp) => {
        if (resp.status === 200) {
          setPersonel((prev) => ({
            ...prev,
            aktif: resp.data.data.aktif,
          }));
        }
      });
  };

  const handleSubmit = () => {
    axios
      .put(`${baseUrl}/api/users/${router.query.id}`, personel, {
        headers: {
          authorization: token,
        },
      })
      .then((resp) => {
        if (resp.status === 200) {
        }
      });
  };

  return (
    <Page
      content={
        <div className="flex-col">
          <div className="m-4">
            <FormControlLabel
              control={
                <IOSSwitch
                  checked={personel.aktif}
                  onChange={toggleAtive}
                  name="checkedB"
                />
              }
              label="Aktif"
            />
          </div>
          <div className="m-4">
            <div className="flex">
              <TextField
                label="Adi"
                variant="outlined"
                fullWidth
                name="adi"
                value={personel.adi}
                onChange={handleChange}
              />
              <TextField
                label="Soyadi"
                variant="outlined"
                fullWidth
                name="soyadi"
                value={personel.soyadi}
                onChange={handleChange}
              />
            </div>
          </div>
          <div className="m-4">
            <TextField
              label="Görünür adı"
              variant="outlined"
              fullWidth
              name="displayName"
              value={personel.displayName}
              onChange={handleChange}
            />
          </div>
          <div className="m-4">
            <TextField
              label="Email"
              variant="outlined"
              fullWidth
              name="email"
              value={personel.email}
              onChange={handleChange}
            />
          </div>
          <div className=" flex m-4">
            <TextField
              label="Rol"
              variant="outlined"
              fullWidth
              select
              name="role"
              value={personel.role}
              onChange={handleChange}
            >
              {roller
                .sort((a, b) => {
                  if (b.rolAdi > a.rolAdi) return -1;
                })
                .map((r) => (
                  <MenuItem key={r.key} value={r.key}>
                    {r.rolAdi}
                  </MenuItem>
                ))}
            </TextField>
            <TextField
              label="Yöneticisi"
              variant="outlined"
              fullWidth
              select
              name="parent"
              value={personel.parent}
              onChange={handleChange}
            >
              {personeller.map((p) => (
                <MenuItem key={p._id} value={p._id}>
                  {`${p.adi} ${p.soyadi}`}
                </MenuItem>
              ))}
            </TextField>
          </div>
          <div className="m-4">
            <TextField
              label="Kayıt Tarihi"
              variant="outlined"
              value={moment(personel.createdAt).format("Do MMMM YYYY, hh:mm")}
              fullWidth
              InputLabelProps={{ shrink: true }}
              InputProps={{
                readOnly: true,
              }}
            />
          </div>

          {personel.role === "Satış Personel" && (
            <div className="flex">
              <div className="m-4">
                <TextField
                  label="Satış Hedefi"
                  variant="outlined"
                  //value={moment(personel.createdAt).format("Do MMMM YYYY, hh:mm")}
                  fullWidth
                  name='satisHedefi'
                  value={personel.satisHedefi}
                  onChange={handleChange}
                  //InputLabelProps={{ shrink: true }}
                  InputProps={{
                    //readOnly: true,
                  }}
                />
              </div>
              <div className="m-4">
                <TextField
                  label="İndirim Oranı"
                  variant="outlined"
                  //value={moment(personel.createdAt).format("Do MMMM YYYY, hh:mm")}
                  fullWidth
                  name='indirimOrani'
                  value={personel.indirimOrani}
                  onChange={handleChange}
                  //InputLabelProps={{ shrink: true }}
                  InputProps={{
                    //readOnly: true,
                  }}
                />
              </div>
            </div>
          )}
          <div className="flex m-4 justify-between">
            <Button variant="contained" color="secondary">
              Sifre Sıfırla
            </Button>

            <Button variant="contained" color="primary" onClick={handleSubmit}>
              Kaydet
            </Button>
          </div>
        </div>
      }
    />
  );
};

Personel.getInitialProps = async (ctx) => {
  const { token } = parseCookies(ctx);

  const res = await fetch(`${baseUrl}/api/users/${ctx.query.id}`, {
    method: "GET",
    headers: {
      authorization: token,
    },
  });
  const result = await res.json();

  const resPersoneller = await fetch(`${baseUrl}/api/users`, {
    method: "GET",
    headers: {
      authorization: token,
    },
  });
  const resultPersoneller = await resPersoneller.json();

  return {
    personelData: result.data,
    personeller: resultPersoneller.data,
  };
};

export default Personel;
