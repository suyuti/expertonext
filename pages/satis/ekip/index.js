import React, { Fragment } from 'react'
import { parseCookies } from "nookies";
import axios from "axios";
import baseUrl from "../../../utils/baseUrl";

const EkipPage = props => {
    return (
        <Fragment>

        </Fragment>
    )
}

EkipPage.getInitialProps = async (ctx) => {
    const { token } = parseCookies(ctx);
    const resp = await axios.get(`${baseUrl}/api/users`, {
        params: {
            all: true
        },
      headers: { authorization: token },
    });
  
    return {
      toplantilar: resp.data.data,
    };
  };
  

export default EkipPage