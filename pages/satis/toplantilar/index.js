import React from "react";
import MaterialTable from "material-table";
import { useRouter } from "next/router";
import { parseCookies } from "nookies";
import axios from "axios";
import baseUrl from "../../../utils/baseUrl";
import moment from "moment";
import { Chip } from "@material-ui/core";

const Toplantilar = (props) => {
  const router = useRouter();
  const { toplantilar } = props;
  return (
    <div className="m-4 p-4">
      <MaterialTable
        title="Toplantlar"
        data={toplantilar}
        columns={[
          { title: "Musteri", field: "musteri.marka" },
          { title: "Proje", field: "" },
          { title: "Yer", field: "konum" },
          {
            title: "Tarih",
            field: "zaman",
            render: (rowData) => moment(rowData.zaman).format("DD.MM.YYYY"),
          },
          {
            title: "Firma Katılımcılar",
            render: (rowData) =>
              rowData.firmaKatilimcilar.map((k, i) => (
                <div className="p-2" key={k._id}>
                  <Chip
                    size="small"
                    label={`${k.adi} ${k.soyadi} / ${k.unvani}`}
                    variant="outlined"
                  />
                </div>
              )),
          },
          {
            title: "Experto Katılımcılar",
            render: (rowData) =>
              rowData.expertoKatilimcilar.map((k) => (
                <div className="p-2">
                  <Chip
                    size="small"
                    label={`${k.adi} ${k.soyadi}`}
                    variant="outlined"
                  />
                </div>
              )),
          },
        ]}
        onRowClick={(e, rowData) => {
          e.preventDefault();
          router.push("/satis/toplantilar/[id]", `/satis/toplantilar/${rowData._id}`);
        }}
        actions={[
          {
            icon: "add",
            tooltip: "Yeni Toplantı",
            isFreeAction: true,
            onClick: (e) => {
              e.preventDefault();
              router.push("/satis/toplantilar/[id]", "/satis/toplantilar/new");
            },
          },
        ]}
      />
    </div>
  );
};

Toplantilar.getInitialProps = async (ctx) => {
  const { token } = parseCookies(ctx);
  const resp = await axios.get(`${baseUrl}/api/toplantilar`, {
    headers: { authorization: token },
  });

  return {
    toplantilar: resp.data.data,
  };
};

export default Toplantilar;
