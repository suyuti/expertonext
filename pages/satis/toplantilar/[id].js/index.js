import React from "react";
import ToplantiForm from "../../../../components/toplantilar/ToplantiForm";
import { parseCookies } from "nookies";
import axios from "axios";
import baseUrl from "../../../../utils/baseUrl";
import ToplantiKararList from "../../../../components/toplantilar/ToplantiKararList";

const KararForm = props => {

}

const Toplanti = (props) => {
  return (
    <div className="flex-col p-8 w-full">
      <div className="w-full">
        <ToplantiForm {...props} />
      </div>
      <div className="w-full">
        <ToplantiKararList {...props} />
      </div>
    </div>
  );
};

Toplanti.getInitialProps = async (ctx) => {
  const { token } = parseCookies(ctx);
  const resp = await axios.get(`${baseUrl}/api/musteriler`, {
    headers: { authorization: token },
  });
  const respUser = await axios.get(`${baseUrl}/api/users`, {
    headers: { authorization: token },
  });
  const respDepartmanlar = await axios.get(`${baseUrl}/api/departmanlar`, {
    headers: { authorization: token },
  });
  const respToplanti = await axios.get(
    `${baseUrl}/api/toplantilar/${ctx.query.id}`,
    {
      headers: { authorization: token },
    }
  );

  return {
    musteriler: resp.data.data,
    personeller: respUser.data.data,
    departmanlar: respDepartmanlar.data.data,
    toplanti: respToplanti.data.data,
  };
};
export default Toplanti;
