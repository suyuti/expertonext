import React, {useState} from "react";
import baseUrl from "../../../utils/baseUrl";
import { useRouter } from "next/router";
import Link from "next/link";
import {
  Box,
  Typography,
  TextField,
  Card,
  CardHeader,
  Divider,
  CardContent,
} from "@material-ui/core";
import Page from "../../../src/Page";
import axios from "axios";
import { parseCookies, destroyCookie } from "nookies";
import KisiBilgileri from "../../../components/kontakKisi/KisiBilgileri";
import Gorusmeler from "../../../components/Gorusmeler";

const Kisi = (props) => {
  const { kisi } = props;
  const router = useRouter();

  const [yeni, setYeni] = useState(router.query.id === "new");

  return (
    <Page
      header={
        <Box
          id="header"
          className="flex w-full p-10 bg-blue-200 justify-between"
        >
          <Typography>{yeni ? "Yeni Kişi" : `Kişi Adı`}</Typography>
        </Box>
      }
      content={
        <div className="flex flex-wrap p-8">
          <div className="p-4 sm:w-1/2 md:w-1/2">
            <KisiBilgileri kisi={kisi} {...props} />
          </div>
          <div className="p-4 sm:w-1/2 md:w-1/2">
            <Gorusmeler relation='kisi' value={kisi} {...props} />
          </div>
        </div>
      }
    />
  );
};

export default Kisi;

export const getServerSideProps = async (ctx) => {
  const { token } = parseCookies(ctx);

  const resp = await axios.get(`${baseUrl}/api/kisiler/${ctx.query.id}`, {
    headers: {authorization: token}
  })

  const respMusteri = await axios.get(`${baseUrl}/api/musteriler`, {
    params: {projection: {fields: 'marka'}}, 
    headers: {authorization: token}
  })
  return { props: {
      kisi: resp.data.data,
      musteriler: respMusteri.data.data
    } };
};
