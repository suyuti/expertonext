import React from "react";
import MaterialTable from "material-table";
import baseUrl from "../../../utils/baseUrl";
import { useRouter } from "next/router";
import { Typography, Box, Button } from "@material-ui/core";
import Page from "../../../src/Page";
import axios from "axios";
import { parseCookies, destroyCookie } from "nookies";

const Kisiler = (props) => {
  const { user, yetkiler, token, kisiler } = props;
  const router = useRouter();

  return (
    <Page
      header={
        <Box
          id="header"
          className="flex w-full p-10 bg-blue-200 justify-between items-center"
        >
          <Typography variant="h5">Kontak Kişiler</Typography>
          {user.yetkiler.create_kisi && (
            <Button
              variant="contained"
              color="primary"
              onClick={(e) => {
                e.preventDefault();
                router.push("/satis/kisiler/[id]", `/satis/kisiler/new`);
              }}
            >
              Yeni
            </Button>
          )}
        </Box>
      }
      content={
        <div className="p-15">
          <MaterialTable
            title="Kontak Kisiler"
            columns={[
              {
                title: "Adı Soyadı",
                render: (rowData) => `${rowData.adi} ${rowData.soyadi}`,
              },
              { title: "Firma", field: "musteri.marka" },
              { title: "Unvanı", field: "unvani" },
              { title: "Yetki", field: "yetki" },
              {
                title: "Telefon",
                render: (rowData) => (
                  <div className="flex-col">
                    <p>Cep: {rowData.cepTelefon}</p>
                    <p>İş: {rowData.isTelefon}</p>
                  </div>
                ),
              },
              { title: "Mail", field: "mail" },
            ]}
            data={kisiler}
            actions={[
              {
                icon: "delete",
                tooltip: "Sil",
                onClick: (event, rowData) =>
                  confirm(rowData.adi + ' ' + rowData.soyadi + " silinecek."),
                disabled: !user.yetkiler.delete_kisi,
              },
            ]}
            options={{
              grouping: true,
              actionsColumnIndex: -1,
            }}
            localization={{
              grouping: { placeholder: "Gruplama için sürükleyin." },
            }}
            onRowClick={(e, rowData) => {
              e.preventDefault();
              router.push(
                "/satis/kisiler/[id]",
                `/satis/kisiler/${rowData._id}`
              );
            }}
          />
        </div>
      }
    />
  );
};

Kisiler.getInitialProps = async (ctx) => {
  const { token } = parseCookies(ctx);

  const resp = await axios.get(`${baseUrl}/api/kisiler`, {
    headers: { authorization: token },
  });

  return { kisiler: resp.data.data };
};

export default Kisiler;
