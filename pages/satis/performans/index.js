import React from 'react'
import PersonelSatisPerformans from '../../../components/satisPerformans/PersonelSatisPerformans'
import useSWR from 'swr'
import baseUrl from '../../../utils/baseUrl'
import { parseCookies } from 'nookies'
import axios from 'axios'

const PerformansIzlemePage = props => {
    const {personeller} = props
    return (
        <div className='p-4'>
            {personeller && personeller.map(p =>
                <PersonelSatisPerformans personel={p} {...props}/>
            )}
        </div>
    )
}

export const getServerSideProps = async (ctx) => {
    const {token} = parseCookies(ctx)
    const resp = await axios.get(`${baseUrl}/api/ekip`, {headers: {authorization: token}})
    return {
        props: {
            personeller: resp.data.data
        }
    }
}
export default PerformansIzlemePage