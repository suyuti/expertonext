import React, { useState } from "react";
import baseUrl from "../../../utils/baseUrl";
import Linko from "../../../components/Link";
import { useRouter } from "next/router";
import Link from "next/link";
import {
  Button,
  Typography,
  TextField,
  Box,
  MenuItem,
} from "@material-ui/core";
import Page from "../../../src/Page";
import FirmaBilgiler from "../../../components/firma/FirmaBilgiler";
import FirmaGorusmeler from "../../../components/firma/FirmaGorusmeler";
import FirmaKisiler from "../../../components/firma/FirmaKisiler";
import FirmaTeklifler from "../../../components/firma/FirmaTeklifler";
import * as Yetki from "../../../utils/permissions";
import Gorusmeler from '../../../components/Gorusmeler'

const Musteri = (props) => {
  const router = useRouter();
  const {user} = props
  const { id } = router.query;
  const [yeni, setYeni] = useState(id === "new");
  const [gorusmeleriGorebilir, setGorusmeleriGorebilir] = useState();
  const [gorusmeEkleyebilir, setGorusmeEkleyebilir] = useState();

  return (
    <Page
      header={
        <Box
          id="header"
          className="flex w-full p-10 bg-blue-200 justify-between"
        >
          <Typography>{yeni ? "Yeni Musteri" : `musteri.marka`}</Typography>
        </Box>
      }
      content={
        <div className="flex flex-wrap p-8">
          <div className="p-4 sm:w-1/2 md:w-1/2">
            <FirmaBilgiler {...props} />
          </div>
          {!yeni && (
            <React.Fragment>
              {user.yetkiler.list_gorusme && (
                <div className="p-4 sm:w-1/2">
                  <Gorusmeler relation='musteri' value={id} {...props} />
                </div>
              )}
              {user.yetkiler.list_kisi && (
                <div className="p-4 sm:w-1/2">
                  <FirmaKisiler />
                </div>
              )}
              {/*user.yetkiler.list_satisfirsati && (
                <div className="p-4 sm:w-1/2">
                  <FirmaTeklifler musteri={musteri} />
                </div>
              )*/}
            </React.Fragment>
          )}
        </div>
      }
    />
  );
};

export default Musteri;
/*
export const getServerSideProps = async (ctx) => {
  if (ctx.query.id !== "new") {
    const res = await fetch(`${baseUrl}/api/musteriler/${ctx.query.id}`);
    const data = await res.json();
    const sres = await fetch(`${baseUrl}/api/sektorler`);
    const sdata = await sres.json();

    const personelRes = await fetch(`${baseUrl}/api/users`);
    const personeller = await personelRes.json();

    return {
      props: {
        musteri: data.data,
        sektorler: sdata.data,
        personeller: personeller.data,
      },
    };
  } else {
    const sres = await fetch(`${baseUrl}/api/sektorler`);
    const sdata = await sres.json();

    const personelRes = await fetch(`${baseUrl}/api/users`);
    const personeller = await personelRes.json();

    return {
      props: {
        musteri: {},
        sektorler: sdata.data,
        personeller: personeller.data,
      },
    };
  }
  
};
*/