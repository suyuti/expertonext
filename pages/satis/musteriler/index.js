import React, { useEffect, useState } from "react";
import { Typography, Box, Button } from "@material-ui/core";
import MaterialTable from "material-table";
import baseUrl from "../../../utils/baseUrl";
import { useRouter } from "next/router";
import Page from "../../../src/Page";
import axios from "axios";
import Link from "next/link";
import cookie from "js-cookie";
import * as Yetki from "../../../utils/permissions";
import useSWR from "swr";

const Musteriler = (props) => {
  const router = useRouter();
  const { user, yetkiler, token } = props;
  const [musteriTipi, setMusteriTipi] = useState('')

  useEffect(() => {
    if (router.query.durum === 'potansiyel') { setMusteriTipi('Potansiyel')}
    else if (router.query.durum === 'aktif') { setMusteriTipi('Aktif')}
    else if (router.query.durum === 'pasif') { setMusteriTipi('Pasif')}
    else if (router.query.durum === 'sorunlu') { setMusteriTipi('Sorunlu')}
  }, [router.query.durum])

  const {
    data: musteriler,
    error: musterilerError,
  } = useSWR(`${baseUrl}/api/musteriler?filter=%7B%22durum%22:%22${router.query.durum}%22%7D`, (url) => // query burada verdim mecburen
    axios
    //.get(url, { params: { filter: router.query }, headers: {authorization: token} }) // query burada vermek olmadi. sayfa degisiyor ama query degismiyordu
    .get(url, {  headers: {authorization: token} })
    .then((repsonse) => repsonse.data.data)
  );

  return (
    <Page
      header={
        <Box
          id="header"
          className="flex w-full p-10 bg-blue-200 justify-between items-center"
        >
          <Typography variant="h5">Müşteriler</Typography>
          {user.yetkiler.create_musteri && (
            <Button
              variant="contained"
              color="primary"
              onClick={(e) => {
                e.preventDefault();
                router.push("/satis/musteriler/[id]", `/satis/musteriler/new`);
              }}
            >
              Yeni
            </Button>
          )}
        </Box>
      }
      content={
        <div className="p-15">
          <MaterialTable
            title={`${musteriTipi} Müşteriler`}
            columns={[
              { title: "Marka", field: "marka" },
              { title: "Sektor", field: "sektor.adi" },
              { title: "Il", field: "" },
            ]}
            isLoading={!musteriler}
            data={musteriler && musteriler || []}
            onRowClick={(e, rowData) => {
              e.preventDefault();
              if (user.yetkiler.view_musteri) {
                router.push(
                  "/satis/musteriler/[id]",
                  `/satis/musteriler/${rowData._id}`
                );
              }
            }}
            actions={[
              {
                icon: "delete",
                tooltip: "Sil",
                onClick: (event, rowData) =>
                  confirm(rowData.marka + " silinecek."),
                disabled: !user.yetkiler.delete_musteri,
              },
            ]}
            options={{
              actionsColumnIndex: -1,
              grouping: true,
              pageSize: 100
            }}
            localization={{
              grouping: { placeholder: "Gruplama için sürükleyin." },
            }}
          />
        </div>
      }
    />
  );
};

export default Musteriler;
