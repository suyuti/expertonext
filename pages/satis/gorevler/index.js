import React from "react";
import MaterialTable from "material-table";
import baseUrl from "../../../utils/baseUrl";
import { useRouter } from "next/router";
import Page from "../../../src/Page";
import { Typography, Box, Button } from "@material-ui/core";
import axios from "axios";
import { parseCookies, destroyCookie } from "nookies";

const Gorevler = (props) => {
  const router = useRouter();
  const { user } = props;

  return (
    <Page
      header={
        <Box
          id="header"
          className="flex w-full p-10 bg-blue-200 justify-between"
        >
          <Typography>Görevler</Typography>
          {user.yetkiler.create_gorev && (
            <Button
              variant="contained"
              color="primary"
              onClick={(e) => {
                e.preventDefault();
                router.push("/satis/gorevler/[id]", `/satis/gorevler/new`);
              }}
            >
              Yeni
            </Button>
          )}
        </Box>
      }
      content={
        <div className="p-15">
          <MaterialTable
            title="Görevler"
            columns={[
              { title: "Urun adi", field: "adi" },
            ]}
            data={props.data}
            onRowClick={(e, rowData) => {
              e.preventDefault();
              if (user.yetkiler.view_gorev) {
                router.push(
                  "/satis/gorevler/[id]",
                  `/satis/gorevler/${rowData._id}`
                );
              }
            }}
            actions={[
              {
                icon: "delete",
                tooltip: "Sil",
                onClick: (event, rowData) =>
                  confirm(rowData.baslık + " silinecek."),
                disabled: !user.yetkiler.delete_gorev,
              },
            ]}
            options={{
              actionsColumnIndex: -1,
            }}
          />
        </div>
      }
    />
  );
};

Gorevler.getInitialProps = async (ctx) => {
    const { token } = parseCookies(ctx);
    const resp = await axios.get(`${baseUrl}/api/urunler`, {headers: {authorization: token}})
    return {gorevler: resp.data.data}
};

export default Gorevler;
