import React, { useState } from "react";
import baseUrl from "../../../utils/baseUrl";
import Linko from "../../../components/Link";
import { useRouter } from "next/router";
import Link from "next/link";
import {
  Button,
  Typography,
  Box,
  TextField,
  formatMs,
  Card,
  CardHeader,
  CardContent,
  CardActions,
  LinearProgress,
    Divider,
    MenuItem
} from "@material-ui/core";
import Page from "../../../src/Page";
import UrunBilgiler from "../../../components/urun/UrunBilgiler";
import UrunDokumanlar from "../../../components/urun/UrunDokumanlar";
import AccountBalanceIcon from "@material-ui/icons/AccountBalance";
import axios from "axios";
import { parseCookies, destroyCookie } from "nookies";
import GorevBilgiler from '../../../components/gorev/GorevBilgiler'
import Notlar from "../../../components/Notlar";

const Gorev = (props) => {
  const {gorev, user, token } = props;
  const router = useRouter();
  const { id } = router.query;
  const [yeni, setYeni] = useState(id === "new");

  const onAddNewNot = (not) => {
    axios.post(`${baseUrl}/api/notlar`, {relation: 'gorev', id: id, not: not}, {headers: {authorization: token}})
  }

  return (
    <Page
      header={
        <Box
          id="header"
          className="flex w-full p-10 bg-blue-200 justify-between"
        >
          <Typography>{yeni ? "Yeni Görev" : "Görev Guncelleme"}</Typography>
        </Box>
      }
      content={
        <div className="flex flex-wrap p-8">
          <div className="p-4 sm:w-1/2 md:w-1/2">
            <GorevBilgiler {...props}/>
          </div>
          {!yeni && 
          <div className="p-4 sm:w-1/2 md:w-1/2">
            <Notlar
              onAddNew = {(not) => onAddNewNot(not)} 
              notlar={gorev.notlar} {...props}/>
          </div>
          }
        </div>
      }
    />
  );
};

export default Gorev;

export const getServerSideProps = async (ctx) => {
  const { token } = parseCookies(ctx);

  const respPersonller    = await axios.get(`${baseUrl}/api/users`,         {params: {hierarcy: true}, headers: {authorization: token}})
  const respDepartmanlar  = await axios.get(`${baseUrl}/api/departmanlar`,  { headers: {authorization: token}})
  const respMusteriler    = await axios.get(`${baseUrl}/api/musteriler`,    { headers: {authorization: token}})
  const resp              = await axios.get(`${baseUrl}/api/gorevler/${ctx.query.id}`,   { headers: {authorization: token}})
  
  return {
    props: {
      gorev: resp.data.data, 
      personeller: respPersonller.data.data,
      musteriler: respMusteriler.data.data,
      departmanlar: respDepartmanlar.data.data
    }
  }
};
