import React, { useState } from "react";
import { useRouter } from "next/router";
import {
  Card,
  CardHeader,
  Divider,
  CardContent,
  CardActions,
  Button,
  TextField,
  MenuItem,
} from "@material-ui/core";
import baseUrl from "../../../utils/baseUrl";
import axios from "axios";
import useSWR from "swr";
import queryString from "query-string";
import { DateTimePicker, KeyboardDateTimePicker } from "@material-ui/pickers";

const Gorusme = (props) => {
  const { token, user } = props;
  const router = useRouter();
  const { id, musteri, satisfirsati } = router.query;
  const query = queryString.parse(router.asPath.split(/\?/)[1]);
  const [yeni, setYeni] = useState(router.query.id === "new");
  const [kisiler, setKisiler] = useState(null);
  const [gorusme, setGorusme] = useState(null);
  const [musteriSabit, setMusteriSabit] = useState(query.musteri !== undefined);

  const { data: gorusmeData, error: gorusmeDataError } = useSWR(
    `${baseUrl}/api/gorusmeler/${id}`,
    (url) =>
      axios
        .get(url, {
          params: { mid: query.musteri },
          headers: { authorization: token },
        })
        .then((res) => res.data.data)
  );

  const {
    data: musteriler,
    error: musterilerError,
  } = useSWR(`${baseUrl}/api/musteriler`, (url) =>
    axios
      .get(url, { headers: { authorization: token } })
      .then((res) => res.data.data)
  );

  const kisileriSec = (musteriId) => {
    axios
      .get(`${baseUrl}/api/musteriler/${musteriId}/kisiler`)
      .then((resp) => setKisiler(resp.data.data));
  };

  const onMusteriChanged = (e) => {
    kisileriSec(e.target.value);
    handleChange(e);
  };

  const canSubmit = () => {
    return (
      gorusme.musteri &&
      gorusme.kisi &&
      gorusme.kanal &&
      gorusme.konu &&
      gorusme.gorusmeZamani
    );
  };

  const handleChange = (e) => {
    var g = { ...gorusme };
    g[e.target.name] = e.target.value;
    setGorusme(g);
  };

  const handleGorusmeZamani = (v) => {
    var g = { ...gorusme };
    g.gorusmeZamani = v;
    setGorusme(g);
  }


  if (
    (!gorusme && gorusmeData) ||
    (gorusme && gorusme._id !== gorusmeData._id)
  ) {
    if (query.musteri) {
      gorusmeData.musteri = query.musteri;
      kisileriSec(query.musteri);
    }
    setGorusme(gorusmeData);
  }

  const save = () => {
    axios
      .post(`${baseUrl}/api/gorusmeler`, gorusme, {
        headers: {
          authorization: token,
        },
      })
      .then((res) => console.log("save"));
  };

  if (!gorusme || gorusme._id !== gorusmeData._id) {
    return <></>;
  }

  //  //console.log(gorusme)

  return (
    <div>
      <Card>
        <CardHeader title="Gorusme notu" />
        <Divider />
        <CardContent>
          <div className="flex-col">
            <div className="m-4">
              <TextField
                label="Musteri"
                variant="outlined"
                name="musteri"
                value={gorusme.musteri}
                fullWidth
                select
                onChange={onMusteriChanged}
                InputProps={{
                  readOnly: musteriSabit,
                }}
              >
                {musteriler &&
                  musteriler.map((m) => (
                    <MenuItem key={m._id} value={m._id}>
                      {m.marka}
                    </MenuItem>
                  ))}
              </TextField>
            </div>
            <div className="m-4">
              <TextField
                label="Kisi"
                variant="outlined"
                fullWidth
                select
                name="kisi"
                value={gorusme.kisi}
                onChange={handleChange}
              >
                {kisiler &&
                  kisiler.map((k) => (
                    <MenuItem
                      key={k._id}
                      value={k._id}
                    >{`${k.adi} ${k.soyadi} / ${k.unvani}`}</MenuItem>
                  ))}
              </TextField>
            </div>
            <div className="m-4 flex">
              <div className='sm:w-1/2 mr-4'>
                <TextField
                  label="Kanal"
                  variant="outlined"
                  fullWidth
                  select
                  name="kanal"
                  value={gorusme.kanal}
                  onChange={handleChange}
                >
                  <MenuItem key="telefon" value="telefon">
                    Telefon
                  </MenuItem>
                  <MenuItem key="mail" value="mail">
                    Mail
                  </MenuItem>
                  <MenuItem key="toplanti" value="toplanti">
                    Toplantı
                  </MenuItem>
                </TextField>
              </div>

              <div className='sm:w-1/2'>
              <KeyboardDateTimePicker
                variant="inline"
                inputVariant='outlined'
                ampm={false}
                label="Görüşme zamanı"
                value={gorusme.gorusmeZamani}
                onChange={handleGorusmeZamani}
                onError={console.log}
                disableFuture
                //disablePast
                format="dd/MM/yyyy HH:mm"
                name="gorusmeZamani"
                fullWidth
                autoOk
                showTodayButton
              />
              </div>
            </div>
            <div className="m-4">
              <TextField
                label="Konu"
                variant="outlined"
                fullWidth
                name="konu"
                value={gorusme.konu}
                onChange={handleChange}
              />
            </div>
            <div className="m-4">
              <TextField
                label="Aciklama"
                variant="outlined"
                fullWidth
                multiline
                rows={3}
                name="aciklama"
                value={gorusme.aciklama}
                onChange={handleChange}
              />
            </div>
            <div className="m-4">
            </div>
          </div>
        </CardContent>
        <CardActions>
          <div className="flex w-full justify-end m-4">
            <Button
              variant="contained"
              color="primary"
              disabled={!canSubmit()}
              onClick={save}
            >
              Kaydet
            </Button>
          </div>
        </CardActions>
      </Card>
    </div>
  );
};

export default Gorusme;
