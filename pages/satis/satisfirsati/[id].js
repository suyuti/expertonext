import React, { useState } from "react";
import baseUrl from "../../../utils/baseUrl";
import { useRouter } from "next/router";
import Link from "next/link";
import {
  Button,
  Typography,
  TextField,
  Box,
  MenuItem,
} from "@material-ui/core";
import Page from "../../../src/Page";
import SatisFirsati from "../../../components/satisFirsati/SatisFirsati";
import MusteriyeYapilimisTeklifler from "../../../components/satisFirsati/MusteriyeYapilmisTeklifler";
import SatisFirsatiSurec from "../../../components/satisFirsati/SatisFirsatiSurec";
import SatisFirsatiAction from "../../../components/satisFirsati/SatisFirsatiAction";
import SatisFirsatiHistory from "../../../components/satisFirsati/SatisFirsatiHistory";

const SatisFirsatiPage = (props) => {
  const router = useRouter();
  const { sf, user, token } = props;

  //const [form, setForm] = useState({ ...musteri });
  const { id } = router.query;
  const [yeni, setYeni] = useState(id === "new");

  const handleChange = (e) => {
    var _f = { ...form };
    _f[e.target.name] = e.target.value;
    setForm(_f);
  };

  const save = () => {
    fetch(`${baseUrl}/api/satisfirsatlari`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(form),
    });
  };

  const update = () => {
    fetch(`${baseUrl}/api/satisfirsatlari/${form._id}`, {
      method: "PUT",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(form),
    });
  };

  return (
    <Page
      header={
        <Box
          id="header"
          className="flex w-full p-10 bg-blue-200 justify-between"
        >
          <Typography variant='h6'>{yeni ? "Satis Firsati" : ""}</Typography>
          {/* 
          <Button variant='contained' color='primary'
            onClick={(e) => {
              e.preventDefault();
              if (yeni) {
                save()
              }
              else {
                update()
              }
            }}
          >Kaydet</Button>
          */}
        </Box>
      }
      content={
        <div className="flex flex-wrap p-8">
          <SatisFirsatiSurec />
          {!yeni && <SatisFirsatiAction />}
          {(user.yetkiler.create_satisfirsati ||
            user.yetkiler.view_satisfirsati) && (
              <div className="p-4 sm:w-1/2 md:w-1/2">
                <SatisFirsati yeni={yeni} {...props} />
              </div>
            )}
          {user.yetkiler.list_satisfirsati && (
            <div className="p-4 sm:w-1/2 md:w-1/2">
              <MusteriyeYapilimisTeklifler musteriId />
            </div>
          )}
          {!yeni &&
            <div className='m-4 w-full'>
              <SatisFirsatiHistory />
            </div>
          }
        </div>
      }
    />
  );
};

export default SatisFirsatiPage;

export const getServerSideProps = async (ctx) => {
  if (ctx.query.id !== "new") {
    const res = await fetch(`${baseUrl}/api/satisfirsati/${ctx.query.id}`);
    const data = await res.json();

    return {
      props: {
        sf: data.data,
      },
    };
  } else {
    return {
      props: {
        sf: {
          musteri: null,
          teklifFiyatlari: {
            onOdemeTutari: 0,
          },
        },
      },
    };
  }
};
