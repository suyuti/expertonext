import React from 'react'
import { Typography } from '@material-ui/core'

const Yeni = props => {
    //console.log('yeni')
    return (
        <div className='flex'>
            <Typography variant='h4'>Hesabınız henüz aktifleştirilmemiş.</Typography>
        </div>
    )
}

export default Yeni